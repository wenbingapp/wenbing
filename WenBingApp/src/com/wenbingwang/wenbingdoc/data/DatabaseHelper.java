package com.wenbingwang.wenbingdoc.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "mydata.db"; 
	private static final int version = 1; 

	public DatabaseHelper(Context context, String s) {
		super(context, s + DB_NAME, null, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql1 = "create table ProblemItemInfo(QuestionID text , SenderID text, SenderVoIP text , TextContent text,SendTime text );";
		String sql2 = "create table ProblemItemInfoUnread(QuestionID text );";
		db.execSQL(sql1);
		db.execSQL(sql2);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
