package com.wenbingwang.mywedgit;

import java.util.ArrayList;

import com.wenbingwang.wenbingapp.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.media.Image;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CallingProcessView extends View {
	private String[] info;
	private int current;
	private int currentDotSize;
	private int verpadding;
	private int textSize;
	private int dotSize;
	private int currentDotColor = 0xff00abfc;
	private int dotColor = 0xffc6c6c6;
	private int textColor = 0xff8b8b8b;
	private int lineColor = 0xffc6c6c6;

	public CallingProcessView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public CallingProcessView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.com_wenbingwang_mywedgit_CallingProcessView, 0, 0);
		String s = a
				.getString(R.styleable.com_wenbingwang_mywedgit_CallingProcessView_infos);
		if (s != null) {
			info = s.split(",");
		}
		current = a
				.getInt(R.styleable.com_wenbingwang_mywedgit_CallingProcessView_current,
						0);
		currentDotSize = a
				.getDimensionPixelOffset(
						R.styleable.com_wenbingwang_mywedgit_CallingProcessView_currentdotsize,
						15);
		textSize = a
				.getDimensionPixelOffset(
						R.styleable.com_wenbingwang_mywedgit_CallingProcessView_mytextsize,
						15);
		verpadding = a
				.getDimensionPixelOffset(
						R.styleable.com_wenbingwang_mywedgit_CallingProcessView_myverpadding,
						15);
		dotSize = a
				.getDimensionPixelOffset(
						R.styleable.com_wenbingwang_mywedgit_CallingProcessView_dotsize,
						10);
		a.recycle();
	}

	public CallingProcessView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG
				| Paint.FILTER_BITMAP_FLAG));
		if (info != null) {
			int width = (getWidth() - getPaddingLeft() - getPaddingRight())
					/ info.length;
			int height = getHeight();
			Paint dotPaint = new Paint();
			Paint linePaint = new Paint();
			Paint textPaint = new Paint();
			textPaint.setTextSize(textSize);
			textPaint.setColor(textColor);
			linePaint.setColor(dotColor);
			canvas.drawRect(width / 2 + getPaddingLeft(), getPaddingTop()
					+ currentDotSize - 3, getPaddingLeft() + (info.length - 1)
					* width + width / 2, getPaddingTop() + currentDotSize + 2,
					linePaint);
			for (int i = 0; i < info.length; i++) {
				int top = getPaddingTop();
				int left = getPaddingLeft() + i * width;
				int middle = left + width / 2;
				if (i == current) {
					dotPaint.setColor(currentDotColor);
					canvas.drawCircle(middle, top + currentDotSize,
							currentDotSize, dotPaint);
				} else {
					dotPaint.setColor(dotColor);
					canvas.drawCircle(middle, top + currentDotSize, dotSize,
							dotPaint);
				}
				canvas.drawText(info[i],
						middle - textPaint.measureText(info[i]) / 2, top
								+ currentDotSize * 2 + verpadding + textSize,
						textPaint);
			}
		}

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width = widthSize;
		int height = verpadding + currentDotSize * 2 + textSize
				+ getPaddingBottom() + getPaddingTop();

		setMeasuredDimension(MeasureSpec.makeMeasureSpec(width, widthMode),
				MeasureSpec.makeMeasureSpec(height, heightMode));
	}

	public void setCurrentDot(int i) {
		if (i < info.length) {
			current = i;
			postInvalidate();
		}
	}
}
