package com.wenbingwang.mywedgit;

import com.wenbingwang.wenbingapp.R;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

public class MyRadioButton extends RadioButton {
	private String textString;
	private Drawable iconDrawable;
	private int h;
	private int w;
	private Rect textRect = new Rect();
	private Paint textPaint = new Paint();

	public MyRadioButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public MyRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.com_wenbingwang_mywedgit_MyRadioButton, 0, 0);
		iconDrawable = a
				.getDrawable(R.styleable.com_wenbingwang_mywedgit_MyRadioButton_myicon);
		textString = a
				.getString(R.styleable.com_wenbingwang_mywedgit_MyRadioButton_mytext);
		a.recycle();
	}

	public MyRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		h = MeasureSpec.getSize(heightMeasureSpec);
		w = MeasureSpec.getSize(widthMeasureSpec);
		double x = 20.0 / 16;
		double a = Math.min(h, w);
		if (iconDrawable != null) {
			iconDrawable.setBounds(w / 2 - (int) (a / x / 2), 0,
					(int) (w / 2 + a / x / 2), (int) (a / x));
		}
		if (textString != null) {
			textPaint.setTextSize((float) (a / 5));
			textPaint.getTextBounds(textString, 0, textString.length(),
					textRect);
			textRect.offset(w / 2 - textRect.right / 2, (int) (a / x + a / 4));
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG
				| Paint.FILTER_BITMAP_FLAG));
		textPaint.setColor(getCurrentTextColor());
		iconDrawable.draw(canvas);
		canvas.drawText(textString, textRect.left, textRect.top, textPaint);
	}

	@Override
	protected void drawableStateChanged() {
		if (iconDrawable != null) {
			int[] myDrawableState = getDrawableState();
			// Set the state of the Drawable
			iconDrawable.setState(myDrawableState);
		}
		super.drawableStateChanged();
	}

}
