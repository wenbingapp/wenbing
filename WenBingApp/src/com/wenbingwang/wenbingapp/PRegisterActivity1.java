package com.wenbingwang.wenbingapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class PRegisterActivity1 extends BaseActivity implements OnClickListener {
	private ProgressDialog progressDialog;
	private EditText p_editText1, p_editText2;
	private Button p_button3;
	private String telephone = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		telephone = getIntent().getStringExtra("Telephone");
		setContentView(R.layout.register_activity);
		p_editText1 = (EditText) findViewById(R.id.p_editText1);
		p_editText2 = (EditText) findViewById(R.id.p_editText2);
		p_button3 = (Button) findViewById(R.id.p_button3);
		p_button3.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("用户注册");
		progressDialog.setMessage("正在提交请求");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.p_button3:
			if (p_editText1.getText().toString().equals("")
					|| p_editText2.getText().toString().equals("")) {
				showToast("请输入您的密码");
			}
			if (p_editText1.getText().toString().equals("") != p_editText2
					.getText().toString().equals("")) {
				showToast("两次的密码不用");
			} else {
				setRegister();
			}
			break;
		}
	}

	private void setRegister() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("Telephone", telephone);
		requestParams.add("Password", p_editText1.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/sjinfo",
				requestParams, new Pregister2Handler());

	}

	private class Pregister2Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}



}
