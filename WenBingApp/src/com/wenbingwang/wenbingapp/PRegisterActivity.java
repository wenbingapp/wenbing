package com.wenbingwang.wenbingapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PRegisterActivity extends BaseActivity implements OnClickListener {
	private Button per_button3, pre_button1, pre_number1;
	private ProgressDialog progressDialog;
	private EditText pre_phone, pre_number;
	private String number = null;// 从服务器上获得的验证码

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pregister);
		per_button3 = (Button) findViewById(R.id.per_button3);
		pre_button1 = (Button) findViewById(R.id.pre_button1);
		pre_phone = (EditText) findViewById(R.id.pre_phone);
		pre_number = (EditText) findViewById(R.id.pre_number);
		pre_number1 = (Button) findViewById(R.id.pre_number1);
		pre_number1.setOnClickListener(this);
		pre_button1 = (Button) findViewById(R.id.pre_button1);
		pre_button1.setOnClickListener(this);
		per_button3.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("用户注册");
		progressDialog.setMessage("正在提交请求");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.per_button3:
			intent.setClass(PRegisterActivity.this, ERegister.class);
			startActivity(intent);
			break;
		case R.id.pre_number1:// 获得验证码
			if (pre_phone.getText().toString().equals("")) {
				showToast("手机号码不能为空");
			} else {
				if (isMobile(pre_phone.getText().toString())) {
					setPassword();
				} else {
					showToast("请输入正确的手机号码");
				}
			}
			break;
		case R.id.pre_button1:
			if (!pre_number.getText().toString().equals("")) {
				setRegister();
			} else {// 18025211740 586143
				showToast("请输入验证码");
			}

			break;
		}
	}

	private void setRegister() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("Telephone", pre_phone.getText().toString());
		requestParams.add("ConfirmationCode", pre_number.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/setaccinfo",
				requestParams, new Pregister2Handler());

	}

	private class Pregister2Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					Intent intent = new Intent();
					intent.setClass(PRegisterActivity.this,
							PRegisterActivity1.class);
					intent.putExtra("Telephone", pre_phone.getText().toString());
					startActivity(intent);
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	private void setPassword() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("Telephone", pre_phone.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/getcodeinfo",
				requestParams, new PregisterHandler());

	}

	private class PregisterHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					number = jsonObject.getString("ConfirmationCode")
							.toString();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	/**
	 * 验证是否是手机号码
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isMobile(String str) {
		Pattern pattern = Pattern.compile("1[0-9]{10}");
		Matcher matcher = pattern.matcher(str);
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}

	}
}
