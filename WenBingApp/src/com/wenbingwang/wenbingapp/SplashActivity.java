package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.bean.MyInfo;
import com.wenbingwang.mywedgit.ACache;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends BaseActivity {
	private int i = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashactivity);
		checkUpdates();
		final String s = CheckHasLogin(SplashActivity.this);
		if (s != null) {
			((MyApplication) (getApplication())).setMyInfo(new MyInfo(s));
			setUserInfo();
		}
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(SplashActivity.this, MainActivity.class);
				if (s != null) {
				} else {
					i++;
				}
				if (i == 1) {
					startActivity(intent);
					finish();
				} else {
					AlertDialog.Builder builder = new Builder(
							SplashActivity.this);
					builder.setMessage("请保持网络通畅");
					builder.setTitle("提示");
					builder.setPositiveButton("确认", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							SplashActivity.this.finish();
						}
					});
					builder.create().show();
				}

			}
		}, 3000);

	}

	private void checkUpdates() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("VersionID", LoginActivity5.getVersionCode(this));
		requestParams.add("VersionType", "0");
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/versioncheck", requestParams,
				new MyHandler3());
	}

	public static String CheckHasLogin(Context c) {
		ACache mCache = ACache.get(c);
		return mCache.getAsString("logininfo");
	}

	private void setUserInfo() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/getperinfo",
				requestParams, new MyHandler1());
	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				((MyApplication) getApplication()).getMyInfo().setUserInfo(
						jsonObject.toString());
				ACache mCache = ACache.get(getApplication());
				mCache.put("logininfo", ((MyApplication) getApplication())
						.getMyInfo().toString());
				i++;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class MyHandler3 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				((MyApplication) getApplication()).checkupdate = jsonObject
						.getString("CheckUpdate");
				((MyApplication) getApplication()).forceupdate = jsonObject
						.getString("ForceUpdate");
				((MyApplication) getApplication()).url = jsonObject
						.getString("DownloadUrl");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

}
