package com.wenbingwang.wenbingapp;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONObject;

import com.alipay.sdk.app.PayTask;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.wenbingwang.app.application.Constants;
import com.wenbingwang.bean.PayvipInfo;
import com.wenbingwang.bean.VipBuyInfo;
import com.wenbingwang.bean.WeixinInfo;
import com.wenbingwang.bean.YinlianInfo;
import com.wenbingwang.zhifubao.PayResult;
import com.wenbingwang.zhifubao.SignUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader.ForceLoadContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class PayVIPActivity extends Pay implements OnClickListener {
	private int priceBtn = 0;// 0 单月会员，1季度会员，2半年会员，3全年会员
	private int priceBtn2 = 0;// 0余额 1 支付宝，2微信，3银联
	private Button pay_button1;
	private VipBuyInfo VIPinfo;
	private WeixinInfo weixininfo;
	private PayvipInfo payVIPInfo;

	private RadioButton payradioButton1, payradioButton2, payradioButton3,
			payradioButton4;
	private RadioButton r[] = {};
	private RadioButton radioButton0_0, radioButton1_1, radioButton2_2,
			radioButton3_3;
	private RadioButton rs[] = {};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.payvipactivity);
		pay_button1 = (Button) findViewById(R.id.pay_button1);
		payradioButton1 = (RadioButton) findViewById(R.id.payradioButton1);
		payradioButton2 = (RadioButton) findViewById(R.id.payradioButton2);
		payradioButton3 = (RadioButton) findViewById(R.id.payradioButton3);
		payradioButton4 = (RadioButton) findViewById(R.id.payradioButton4);
		radioButton0_0 = (RadioButton) findViewById(R.id.radioButton0_0);
		radioButton1_1 = (RadioButton) findViewById(R.id.radioButton1_1);
		radioButton2_2 = (RadioButton) findViewById(R.id.radioButton2_2);
		radioButton3_3 = (RadioButton) findViewById(R.id.radioButton3_3);
		r = new RadioButton[] { payradioButton1, payradioButton2,
				payradioButton3, payradioButton4 };
		rs = new RadioButton[] { radioButton0_0, radioButton1_1,
				radioButton2_2, radioButton3_3 };
		pay_button1.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.pay_button1:
			getPrice("" + priceBtn, "" + priceBtn2);
			break;
		}

	}

	// 支付套餐
	public void radioButtons(View v) {
		for (int i = 0; i < r.length; i++) {
			if (v.getId() == r[i].getId()) {
				r[i].setChecked(true);
				priceBtn = i;
			} else {
				r[i].setChecked(false);
			}
		}
	}

	// 支付方式
	public void radioButton1(View v) {
		for (int i = 0; i < rs.length; i++) {
			if (v.getId() == rs[i].getId()) {
				rs[i].setChecked(true);
				priceBtn2 = i;
			} else {
				rs[i].setChecked(false);
			}
		}
	}

	// 支付宝支付
	private void getPrice(String vipType, String type) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("VIPType", vipType);
		requestParams.add("PayType", type);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/vipbuyinfo",
				requestParams, new ZhifubaoHandler());
	}

	private class ZhifubaoHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			System.out.println("正在加载数据");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				VIPinfo = new VipBuyInfo(response.toString());// 订单(Recharge=id=随机数)
				if (response.getString("SubmitResult").equals("1")) {
					System.out.println(priceBtn);
					int price = 0;
					if (priceBtn == 0) {
						price = 8;
					} else if (priceBtn == 1) {
						price = 22;
					} else if (priceBtn == 2) {
						price = 42;
					} else if (priceBtn == 3) {
						price = 80;
					}
					if (priceBtn2 == 0) {
						getYue("UserVIP=" + VIPinfo.getVIPID() + "="
								+ SignUtils.getOutTradeNo(), "购买会员",
								getMyInfo().getUserID() + "购买会员", "" + price);
					} else if (priceBtn2 == 1) {

						pay("UserVIP=" + VIPinfo.getVIPID() + "="
								+ SignUtils.getOutTradeNo(), "购买会员",
								getMyInfo().getUserID() + "购买会员", "" + price);
					} else if (priceBtn2 == 2) {
						getWeixinPrice("UserVIP=" + VIPinfo.getVIPID() + "="
								+ SignUtils.getOutTradeNo(), "购买会员",
								getMyInfo().getUserID() + "购买会员", "" + price);
					} else if (priceBtn2 == 3) {
						getYinLianPrice("UserVIP=" + VIPinfo.getVIPID() + "="
								+ SignUtils.getOutTradeNo(), "购买会员",
								getMyInfo().getUserID() + "购买会员", "" + price);
					}
				} else {
					Toast.makeText(PayVIPActivity.this,
							response.getString("SubmitResult"),
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void getDan(View v) {
		radioButton();
	}

	public void getJi(View v) {
		priceBtn = 1;
	}

	public void getBan(View v) {
		priceBtn = 2;
	}

	public void getQuan(View v) {
		priceBtn = 3;
	}

	public String radioButton() {
		for (int i = 0; i < r.length; i++) {
			if (r[i].isChecked()) {
				priceBtn = i;
			} else {
				r[i].setChecked(false);
			}
		}
		return "" + priceBtn;
	}

	public String radioButtons() {
		for (int i = 0; i < rs.length; i++) {
			if (rs[i].isChecked()) {
				priceBtn2 = i;
			} else {
				rs[i].setChecked(false);
			}
		}
		return "" + priceBtn2;
	}

}
