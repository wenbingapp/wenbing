package com.wenbingwang.wenbingapp;

import java.text.DecimalFormat;

import com.loopj.android.http.AsyncHttpClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.bean.MyInfo;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class BaseActivity extends FragmentActivity {
	protected Toast toast;
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader;
	protected AsyncHttpClient asyncHttpClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		asyncHttpClient = ((MyApplication) getApplication())
				.getAsyncHttpClient();
		imageLoader = ImageLoader.getInstance();
		ColorDrawable colorDrawable = new ColorDrawable(0x969696);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(colorDrawable)
				.showImageOnFail(colorDrawable).cacheInMemory(true)
				.cacheOnDisk(true).displayer(new BitmapDisplayer() {
					@Override
					public void display(Bitmap bitmap, ImageAware imageAware,
							LoadedFrom loadedFrom) {
						// TODO Auto-generated method stub
						imageAware.setImageBitmap(bitmap);
					}
				}).build();
	}

	public void gh_back(View v) {
		finish();
	}

	protected void showToast(String s) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
		toast.show();
	}

	protected String doble(String a) {
		DecimalFormat df = new DecimalFormat("0");
		double price = Double.parseDouble(a);
		return df.format(price);
	}

	protected String dobles(String a) {
		DecimalFormat df = new DecimalFormat("0.00");
		double price = Double.parseDouble(a);
		return df.format(price);
	}

	public MyInfo getMyInfo() {
		if (((MyApplication) getApplication()).getMyInfo() != null) {
			return ((MyApplication) getApplication()).getMyInfo();
		} else {
			new AlertDialog.Builder(this).setTitle("登录").setMessage("您的账号还未登录")
					.setPositiveButton("确定", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(BaseActivity.this,
									MainActivity.class);
							intent.putExtra("login", "true");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
						}
					}).show();
		}
		return null;
	}
	
	protected void checkUpdate() {
		if (((MyApplication) getApplication()).checkupdate.equals("1")) {
			if (((MyApplication) getApplication()).forceupdate.equals("1")) {
				new AlertDialog.Builder(this)
						.setTitle("更新提示")
						.setMessage("有新的版本发布,是否更新?")
						.setPositiveButton(
								"现在更新",
								new update(
										((MyApplication) getApplication()).url))
						.setNegativeButton("退出程序",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										finish();
									}
								}).show();
			} else {
				new AlertDialog.Builder(this)
						.setTitle("更新提示")
						.setMessage("有新的版本发布,是否更新?")
						.setPositiveButton(
								"现在更新",
								new update(
										((MyApplication) getApplication()).url))
						.setNegativeButton("稍后更新", null).show();
			}
		}
	}

	class update implements DialogInterface.OnClickListener {
		private String ss;

		public update(String s) {
			ss = s;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			DownloadManager dManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
			Uri uri = Uri.parse(ss);
			DownloadManager.Request request = new Request(uri);
			request.setDestinationInExternalPublicDir("download", "叮当医生.apk");
			request.setDescription("叮当医生");
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
			request.setMimeType("application/vnd.android.package-archive");
			request.allowScanningByMediaScanner();
			request.setVisibleInDownloadsUi(true);
			long refernece = dManager.enqueue(request);
			SharedPreferences sPreferences = getSharedPreferences(
					"downloadcomplete", 0);
			sPreferences.edit().putLong("refernece", refernece).commit();
			finish();
		}

	}
}
