package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.HomeDocterOutpatients;
import com.wenbingwang.mywedgit.MyImageView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class XiangqingActivity extends BaseActivity {

	private ImageView imageView1;
	private TextView t1, t2, t3, t4, x1, x2, x3, x4;
	private LinearLayout pic;
	private String[] s;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xiangqing);
		HomeDocterOutpatients h = (HomeDocterOutpatients) getIntent()
				.getSerializableExtra("doc_xiangqing");
		imageView1 = (ImageView) findViewById(R.id.imageView1);
		pic = (LinearLayout) findViewById(R.id.pic);
		t1 = (TextView) findViewById(R.id.textView1_1);
		t2 = (TextView) findViewById(R.id.textView2);
		t3 = (TextView) findViewById(R.id.textView3);
		t4 = (TextView) findViewById(R.id.textView4);
		x1 = (TextView) findViewById(R.id.x_1);
		x2 = (TextView) findViewById(R.id.x_2);
		x3 = (TextView) findViewById(R.id.x_3);
		x4 = (TextView) findViewById(R.id.x_4);
		imageLoader.displayImage(h.getDoctorHeadImage(), imageView1, options);
		t1.setText("医生：" + h.getDoctorNickname());
		t2.setText("科室：" + h.getDetailDescription());
		t3.setText("医院：" + h.getDoctorHospital());
		t4.setText("职位：" + h.getDoctorLevel());
		x1.setText("年龄：" + h.getAge() + "  性别：" + h.getSex());
		x2.setText("预约时间：" + h.getOutpatientBeginTime() + "至"
				+ h.getOutpatientEndTime());
		x3.setText("联系电话：" + h.getTelephone());
		x4.setText(h.getDetailDescription());
		s = h.getImageArray().split(",");
		for (String string : s) {
			final String ss = string;
			MyImageView imageView = new MyImageView(this);
			imageView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(XiangqingActivity.this,
							PicActivity.class);
					intent.putExtra("pic", ss);
					startActivity(intent);
				}
			});
			pic.addView(imageView, new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));
			imageLoader.displayImage(string, imageView, options);
		}
	}

}
