package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class GetPassWord extends BaseActivity implements OnClickListener {

	private Button get_btn;
	private EditText get_editext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.getpassword1_activity);
		get_btn = (Button) findViewById(R.id.get_btn);
		get_editext = (EditText) findViewById(R.id.get_editext);
		get_btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.get_btn:
			setIdea();
			break;

		}
	}

	private void setIdea() {
		RequestParams requestParams = new RequestParams();
		if (get_editext.getText().toString().equals("")
				|| get_editext.getText().toString().length() != 11) {
			showToast("请输入您的手机号或邮箱");
		} else {
			requestParams.add("UserName", get_editext.getText().toString());
		}

		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/gzfindinfo",
				requestParams, new LoginHandler());
	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					Intent intent = new Intent(GetPassWord.this,
							GetPassWord1.class);
					intent.putExtra("phone", get_editext.getText().toString());
					startActivity(intent);
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
