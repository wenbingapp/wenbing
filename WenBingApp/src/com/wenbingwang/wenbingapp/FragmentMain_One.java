﻿package com.wenbingwang.wenbingapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.OnChatReceiveListener;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.wenbingwang.bean.PayResult_gh;
import com.wenbingwang.bean.ProblemInfo;
import com.wenbingwang.bean.SendProblemItemInfo;

import de.greenrobot.event.EventBus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class FragmentMain_One extends BaseFragment implements
		OnRefreshListener, OnItemClickListener {
	private View rootView;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ListView listView;
	private Adapter adapter;
	private int page = 1;
	private TextView footer;
	private boolean hasmore;
	private String head;
	private String url;

	public FragmentMain_One(String head, String url) {
		this.head = head;
		this.url = url;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_one_listview,
					container, false);
			swipeRefreshLayout = (SwipeRefreshLayout) rootView
					.findViewById(R.id.swipe_container);
			swipeRefreshLayout.setOnRefreshListener(this);
			listView = (ListView) rootView.findViewById(R.id.listView1);
			listView.setOnItemClickListener(this);
			adapter = new Adapter();
			listView.setAdapter(adapter);
			addFooter(listView, R.layout.fragment_two_doclist_bottom);
			onRefresh();
		}
		EventBus.getDefault().register(this);
		return rootView;
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(getActivity()).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("Page", page + "");
		requestParams.add("QuestionType", "0");
		hasmore = true;
		asyncHttpClient.post(getActivity(), url, requestParams,
				new MyHandler(s));
	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					ProblemInfo problemInfo = new ProblemInfo(response
							.getJSONObject(i).toString(), head);
					adapter.getList().add(problemInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private class Adapter extends BaseAdapter {
		private ArrayList<ProblemInfo> list;

		public Adapter() {
			list = new ArrayList<ProblemInfo>();
		}

		public ArrayList<ProblemInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(getActivity()).inflate(
						R.layout.fragment_one_item, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.textView1);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.textView2);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.textView3);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.textView4);
				viewHolder.textView5 = (TextView) convertView
						.findViewById(R.id.textView5);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				viewHolder.state = (TextView) convertView
						.findViewById(R.id.state);
				viewHolder.dot = (View) convertView.findViewById(R.id.dot);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText(list.get(position).getQuestionTitle());
			viewHolder.textView2.setText("医生："
					+ list.get(position).getSenderNickname());
			if (list.get(position).getSex().equals("1")) {
				viewHolder.textView3.setText("性别：男");
			} else {
				viewHolder.textView3.setText("性别：女");
			}
			if (list.get(position).getFormStatus() == null) {
				viewHolder.state.setText("回复："
						+ time(list.get(position).getAccepterTime()));
			} else if (list.get(position).getFormStatus().equals("0")) {
				viewHolder.state.setText("订单状态：待付款");
			} else if (list.get(position).getFormStatus().equals("1")) {
				viewHolder.state.setText("订单状态：待回复");
			} else if (list.get(position).getFormStatus().equals("2")) {
				viewHolder.state.setText("订单状态：待评价");
			} else if (list.get(position).getFormStatus().equals("3")) {
				viewHolder.state.setText("订单状态：已完成");
			}
			viewHolder.textView4.setText("年龄：" + list.get(position).getAge()
					+ "岁");
			viewHolder.textView5.setText("提交："
					+ time(list.get(position).getSubmitTime()));
			viewHolder.dot.setVisibility(View.INVISIBLE);
			imageLoader.displayImage(list.get(position).getSenderHeadImage(),
					viewHolder.imageView, options);
			return convertView;
		}

		private class ViewHolder {
			public TextView textView1;
			public TextView textView2;
			public TextView textView3;
			public TextView textView4;
			public TextView textView5;
			public TextView state;
			private ImageView imageView;
			private View dot;
		}

		private String time(String nowTime) {
			SimpleDateFormat formatter2 = new SimpleDateFormat(
					"yyyy-MM-dd hh:mm");
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd");
			SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
			Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
			String str1 = formatter2.format(curDate);
			String str = formatter.format(curDate);
			String dayStr = formatter1.format(curDate);
			// 当前时间
			String now = nowTime.substring(0, 10);// 输入的年月日
			String now1 = nowTime.substring(0, 7);// 输入的年月
			String nowyestoday = nowTime.substring(8, 10);// 输入的日
			String today = str1.substring(0, 10);// 当前的年月日
			String today2 = str1.substring(0, 7);// 当前的年月
			String today1 = str1.substring(8, 10);
			System.out.println(Integer.valueOf(today1).intValue());
			int yestodays = (Integer.parseInt(today1)) - 1;
			String yes = "" + yestodays;
			if (now.equals(today)) {
				return "今天 ";
			} else if (nowyestoday.equals(yes) && now1.equals(today2)) {
				return "昨天 ";
			} else {
				return str;
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view,
			final int position, long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		} else {
			if (adapter.getList().get(position).getFormStatus() == null
					|| adapter.getList().get(position).getFormStatus()
							.equals("1")) {
				Intent intent = new Intent(getActivity(), MessageActivity.class);
				intent.putExtra("probleminfo", adapter.getList().get(position));
				startActivity(intent);
			} else if (adapter.getList().get(position).getFormStatus()
					.equals("0")) {
				new AlertDialog.Builder(getActivity()).setTitle("图文资讯")
						.setMessage("您的订单尚未付款，是否对订单进行付款？")
						.setPositiveButton("付款", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Intent intent = new Intent();
								intent.setClass(getActivity(),
										PayActivity.class);
								intent.putExtra("oderinfo", "图文资讯购买了");
								intent.putExtra("orderid",
										adapter.getList().get(position)
												.getQuestionID().split("_")[1]);
								intent.putExtra("odername", "图文资讯");
								intent.putExtra("oderhead", "ImageWord");
								intent.putExtra("oderprice", adapter.getList()
										.get(position).getImageWordPrice());
								intent.putExtra("ordershow", adapter.getList()
										.get(position).getImageWordPrice()
										+ " 元");
								intent.putExtra("ordertitle", adapter.getList()
										.get(position).getSenderNickname()
										+ "-图文资讯服务");
								startActivity(intent);
							}
						}).setNegativeButton("取消", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						}).show();

			} else if (adapter.getList().get(position).getFormStatus()
					.equals("2")) {
				new AlertDialog.Builder(getActivity()).setTitle("图文资讯")
						.setMessage("您的订单尚已经关闭，是否对订单进行评价？")
						.setPositiveButton("评价", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						}).setNegativeButton("浏览", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(getActivity(),
										MessageActivity.class);
								intent.putExtra("close", "");
								intent.putExtra("probleminfo", adapter
										.getList().get(position));
								startActivity(intent);
							}
						}).show();
			} else if (adapter.getList().get(position).getFormStatus()
					.equals("3")) {

			}

		}
	}

	public void onEventMainThread(SendProblemItemInfo event) {
		for (int i = 0; i < adapter.list.size(); i++) {
			if (adapter.list.get(i).getQuestionID()
					.equals(event.getQuestionID())) {
				if (FragmentMain_Two.adapter != null
						&& head.equals(event.getQuestionID().split("_")[0])) {
					FragmentMain_Two.adapter.getList().add(0,
							adapter.list.get(i));
				}
				adapter.list.remove(i);
				adapter.notifyDataSetChanged();
				break;
			}
		}
	}

	public void onEventMainThread(PayResult_gh event) {
		String s = event.state;
		if (s.equals("0")) {
			onRefresh();
		} else if (s.equals("1")) {
			return;
		} else if (s.equals("2")) {
			return;
		}
	}

}
