﻿package com.wenbingwang.wenbingapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.CommentsInfo;
import com.wenbingwang.bean.DepartmentInfo;
import com.wenbingwang.bean.DocInfo;
import com.wenbingwang.bean.HomeInfo;
import com.wenbingwang.bean.MyInfo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

public class Activity5_item extends BaseActivity implements OnRefreshListener,
		OnItemClickListener, OnClickListener {

	private TextView textView1;
	private TextView textView2;
	private TextView textView3;
	private TextView textView4;
	private RatingBar ratingBar;
	private SwipeRefreshLayout swipeRefreshLayout;
	private Adapter adapter;
	private ListView listView;
	private TextView footer;
	private int page = 1;
	private boolean hasmore;
	private String UserID;
	private TextView title;

	private TextView price1;
	private TextView price2;
	private TextView buy;
	private double p1;
	private double p2;
	private PopupWindow popupWindow;
	private ListView roomList;
	private Adapter1 adapter1;
	private String docname;

	private String SubmitPrice;

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity5_item);
		docname = getIntent().getStringExtra("docname");
		p1 = Double.parseDouble(getIntent().getStringExtra("p1"));
		p2 = Double.parseDouble(getIntent().getStringExtra("p2"));
		price1 = (TextView) findViewById(R.id.price1);
		price2 = (TextView) findViewById(R.id.price2);
		buy = (TextView) findViewById(R.id.buy);
		adapter1 = new Adapter1();
		price1.setText("￥" + getIntent().getStringExtra("p1") + "/周 ￥"
				+ getIntent().getStringExtra("p2") + "/月");
		price2.setText("会员：￥" + Activity5.getVipPrice(p1) + "/周 ￥"
				+ Activity5.getVipPrice(p2) + "/月");
		buy.setText("立即购买");
		buy.setOnClickListener(this);
		textView1 = (TextView) findViewById(R.id.activity5_name1);
		textView2 = (TextView) findViewById(R.id.activity5_man);
		textView3 = (TextView) findViewById(R.id.gh1);
		textView4 = (TextView) findViewById(R.id.gh2);
		ratingBar = (RatingBar) findViewById(R.id.ratingBar1);
		textView1.setText(getIntent().getStringExtra("getHomeDoctorWeekPrice"));
		textView2.setText(getIntent().getStringExtra("getHomeDoctorBuys"));
		textView3.setText(getIntent().getStringExtra("getImageWordPrice"));
		textView4.setText(getIntent().getStringExtra("getTelephonePrice"));
		ratingBar.setRating(Float.parseFloat(getIntent().getStringExtra(
				"getHomeDoctorValue")));
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		UserID = getIntent().getStringExtra("UserID");
		title = (TextView) findViewById(R.id.title);
		title.setText(getIntent().getStringExtra("title"));
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", UserID);
		requestParams.add("Page", page + "");
		requestParams.add("CommentType", "1");
		hasmore = true;
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/pjlistinfo",
				requestParams, new MyHandler(s));

	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					CommentsInfo indentListInfo = new CommentsInfo(response
							.getJSONObject(i).toString());
					adapter.getList().add(indentListInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);

		}

	}

	private class Adapter extends BaseAdapter {
		private ArrayList<CommentsInfo> list;

		public Adapter() {
			list = new ArrayList<CommentsInfo>();
		}

		public ArrayList<CommentsInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(Activity5_item.this).inflate(
						R.layout.activity5_item2_item, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.activity5_item2_textView1);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.activity5_item2_textView2);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.activity5_item2_textView3);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText("患者："
					+ list.get(position).getNickname() + " ("
					+ list.get(position).getCommentValue() + "星)");
			viewHolder.textView2.setText("时间："
					+ time(list.get(position).getTime()));
			viewHolder.textView3.setText("评价："
					+ list.get(position).getCommentDescription());
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
	}

	private String time(String nowTime) {
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd hh:mm");
		SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str1 = formatter2.format(curDate);
		String str = formatter.format(curDate);
		String dayStr = formatter1.format(curDate);
		// 当前时间
		String now = nowTime.substring(0, 10);// 输入的年月日
		String now1 = nowTime.substring(0, 7);// 输入的年月
		String nowyestoday = nowTime.substring(8, 10);// 输入的日
		String today = str1.substring(0, 10);// 当前的年月日
		String today2 = str1.substring(0, 7);// 当前的年月
		String today1 = str1.substring(8, 10);
		System.out.println(Integer.valueOf(today1).intValue());
		int yestodays = (Integer.parseInt(today1)) - 1;
		String yes = "" + yestodays;
		if (now.equals(today)) {
			return "今天 " + dayStr;
		} else if (nowyestoday.equals(yes) && now1.equals(today2)) {
			return "昨天 " + dayStr;
		} else {
			return str;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (parent == listView) {
			if (position == adapter.getList().size()) {
				if (hasmore) {
					page++;
					load(1);
				}
			}
		}
		if (parent == roomList) {
			RequestParams requestParams = new RequestParams();
			if (getMyInfo() == null) {
				return;
			}
			requestParams.put("UserID", getMyInfo().getUserID());
			requestParams.put("DoctorID", UserID);
			if (position == 0) {
				requestParams.put("HomeDoctorType", "0");
				if (getMyInfo().getVip().equals("1")) {
					requestParams.put("HomeDoctorPrice", p1 + "");
					SubmitPrice = p1 + "";
				} else {
					requestParams.put("HomeDoctorPrice",
							Activity5.getVipPrice(p1) + "");
					SubmitPrice = Activity5.getVipPrice(p1) + "";
				}
			} else {
				requestParams.put("HomeDoctorType", "1");
				if (getMyInfo().getVip().equals("1")) {
					requestParams.put("HomeDoctorPrice", p2 + "");
					SubmitPrice = p2 + "";
				} else {
					requestParams.put("HomeDoctorPrice",
							Activity5.getVipPrice(p2) + "");
					SubmitPrice = Activity5.getVipPrice(p2) + "";
				}
			}
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/jtysbuyinfo",
					requestParams, new PayHandler());
		}
	}

	private class PayHandler extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			// Initiated the request
			progressDialog = ProgressDialog.show(Activity5_item.this, "购买",
					"订单生成中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				if (response.getString("SubmitResult").equals("1")) {
					Intent intent = new Intent();
					intent.setClass(Activity5_item.this, PayActivity.class);
					intent.putExtra("oderinfo", "家庭医生购买了");
					intent.putExtra("orderid",
							response.getString("HomeDoctorID"));
					intent.putExtra("odername", "家庭医生");
					intent.putExtra("oderhead", "HomeDoctor");
					intent.putExtra("oderprice", SubmitPrice);
					intent.putExtra("ordershow", SubmitPrice + " 元");
					intent.putExtra("ordertitle", docname + "-家庭医生服务");
					startActivity(intent);
				} else {
					showToast(response.getString("SubmitDescription"));
				}
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.cancle:
			popupWindow.dismiss();
			break;
		case R.id.buy:
			showPopup(v, R.layout.popupwin_1);
			break;
		default:
			break;
		}
	}

	private void showPopup(View v, int res) {
		if (popupWindow == null) {
			View inflateview = LayoutInflater.from(this).inflate(res, null);
			popupWindow = new PopupWindow(inflateview);
			popupWindow.setWidth(getWindowManager().getDefaultDisplay()
					.getWidth() / 4 * 3);
			popupWindow.setHeight(getWindowManager().getDefaultDisplay()
					.getHeight() / 4 * 3);
			popupWindow.setFocusable(true);
			popupWindow.setBackgroundDrawable(new PaintDrawable());
			popupWindow.setAnimationStyle(R.style.popup_anim);
			popupWindow.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
					WindowManager.LayoutParams lp = getWindow().getAttributes();
					lp.alpha = 1f;
					getWindow().setAttributes(lp);
				}
			});
			roomList = (ListView) inflateview.findViewById(R.id.listView1);
			roomList.setAdapter(adapter1);
			roomList.setOnItemClickListener(this);
			((Button) inflateview.findViewById(R.id.cancle))
					.setOnClickListener(this);
		}
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = 0.4f;
		getWindow().setAttributes(lp);
		popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	private class Adapter1 extends BaseAdapter {
		private ArrayList<String> list;

		public Adapter1() {
			list = new ArrayList<String>();
			list.add("一周");
			list.add("一月");
		}

		public ArrayList<String> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(Activity5_item.this).inflate(
					R.layout.fragment_two_room, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText(list.get(position));
			return convertView;
		}

	}

}
