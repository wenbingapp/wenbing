package com.wenbingwang.wenbingapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.HomeDocterPhones;
import com.wenbingwang.bean.TelephoneInfo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class TelephoneActivity extends BaseActivity implements
		OnRefreshListener, OnItemClickListener {
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private TextView footer;
	private boolean hasmore;
	private ArrayList<TelephoneInfo> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.telephone);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("Page", page + "");
		hasmore = true;
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/hzkjdhlistinfo", requestParams,
				new MyHandler(s));

	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");

		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					TelephoneInfo problemInfo = new TelephoneInfo(response
							.getJSONObject(i).toString());
					adapter.getList().add(problemInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);

		}

	}

	private class Adapter extends BaseAdapter {

		public Adapter() {
			list = new ArrayList<TelephoneInfo>();
		}

		public ArrayList<TelephoneInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(TelephoneActivity.this)
						.inflate(R.layout.telephone_item, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.textView2);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.textView5);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.textView1);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.textView6);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			if ((list.get(position).getDoctorNickname()).equals("")) {
				viewHolder.textView1.setText("医生：无");
			} else {
				viewHolder.textView1.setText("医生："
						+ list.get(position).getDoctorNickname());

			}
			// sex:1 男 2 女
			if ((list.get(position).getFormStatus()).equals("0")) {
				viewHolder.textView2.setText("订单状态:待付款");
			} else if ((list.get(position).getFormStatus()).equals("1")) {
				viewHolder.textView2.setText("订单状态:待通话");
			} else if ((list.get(position).getFormStatus()).equals("2")) {
				viewHolder.textView2.setText("订单状态:待评价");
			} else if ((list.get(position).getFormStatus()).equals("3")) {
				viewHolder.textView2.setText("订单状态:已完成");
			}
			viewHolder.textView3.setText("科室："
					+ list.get(position).getDoctorDepartment());
			viewHolder.textView4.setText("电话："
					+ list.get(position).getUserTelephone());
			imageLoader.displayImage(list.get(position).getDoctorHeadImage(),
					viewHolder.imageView, options);
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
		public TextView textView4;
		private ImageView imageView;
	}

	public void onItemClick(AdapterView<?> parent, View view,
			final int position, long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		} else {

			if ((list.get(position).getFormStatus()).equals("0")) {
				new AlertDialog.Builder(this)
						.setTitle("快捷电话")
						.setMessage("该订单尚未付款,是否进行付款")
						.setPositiveButton("付款",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(TelephoneActivity.this,
												PayActivity.class);
										intent.putExtra("orderid",
												list.get(position)
														.getTelephoneID());
										intent.putExtra("oderprice",
												list.get(position)
														.getTelephonePrice());
										intent.putExtra("oderhead", "Telephone");
										intent.putExtra("odername", "快捷电话");
										intent.putExtra("oderinfo",
												"向医生进行了快捷电话");
										if ((list.get(position)
												.getTelephoneType())
												.equals("0")) {
											intent.putExtra(
													"ordershow",
													"时间：一周        "
															+ "金额："
															+ list.get(position)
																	.getTelephonePrice()
															+ "元");
										} else if ((list.get(position)
												.getTelephoneType())
												.equals("1")) {
											intent.putExtra(
													"ordershow",
													"时间：一月        "
															+ "金额："
															+ list.get(position)
																	.getTelephonePrice()
															+ "元");
										}
										intent.putExtra("ordertitle", "快捷电话");
										startActivity(intent);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("1")) {
				new AlertDialog.Builder(this).setTitle("快捷电话")
						.setMessage("您已付款，请耐心等待医生回电")
						.setPositiveButton("确定", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("2")) {
				new AlertDialog.Builder(this)
						.setTitle("快捷电话")
						.setMessage("您尚未对该医生评价，请前往评价")
						.setPositiveButton("确定",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(TelephoneActivity.this,
												PingjiaActivity.class);
										intent.putExtra("DoctorID",
												list.get(position)
														.getDoctorID());
										intent.putExtra("TelephoneID", list
												.get(position).getTelephoneID());
										intent.putExtra("TelephoneType", "3");
										intent.putExtra("DoctorDepartment",
												list.get(position)
														.getDoctorDepartment());
										intent.putExtra("DoctorNickname", list
												.get(position)
												.getDoctorNickname()
												+ " ");
										startActivity(intent);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("3")) {
				new AlertDialog.Builder(this)
						.setTitle("快捷电话")
						.setMessage(
								"您购买" + list.get(position).getDoctorNickname()
										+ "医生的快捷电话服务已经结束")
						.setPositiveButton("确定", null).show();
			}

		}
	}
}
