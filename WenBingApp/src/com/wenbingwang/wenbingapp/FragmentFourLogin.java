package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.mywedgit.ACache;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FragmentFourLogin extends BaseFragment implements
		OnRefreshListener, OnClickListener {
	private View rootView;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ImageView imageView;
	private TextView name;
	private TextView address;
	private TextView vip;
	private RelativeLayout relativeLayout;
	private Button button1, button2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_four_login,
					container, false);
			swipeRefreshLayout = (SwipeRefreshLayout) rootView
					.findViewById(R.id.swipe_container);
			swipeRefreshLayout.setOnRefreshListener(this);
			imageView = (ImageView) rootView.findViewById(R.id.imageView1);
			address = (TextView) rootView.findViewById(R.id.textView4);
			name = (TextView) rootView.findViewById(R.id.textView2);
			vip = (TextView) rootView.findViewById(R.id.textView3);
			relativeLayout = (RelativeLayout) rootView
					.findViewById(R.id.myinfo);
			button1 = (Button) rootView.findViewById(R.id.button2_1);
			button2 = (Button) rootView.findViewById(R.id.button2_2);
			relativeLayout.setOnClickListener(this);
			button1.setOnClickListener(this);
			button2.setOnClickListener(this);
		}
		show();
		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/getperinfo", requestParams,
				new MyHandler1());
	}

	private void show() {
		imageLoader.displayImage(getMyInfo().getUserHeadImageUrl(), imageView,
				options);
		vip.setVisibility(getMyInfo().getVip().equals("1") ? View.VISIBLE
				: View.INVISIBLE);
		address.setText(getMyInfo().getAddress());
		name.setText(getMyInfo().getNickName());
	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				getMyInfo().setUserInfo(jsonObject.toString());
				ACache mCache = ACache.get(getActivity());
				mCache.put("logininfo", getMyInfo().toString());
				show();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.myinfo:
			Intent intent1 = new Intent();
			intent1.setClass(getActivity(), UpdataActivity.class);
			startActivity(intent1);
			break;
		case R.id.button2_1:
			Intent intent2 = new Intent();
			intent2.setClass(getActivity(), MyDocActivity.class);
			startActivity(intent2);
			break;
		case R.id.button2_2:
			Intent intent3 = new Intent();
			intent3.setClass(getActivity(), MyOutpatientActivity.class);
			startActivity(intent3);
			break;
		default:
			break;
		}
	}
}
