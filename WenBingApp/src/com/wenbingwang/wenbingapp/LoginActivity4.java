package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity4 extends BaseActivity {
	private Button Login4_ok;
	private ProgressDialog progressDialog;
	private EditText idea_Text1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity4);
		idea_Text1 = (EditText) findViewById(R.id.login4_Text1);
		Login4_ok = (Button) findViewById(R.id.Login4_ok);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("建议");
		progressDialog.setMessage("正在提交");
		Login4_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setIdea();
			}
		});

	}

	private void setIdea() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("Suggestion", idea_Text1.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/ysopininfo",
				requestParams, new IdeaHandler());

	}

	private class IdeaHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					finish();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}
}