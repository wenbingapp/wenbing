package com.wenbingwang.wenbingapp;

import com.loopj.android.http.AsyncHttpClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.bean.MyInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

public class BaseFragment extends Fragment {
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader;
	protected AsyncHttpClient asyncHttpClient;
	protected Toast toast;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		asyncHttpClient = ((MyApplication) activity.getApplication())
				.getAsyncHttpClient();
		imageLoader = ImageLoader.getInstance();
		ColorDrawable colorDrawable = new ColorDrawable(0x969696);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(colorDrawable)
				.showImageOnFail(colorDrawable).cacheInMemory(true)
				.cacheOnDisk(true).displayer(new BitmapDisplayer() {
					@Override
					public void display(Bitmap bitmap, ImageAware imageAware,
							LoadedFrom loadedFrom) {
						// TODO Auto-generated method stub
						imageAware.setImageBitmap(bitmap);
					}
				}).build();
	}

	protected void showToast(String s) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT);
		toast.show();
	}

	public MyInfo getMyInfo() {
		if (((MyApplication) getActivity().getApplication()).getMyInfo() != null) {
			return ((MyApplication) getActivity().getApplication()).getMyInfo();
		} else {
			new AlertDialog.Builder(getActivity().getApplicationContext())
					.setTitle("登录").setMessage("您的账号还未登录")
					.setPositiveButton("确定", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(getActivity(),
									MainActivity.class);
							intent.putExtra("login", "true");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							getActivity().finish();
						}
					}).show();
		}
		return null;
	}
}
