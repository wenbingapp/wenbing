package com.wenbingwang.wenbingapp;

import java.util.ArrayList;
import java.util.Locale;

import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.ECError;
import com.speedtong.sdk.OnChatReceiveListener;
import com.speedtong.sdk.ECDevice.OnECDeviceConnectListener;
import com.speedtong.sdk.ECDevice.OnLogoutListener;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.bean.MyInfo;
import com.wenbingwang.bean.RecvProblemItemInfo;
import com.wenbingwang.mywedgit.ACache;
import com.wenbingwang.wenbingdoc.data.DataProvider;

import de.greenrobot.event.EventBus;

import android.R.bool;
import android.R.integer;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.os.Build;

public class MainActivity extends BaseActivity {
	private ArrayList<Fragment> list;
	private FragmentMain fragmentOne;
	private FragmentTwo fragmentTwo;
	private FragmentThree fragmentThree;
	private FragmentFourUnlogin fragmentFourUnlogin;
	private FragmentFourLogin fragmentFourLogin;
	private RadioGroup radioGroup;
	private int currentFragment = 0;
	private SoundPool soundPool;
	private int soundId;
	private boolean gh = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ACache mCache = ACache.get(this);
		String s = mCache.getAsString("logininfo");
		if (s != null) {
			((MyApplication) getApplication()).setMyInfo(new MyInfo(s));
			((MyApplication) getApplication()).getMyInfo().setUserInfo(s);
		}
		initFragment();
		if (getIntent().getStringExtra("login") != null) {
			((RadioButton) radioGroup.getChildAt(3)).setChecked(true);
		}
	}

	private boolean hasLogin() {
		return ((MyApplication) getApplication()).getMyInfo() != null;
	}

	private void initFragment() {
		soundPool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
		soundId = soundPool.load(this, R.raw.message, 1);
		list = new ArrayList<Fragment>();
		fragmentOne = new FragmentMain();
		fragmentTwo = new FragmentTwo();
		fragmentThree = new FragmentThree();
		fragmentFourUnlogin = new FragmentFourUnlogin();
		fragmentFourLogin = new FragmentFourLogin();
		list.add(fragmentOne);
		list.add(fragmentTwo);
		list.add(fragmentThree);
		list.add(fragmentFourLogin);
		list.add(fragmentFourUnlogin);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int i = 0;
				switch (checkedId) {
				case R.id.radio0:
					i = 0;
					break;
				case R.id.radio1:
					i = 1;
					break;
				case R.id.radio2:
					i = 2;
					break;
				case R.id.radio3:
					i = 3;
					break;
				default:
					break;
				}
				changeFragment(i);
			}
		});
		changeFragment(currentFragment);
		loginDevices();
		// checkUpdate();
		EventBus.getDefault().register(this);
	}

	private void playSound() {
		soundPool.stop(soundId);
		soundPool.play(soundId, 1, 1, 0, 0, 1);
	}

	private void playVibrate() {
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.cancel();
		long[] pattern = { 100, 250, 100, 250 };
		vibrator.vibrate(pattern, -1);
	}

	private void changeFragment(int ids) {
		int id = 0;
		if (ids == 3) {
			if (hasLogin()) {
				id = 3;
			} else {
				id = 4;
			}
		} else {
			id = ids;
		}
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		if (!list.get(id).isAdded()) {
			fragmentTransaction.add(R.id.container, list.get(id), getTag(id));
			fragmentTransaction.commit();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		}
		if (list.get(currentFragment).isAdded()
				&& !list.get(currentFragment).isDetached()) {
			fragmentTransaction.detach(list.get(currentFragment));
		}
		if (list.get(id).isDetached()) {
			fragmentTransaction.attach(list.get(id));
		}
		fragmentTransaction.commit();
		currentFragment = id;
	}

	private String getTag(int id) {
		switch (id) {
		case 0:
			return "咨询";
		case 1:
			return "医生";
		case 2:
			return "陪诊";
		case 3:
			return "我的";
		default:
			return "";
		}
	}

	public void LoginStateChange() {
		// radioGroup.clearCheck();
		// ((RadioButton) radioGroup.getChildAt(3)).setChecked(true);
		loginDevices();
		changeFragment(3);
	}

	public void gh_1(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, LoginActivity1.class);
		startActivity(intent);
	}

	public void gh_2(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, LoginActivity2.class);
		startActivity(intent);
	}

	public void gh_3(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, LoginActivity3.class);
		startActivity(intent);
	}

	public void gh_4(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, LoginActivity4.class);
		startActivity(intent);
	}

	public void gh_5(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, LoginActivity5.class);
		startActivity(intent);
	}

	private void loginDevice(OnECDeviceConnectListener b,
			OnChatReceiveListener c) {
		if (((MyApplication) getApplication()).params != null) {
			((MyApplication) getApplication()).params
					.setOnChatReceiveListener(c);
			((MyApplication) getApplication()).params
					.setOnECDeviceConnectListener(b);
			ECDevice.login(((MyApplication) getApplication()).params);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ECDevice.logout(new OnLogoutListener() {

			@Override
			public void onLogout() {
				// TODO Auto-generated method stub
				showToast("断开");
			}
		});
		EventBus.getDefault().unregister(this);
	}

	public void onEventMainThread(RecvProblemItemInfo event) {

	}

	public void loginDevices() {
		loginDevice(new OnECDeviceConnectListener() {

			@Override
			public void onDisconnect(ECError arg0) {
				// TODO Auto-generated method stub
				if (arg0.errorCode.equals("175004")) {
					ACache mCache = ACache.get(MainActivity.this);
					mCache.remove("logininfo");
					((MyApplication) getApplication()).setMyInfo(null);
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("login", "");
					startActivity(intent);
					finish();
				}
			}

			@Override
			public void onConnect() {
				// TODO Auto-generated method stub
				showToast("连接到聊天服务器");
			}
		}, new OnChatReceiveListener() {

			@Override
			public void OnReceivedReport(ECReport arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void OnReceivedMessage(ECMessage arg0) {
				// TODO Auto-generated method stub
				RecvProblemItemInfo recvProblemItemInfo = new RecvProblemItemInfo(
						arg0.getBody()
								.toString()
								.substring(19,
										arg0.getBody().toString().length() - 1));
				DataProvider.saveProblemItemInfo(
						DataProvider.getinstant(getApplicationContext()),
						recvProblemItemInfo);
				ACache mCache = ACache.get(MainActivity.this);
				if (mCache.getAsString("Vibrate") == null
						|| mCache.getAsString("Vibrate").equals("1")) {
					playVibrate();
				}
				if (mCache.getAsString("Sound") == null
						|| mCache.getAsString("Sound").equals("1")) {
					playSound();
				}

				if (((MyApplication) getApplication()).question == null
						|| !((MyApplication) getApplication()).question
								.equals(recvProblemItemInfo.getQuestionID())) {
					DataProvider.setProblemItemInfoUnread(
							DataProvider.getinstant(getApplicationContext()),
							recvProblemItemInfo.getQuestionID(), true);
				}
				EventBus.getDefault().post(recvProblemItemInfo);
			}

			@Override
			public void OnReceiveGroupNoticeMessage(ECGroupNotice arg0) {
				// TODO Auto-generated method stub
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (gh) {
				finish();
			} else {
				gh = true;
				showToast("请再按一次返回退出应用");
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						gh = false;
					}
				}, 1000);
			}

		}
		return false;

	}

}
