package com.wenbingwang.wenbingapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MyDocActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mydoc_activity);
	}

	public void gh_0(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, TelephoneActivity.class);
		startActivity(intent);
	}

	public void gh_1(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, HomeDoctorFansActivity.class);
		startActivity(intent);
	}

	public void gh_2(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, IndentListActivity2.class);
		intent.putExtra("type", "0");
		intent.putExtra("url", "http://yisheng.wenbing.cn/Info/mfhzlistinfo");
		startActivity(intent);
	}

	public void gh_3(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, HomeDocterFormsActivity.class);
		startActivity(intent);
	}

	public void gh_4(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, IndentListActivity2.class);
		intent.putExtra("type", "1");
		intent.putExtra("url", "http://yisheng.wenbing.cn/Info/fftwhzlistinfo");
		startActivity(intent);
	}

	public void gh_5(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, HomeDoctorPhonesActivity.class);
		startActivity(intent);
	}

	public void gh_6(View v) {
		Intent intent = new Intent();
		intent.setClass(MyDocActivity.this, HomeDoctorOutPatientsActivity.class);
		startActivity(intent);
	}
}
