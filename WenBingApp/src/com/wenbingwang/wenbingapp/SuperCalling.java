package com.wenbingwang.wenbingapp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.DepartmentInfo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.PaintDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;

public class SuperCalling extends BaseActivity implements OnClickListener,
		OnItemClickListener, OnRefreshListener {

	private Button button1;
	private EditText editText1;
	private ProgressDialog progressDialog;
	private Adapter1 adapter1;
	private ListView roomList;
	private PopupWindow popupWindow;
	private String phone;
	private Button button2;
	private SwipeRefreshLayout swipeRefreshLayout;
	private String pageDep;
	private Button button3;
	private String TelephoneID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.callphone_activity);
		editText1 = (EditText) findViewById(R.id.editText1);
		button1 = (Button) findViewById(R.id.button_gh);
		button2 = (Button) findViewById(R.id.button1);
		button2.setOnClickListener(this);
		button3 = (Button) findViewById(R.id.button_2);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("提交");
		progressDialog.setMessage("正在提交请求");
		adapter1 = new Adapter1();
		onRefresh();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			commite();
			break;
		case R.id.cancle:
			popupWindow.dismiss();
			break;
		default:
			break;
		}
	}

	private void commite() {
		if (editText1.getText().toString().equals("")
				|| editText1.length() != 11) {
			showToast("请填正确的手机号码");
			return;
		}
		if (pageDep == null) {
			showToast("请选择提交科室");
			return;
		}
		RequestParams requestParams = new RequestParams();
		if (getMyInfo() == null) {
			return;
		}
		requestParams.put("UserID", getMyInfo().getUserID());
		requestParams.put("TelephoneType", "0");
		requestParams.put("TelephonePrice", "0");
		requestParams.put("DepartmentID", pageDep);
		requestParams.put("DoctorID", "0");
		requestParams.put("UserTelephone", editText1.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/hzkjdhinfo",
				requestParams, new Handler());
	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					TelephoneID = jsonObject.getString("TelephoneID");
					call_phone();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private class Handler2 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					finish();
					showToast("拨打电话成功，请稍后");
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	public void call_phone() {
		RequestParams requestParams = new RequestParams();
		requestParams.put("TelephoneID", TelephoneID);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/hzbddhinfo",
				requestParams, new Handler2());
	}

	public void gh_getroom(View v) {
		showPopup(v, R.layout.popupwin_1);
	}

	private void loadDepartment() {
		asyncHttpClient.get(this, "http://yisheng.wenbing.cn/Info/ksinfo",
				new DepHandler());
	}

	private class DepHandler extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				adapter1.getList().clear();
				adapter1.notifyDataSetChanged();
				DepartmentInfo departmentInfos = new DepartmentInfo();
				departmentInfos.setDepartmentID("0");
				departmentInfos.setDepartmentName("所有科室");
				adapter1.getList().add(departmentInfos);
				for (int i = 0; i < response.length(); i++) {
					DepartmentInfo departmentInfo = new DepartmentInfo();
					departmentInfo.setDepartmentID(response.getJSONObject(i)
							.getString("DepartmentID"));
					departmentInfo.setDepartmentName(response.getJSONObject(i)
							.getString("DepartmentName"));
					adapter1.getList().add(departmentInfo);
				}
				adapter1.notifyDataSetChanged();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private class Adapter1 extends BaseAdapter {
		private ArrayList<DepartmentInfo> list;

		public Adapter1() {
			list = new ArrayList<DepartmentInfo>();
		}

		public ArrayList<DepartmentInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(SuperCalling.this).inflate(
					R.layout.fragment_two_room, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText(list.get(position).getDepartmentName());
			return convertView;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (parent == roomList) {
			pageDep = adapter1.getList().get(position).getDepartmentID();
			button3.setText(adapter1.getList().get(position)
					.getDepartmentName());
			popupWindow.dismiss();
			getDocNumber();
		}

	}

	private void showPopup(View v, int res) {
		if (popupWindow == null) {
			View inflateview = LayoutInflater.from(this).inflate(res, null);
			popupWindow = new PopupWindow(inflateview);
			popupWindow.setWidth(this.getWindowManager().getDefaultDisplay()
					.getWidth() / 4 * 3);
			popupWindow.setHeight(this.getWindowManager().getDefaultDisplay()
					.getHeight() / 4 * 3);
			popupWindow.setFocusable(true);
			popupWindow.setBackgroundDrawable(new PaintDrawable());
			popupWindow.setAnimationStyle(R.style.popup_anim);
			popupWindow.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
					WindowManager.LayoutParams lp = getWindow().getAttributes();
					lp.alpha = 1f;
					getWindow().setAttributes(lp);
				}
			});
			roomList = (ListView) inflateview.findViewById(R.id.listView1);
			roomList.setAdapter(adapter1);
			roomList.setOnItemClickListener(this);
			((Button) inflateview.findViewById(R.id.cancle))
					.setOnClickListener(this);
		}
		WindowManager.LayoutParams lp = this.getWindow().getAttributes();
		lp.alpha = 0.4f;
		this.getWindow().setAttributes(lp);
		popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		loadDepartment();
		getDocNumber();
	}

	private void getDocNumber() {
		RequestParams requestParams = new RequestParams();
		requestParams.put("DepartmentID", pageDep == null ? "0" : pageDep);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/zxkjdhinfo",
				requestParams, new Handler1());
	}

	private class Handler1 extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			button1.setText("正在获取在线医生");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				button1.setText(response.getString("OnlineDoctorNumber")
						+ "位医生在线");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

	}

}
