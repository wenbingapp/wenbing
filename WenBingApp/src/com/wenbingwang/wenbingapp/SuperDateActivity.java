package com.wenbingwang.wenbingapp;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wenbingwang.bean.City;
import com.wenbingwang.bean.DepartmentInfo;
import com.wenbingwang.bean.MainTow_Docinfo;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RadioGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class SuperDateActivity extends BaseActivity implements
		OnRefreshListener, OnClickListener, OnItemClickListener,
		OnCheckedChangeListener {
	private CheckBox checkBox1;
	private CheckBox checkBox2;
	private HashMap<Integer, PopupWindow> popupWindows;
	private HashMap<Integer, ListView> listViews;
	private Adapter1_city adapter1_city;
	private Adapter1_Dep adapter1_Dep;
	private ListView listView;
	private Adapter2 adapter2;
	private SwipeRefreshLayout swipeRefreshLayout;
	private String city = "0";
	private String dep = "0";
	private String precity = "0";
	private String predep = "0";
	private boolean hasmore;
	private int pageNum = 1;;
	private TextView footer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.superdate_activity);
		checkBox1 = (CheckBox) findViewById(R.id.checkBox1);
		checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
		checkBox1.setOnCheckedChangeListener(this);
		checkBox2.setOnCheckedChangeListener(this);
		popupWindows = new HashMap<Integer, PopupWindow>();
		listViews = new HashMap<Integer, ListView>();
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		adapter2 = new Adapter2();
		listView.setAdapter(adapter2);
		adapter1_city = new Adapter1_city();
		adapter1_Dep = new Adapter1_Dep();
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(
				R.layout.fragment_two_doclist_bottom, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (parent == listView) {
			if (position == adapter2.getList().size()) {
				if (hasmore) {
					precity = city;
					predep = dep;
					pageNum++;
					loadInfo(1);
				}
			} else {
				Intent intent = new Intent(SuperDateActivity.this,
						Activity5.class);
				intent.putExtra("UserID", adapter2.getList().get(position)
						.getDoctorID());
				startActivity(intent);
			}
		}
		if (parent == listViews.get(R.id.checkBox1)) {
			precity = city;
			city = adapter1_city.getList().get(position).getDepartmentID();
			checkBox1.setText(adapter1_city.getList().get(position)
					.getDepartmentName());
			pageNum = 1;
			loadInfo(0);
			popupWindows.get(R.id.checkBox1).dismiss();
		}
		if (parent == listViews.get(R.id.checkBox2)) {
			predep = dep;
			dep = adapter1_Dep.getList().get(position).getDepartmentID();
			checkBox2.setText(adapter1_Dep.getList().get(position)
					.getDepartmentName());
			pageNum = 1;
			loadInfo(0);
			popupWindows.get(R.id.checkBox2).dismiss();
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		precity = city;
		predep = dep;
		pageNum = 1;
		loadDepartment();
		loadCity();
		loadInfo(0);
	}

	private void loadInfo(int state) {
		hasmore = true;
		RequestParams requestParams = new RequestParams();
		requestParams.add("Page", pageNum + "");
		requestParams.add("DepartmentID", dep);
		requestParams.add("CityID", city);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/mzyyinfo",
				requestParams, new DocHandler(state));
	}

	private void loadDepartment() {
		asyncHttpClient.get(this, "http://yisheng.wenbing.cn/Info/ksinfo",
				new DepHandler());
	}

	private void loadCity() {
		asyncHttpClient.get(this, "http://yisheng.wenbing.cn/Info/ssinfo",
				new CityHandler());
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked == true) {
			if (buttonView.getId() == R.id.checkBox1) {
				showPopup(buttonView, R.layout.popupwin_2, 0);
			} else {
				showPopup(buttonView, R.layout.popupwin_2, 1);
			}
		}
	}

	private void showPopup(final View view, int res, int num) {
		if (popupWindows.get(view.getId()) == null) {
			View inflateview = LayoutInflater.from(this).inflate(res, null);
			inflateview.findViewById(R.id.back).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							popupWindows.get(view.getId()).dismiss();
						}
					});
			ListView l = (ListView) inflateview.findViewById(R.id.listView1);
			l.setOnItemClickListener(this);
			listViews.put(view.getId(), l);
			if (num == 0) {
				l.setAdapter(adapter1_city);
			} else {
				l.setAdapter(adapter1_Dep);
			}
			PopupWindow popupWindow = new PopupWindow(inflateview);
			popupWindows.put(view.getId(), popupWindow);
			popupWindow.setWidth(getWindowManager().getDefaultDisplay()
					.getWidth());
			popupWindow.setHeight(getWindowManager().getDefaultDisplay()
					.getHeight() / 2);
			popupWindow.setFocusable(true);
			popupWindow.setBackgroundDrawable(new PaintDrawable(
					Color.TRANSPARENT));
			popupWindow.setAnimationStyle(R.style.popup_anim);
			popupWindow.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
					((CheckBox) view).setChecked(false);
				}
			});
		}
		popupWindows.get(view.getId()).showAsDropDown(view);
	}

	private class Adapter1_city extends BaseAdapter {
		private ArrayList<DepartmentInfo> list;

		public Adapter1_city() {
			list = new ArrayList<DepartmentInfo>();
		}

		public ArrayList<DepartmentInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(SuperDateActivity.this).inflate(
					R.layout.fragment_two_room, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText(list.get(position).getDepartmentName());
			return convertView;
		}

	}

	private class Adapter1_Dep extends BaseAdapter {
		private ArrayList<DepartmentInfo> list;

		public Adapter1_Dep() {
			list = new ArrayList<DepartmentInfo>();
		}

		public ArrayList<DepartmentInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(SuperDateActivity.this).inflate(
					R.layout.fragment_two_room, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText(list.get(position).getDepartmentName());
			return convertView;
		}

	}

	private class Adapter2 extends BaseAdapter {
		private ArrayList<MainTow_Docinfo> list;

		public Adapter2() {
			list = new ArrayList<MainTow_Docinfo>();
		}

		public ArrayList<MainTow_Docinfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(SuperDateActivity.this)
						.inflate(R.layout.fragment_two_doc, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.textView1);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.textView2);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.textView3);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.textView4);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText(list.get(position).getDoctorName());
			viewHolder.textView2
					.setText(list.get(position).getDoctorHospital());
			viewHolder.textView3.setText(list.get(position).getDoctorAbility());
			viewHolder.textView4.setText(list.get(position).getDoctorLevel());
			imageLoader.displayImage(
					list.get(position).getDoctorHeadImageUrl(),
					viewHolder.imageView, options);
			return convertView;
		}

		private class ViewHolder {
			public TextView textView1;
			public TextView textView2;
			public TextView textView3;
			public TextView textView4;
			private ImageView imageView;
		}

	}

	private class DepHandler extends JsonHttpResponseHandler {

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				adapter1_Dep.getList().clear();
				adapter1_Dep.notifyDataSetChanged();
				DepartmentInfo departmentInfos = new DepartmentInfo();
				departmentInfos.setDepartmentID("0");
				departmentInfos.setDepartmentName("所有科室");
				adapter1_Dep.getList().add(departmentInfos);
				for (int i = 0; i < response.length(); i++) {
					DepartmentInfo departmentInfo = new DepartmentInfo();
					departmentInfo.setDepartmentID(response.getJSONObject(i)
							.getString("DepartmentID"));
					departmentInfo.setDepartmentName(response.getJSONObject(i)
							.getString("DepartmentName"));
					adapter1_Dep.getList().add(departmentInfo);
				}
				adapter1_Dep.notifyDataSetChanged();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

	}

	private class CityHandler extends JsonHttpResponseHandler {

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				adapter1_city.getList().clear();
				adapter1_city.notifyDataSetChanged();
				DepartmentInfo departmentInfos = new DepartmentInfo();
				departmentInfos.setDepartmentID("0");
				departmentInfos.setDepartmentName("所有城市");
				adapter1_city.getList().add(departmentInfos);
				for (int i = 0; i < response.length(); i++) {
					DepartmentInfo departmentInfo = new DepartmentInfo();
					departmentInfo.setDepartmentID(response.getJSONObject(i)
							.getString("CityID"));
					departmentInfo.setDepartmentName(response.getJSONObject(i)
							.getString("CityName"));
					adapter1_city.getList().add(departmentInfo);
				}
				adapter1_city.notifyDataSetChanged();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

	}

	private class DocHandler extends JsonHttpResponseHandler {

		private int state;

		public DocHandler(int num) {
			state = num;
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			if (!predep.equals(dep) || !precity.equals(city)) {
				adapter2.getList().clear();
				adapter2.notifyDataSetChanged();
			}
			hasmore = false;
			footer.setText("没有更多数据");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter2.getList().clear();
					adapter2.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					adapter2.getList().add(
							new MainTow_Docinfo(response.getJSONObject(i)
									.toString()));
					adapter2.notifyDataSetChanged();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

	}

}
