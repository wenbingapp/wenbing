﻿package com.wenbingwang.wenbingapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.CommentsInfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Activity5_item3 extends BaseActivity implements OnRefreshListener,
		OnItemClickListener {
	private TextView activity5_item2_name, activity5_name1, activity5_man;
	private ImageView activity5_image;
	private String price3, buys3, value3;

	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private TextView footer;
	private boolean hasmore;
	private RatingBar ratingBar;
	private String UserID;
	private TextView title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity5_item2);
		Intent intent = this.getIntent();
		price3 = intent.getStringExtra("price3");
		buys3 = intent.getStringExtra("buys3");
		value3 = intent.getStringExtra("value3");
		activity5_name1 = (TextView) findViewById(R.id.activity5_name1);
		activity5_man = (TextView) findViewById(R.id.activity5_man);
		activity5_item2_name = (TextView) findViewById(R.id.activity5_item2_name);
		activity5_image = (ImageView) findViewById(R.id.activity5_image);
		activity5_image.setImageResource(R.drawable.icon_zixun_2);
		activity5_item2_name.setText("电话咨询");
		activity5_name1.setText(price3 + "/次");
		activity5_man.setText(buys3 + "人购买");
		ratingBar = (RatingBar) findViewById(R.id.ratingBar1);
		ratingBar.setRating(Float.parseFloat(getIntent().getStringExtra(
				"value3")));
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		UserID = getIntent().getStringExtra("UserID");
		title = (TextView) findViewById(R.id.title);
		title.setText(getIntent().getStringExtra("title"));
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", UserID);
		requestParams.add("Page", page + "");
		requestParams.add("CommentType", "3");
		hasmore = true;
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/pjlistinfo",
				requestParams, new MyHandler(s));

	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					CommentsInfo indentListInfo = new CommentsInfo(response
							.getJSONObject(i).toString());
					adapter.getList().add(indentListInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);

		}

	}

	private class Adapter extends BaseAdapter {
		private ArrayList<CommentsInfo> list;

		public Adapter() {
			list = new ArrayList<CommentsInfo>();
		}

		public ArrayList<CommentsInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(Activity5_item3.this)
						.inflate(R.layout.activity5_item2_item, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.activity5_item2_textView1);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.activity5_item2_textView2);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.activity5_item2_textView3);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText("患者："
					+ list.get(position).getNickname() + " ("
					+ list.get(position).getCommentValue() + "星)");
			viewHolder.textView2.setText("时间："
					+ time(list.get(position).getTime()));
			viewHolder.textView3.setText("评价："
					+ list.get(position).getCommentDescription());
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
	}

	private String time(String nowTime) {
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd hh:mm");
		SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str1 = formatter2.format(curDate);
		String str = formatter.format(curDate);
		String dayStr = formatter1.format(curDate);
		// 当前时间
		String now = nowTime.substring(0, 10);// 输入的年月日
		String now1 = nowTime.substring(0, 7);// 输入的年月
		String nowyestoday = nowTime.substring(8, 10);// 输入的日
		String today = str1.substring(0, 10);// 当前的年月日
		String today2 = str1.substring(0, 7);// 当前的年月
		String today1 = str1.substring(8, 10);
		System.out.println(Integer.valueOf(today1).intValue());
		int yestodays = (Integer.parseInt(today1)) - 1;
		String yes = "" + yestodays;
		if (now.equals(today)) {
			return "今天 " + dayStr;
		} else if (nowyestoday.equals(yes) && now1.equals(today2)) {
			return "昨天 " + dayStr;
		} else {
			return str;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		}
	}
}
