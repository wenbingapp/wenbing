package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends BaseActivity implements OnClickListener {

	private EditText editText1;
	private EditText editText2;
	private Button button1;
	private Button button2;
	private Button button3;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_activity);
		editText1 = (EditText) findViewById(R.id.editText1);
		editText2 = (EditText) findViewById(R.id.editText2);
		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("提交");
		progressDialog.setMessage("正在提交请求");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			// Intent intent=new Intent(RegisterActivity.this, )
			break;
		case R.id.button2:
			if (editText1.getText().toString().equals("")
					|| editText1.getText().toString().length() != 11) {
				showToast("请输入正确的手机号码");
			} else {

			}
			break;
		case R.id.button3:
			if (editText2.getText().toString().equals("")) {

			}
			if (editText1.getText().toString().equals("")
					|| editText1.getText().toString().length() != 11) {

			}
			break;
		}
	}

	private class GetSmsHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}
}
