package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.HomeInfo;
import com.wenbingwang.mywedgit.CustomRatingBar;
import com.wenbingwang.mywedgit.CustomRatingBar.IRatingBarCallbacks;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class PingjiaActivity extends BaseActivity implements
		IRatingBarCallbacks {
	private HomeInfo homeInfo;
	private ImageView image;
	private TextView t1, t2, t3, t4;
	private Button pjBtn;
	private EditText pingjia;
	private CustomRatingBar ratingbar1;
	private String pingjiaNum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pingjia);
		image = (ImageView) findViewById(R.id.imageView1);
		t1 = (TextView) findViewById(R.id.textView1_1);
		t2 = (TextView) findViewById(R.id.textView2);
		t3 = (TextView) findViewById(R.id.textView3);
		t4 = (TextView) findViewById(R.id.textView4);
		
		pingjia = (EditText) findViewById(R.id.pingjia);
		ratingbar1 = (CustomRatingBar) findViewById(R.id.ratingbar1);
		ratingbar1.setOnScoreChanged(this);
		doc();
		pjBtn = (Button) findViewById(R.id.pjBtn);
		pjBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				doc1();
			}
		});
	}

	public void doc1() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("FormID", getIntent().getStringExtra("HomeDoctorID"));
		requestParams.add("FormType",
				getIntent().getStringExtra("HomeDoctorType"));
		requestParams.add("CommentValue", pingjiaNum);
		requestParams.add("CommentDescription", pingjia.getText().toString());
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/pattodocinfo", requestParams,
				new MyHandler1());
	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("")) {
					showToast(jsonObject.getString("SubmitDescription"));
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public void doc() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("DoctorID", getIntent().getStringExtra("DoctorID"));
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/docperinfo",
				requestParams, new MyHandler());
	}

	private class MyHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				homeInfo = new HomeInfo(jsonObject.toString());
				t1.setText(homeInfo.getDoctorName());
				t2.setText(homeInfo.getDoctorLevel());
				t3.setText(homeInfo.getDoctorHospital());
				t4.setText(homeInfo.getDoctorDepartment());
				imageLoader.displayImage(homeInfo.getDoctorHeadImageUrl(),
						image, options);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	@Override
	public void scoreChanged(float score) {
		// TODO Auto-generated method stub
		int a = (int) Double.parseDouble(score + "");
		pingjiaNum = a + "";
	}
}
