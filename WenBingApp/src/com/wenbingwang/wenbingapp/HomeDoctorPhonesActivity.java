package com.wenbingwang.wenbingapp;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.HomeDocterPhones;
import com.wenbingwang.bean.PayResult_gh;

import de.greenrobot.event.EventBus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class HomeDoctorPhonesActivity extends BaseActivity implements
		OnRefreshListener, OnItemClickListener {
	private TextView doc_textview;
	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private TextView footer;
	private boolean hasmore;
	private ArrayList<HomeDocterPhones> list;
	private int a;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doctor);
		doc_textview = (TextView) findViewById(R.id.doc_textview);
		doc_textview.setText("电话咨询");
		EventBus.getDefault().register(this);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}
	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void loads(int position) {//获得电话咨询列表
		RequestParams requestParams = new RequestParams();
		requestParams.add("TelephoneID", list.get(position).getTelephoneID());
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/hzbddhinfo", requestParams,
				new MyHandlers());

	}
	private class MyHandlers extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			showToast("正在加载数据");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				if(response.getString("SubmitResult").equals("1")){
					showToast("提交成功");
				}else{
					showToast(response.getString("SubmitDescription").toString());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}
	
	private void load(int s) {//获得电话咨询列表
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("Page", page + "");
		hasmore = true;
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/hzdhlistinfo", requestParams,
				new MyHandler(s));

	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					HomeDocterPhones indentListInfo = new HomeDocterPhones(
							response.getJSONObject(i).toString());
					adapter.getList().add(indentListInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private class Adapter extends BaseAdapter {

		public Adapter() {
			list = new ArrayList<HomeDocterPhones>();
		}

		public ArrayList<HomeDocterPhones> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater
						.from(HomeDoctorPhonesActivity.this).inflate(
								R.layout.doctors, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.fans_activity_name);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.fans_item_age);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.fans_item_time);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.fans_item_address);
				viewHolder.textView5 = (TextView) convertView
						.findViewById(R.id.fans_item_age1);
				viewHolder.textView6 = (TextView) convertView
						.findViewById(R.id.fans_item_age2);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.fans_item_image);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			if ((list.get(position).getDoctorNickname()).length() > 3) {
				viewHolder.textView1.setText("医生："
						+ (list.get(position).getDoctorNickname()).substring(0,
								2) + "...");
			} else {
				viewHolder.textView1.setText("医生："
						+ list.get(position).getDoctorNickname());
			}
			// sex:1 男 2 女
			viewHolder.textView2.setText("科室:"
					+ list.get(position).getDoctorDepartment());
			if ((list.get(position).getFormStatus()).equals("0")) {
				viewHolder.textView3.setText("订单状态:待付款");
			}
			if ((list.get(position).getFormStatus()).equals("1")) {
				viewHolder.textView3.setText("订单状态:待通话");
			}
			if ((list.get(position).getFormStatus()).equals("2")) {
				viewHolder.textView3.setText("订单状态:待评价");
			}
			if ((list.get(position).getFormStatus()).equals("3")) {
				viewHolder.textView3.setText("订单状态:已完成");
			}
			viewHolder.textView4.setText("通话："
					+ list.get(position).getTelephoneTime());
			viewHolder.textView5.setText("电话:"
					+ list.get(position).getUserTelephone());
			viewHolder.textView6.setText("时长:"
					+ list.get(position).getTelephoneDuration());
			imageLoader.displayImage(list.get(position).getDoctorHeadImage(),
					viewHolder.imageView, options);
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
		public TextView textView4;
		public TextView textView5;
		public TextView textView6;
		private ImageView imageView;
	}
	public void onEventMainThread(PayResult_gh event) {
		String s = event.state;
		if (s.equals("0")) {
			onRefresh();
		} else if (s.equals("1")) {
			return;
		} else if (s.equals("2")) {
			return;
		}

	}

	public void onItemClick(AdapterView<?> parent, View view,
			final int position, long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		} else {
			if ((list.get(position).getFormStatus()).equals("0")) {
				new AlertDialog.Builder(this)
						.setTitle("电话咨询")
						.setMessage("该订单尚未付款,是否进行付款")
						.setPositiveButton("付款",
								new AlertDialog.OnClickListener() {
									/**
									 * orderid oderprice oderhead 枚举： odername
									 * oderinfo
									 * 
									 * ordershow ordertitle
									 * 
									 * @author Administrator
									 * 
									 */
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(
												HomeDoctorPhonesActivity.this,
												PayActivity.class);
										intent.putExtra("orderid",
												list.get(position)
														.getTelephoneID());
										intent.putExtra("oderprice",
												list.get(position)
														.getTelephonePrice());
										intent.putExtra("oderhead", "Telephone");
										intent.putExtra("odername", "电话咨询");
										intent.putExtra("oderinfo",
												"向医生进行了电话咨询");
										if ((list.get(position)
												.getTelephoneType())
												.equals("0")) {
											intent.putExtra(
													"ordershow",
													"时间：一周        "
															+ "金额："
															+ list.get(position)
																	.getTelephonePrice()
															+ "元");
										} else if ((list.get(position)
												.getTelephoneType())
												.equals("1")) {
											intent.putExtra(
													"ordershow",
													"时间：一月        "
															+ "金额："
															+ list.get(position)
																	.getTelephonePrice()
															+ "元");
										}
										intent.putExtra("ordertitle", "电话咨询");
										startActivity(intent);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("1")) {
				new AlertDialog.Builder(this)
						.setTitle("电话咨询")
						.setMessage("您的订单已完成付款，尚未进行通话，是否与医生进行通话")
						.setPositiveButton("通话",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										 new AlertDialog.Builder(HomeDoctorPhonesActivity.this)
											.setTitle("电话咨询")
											.setMessage(
													"与医生的通话发起成功，本次通话除咨询费用之外将不产生任何通话费用，可放心接听电话")
											.setPositiveButton("确定", null).show();
										 loads(position);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("2")) {
				new AlertDialog.Builder(this)
						.setTitle("家庭医生")
						.setMessage("您尚未对该医生评价，请前往评价")
						.setPositiveButton("确定",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(
												HomeDoctorPhonesActivity.this,
												PingjiaActivity.class);
										intent.putExtra("DoctorID",
												list.get(position)
														.getDoctorID());
										intent.putExtra("HomeDoctorID", list
												.get(position).getTelephoneID());
										intent.putExtra("HomeDoctorType", "1");
										startActivity(intent);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("3")) {
				new AlertDialog.Builder(this).setTitle("电话咨询")
						.setMessage("您购买医生的家庭医生服务已经结束")
						.setPositiveButton("确定", null).show();
			}

		}
	}
}
