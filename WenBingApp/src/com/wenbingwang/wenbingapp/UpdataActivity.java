package com.wenbingwang.wenbingapp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class UpdataActivity extends BaseActivity implements OnClickListener {
	final String[] mItems = { "拍照", "相册" };
	private File file;
	private ProgressDialog progressDialog;
	private RadioGroup radioGroup;
	private EditText name, age, adress;
	private ImageView image;
	private Button updateBtn;
	private RadioButton radio0, radio1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.updata);
		radioGroup = (RadioGroup) findViewById(R.id.updata_radioGroup1);
		name = (EditText) findViewById(R.id.updata_name);
		age = (EditText) findViewById(R.id.updata_name2);
		adress = (EditText) findViewById(R.id.updata_name3);
		updateBtn = (Button) findViewById(R.id.updateBtn);
		image = (ImageView) findViewById(R.id.updata_imageView1);
		radio0 = (RadioButton) findViewById(R.id.radio0);
		radio1 = (RadioButton) findViewById(R.id.radio1);
		name.setText(getMyInfo().getNickName());
		age.setText(getMyInfo().getAge());
		adress.setText(getMyInfo().getAddress());
		imageLoader.displayImage(getMyInfo().getUserHeadImageUrl(), image,
				options);
		String sex = getMyInfo().getSex();
		if (sex.equals("1")) {// 男
			radio0.setChecked(true);
			radio1.setChecked(false);
		} else {// 女
			radio1.setChecked(true);
			radio0.setChecked(false);
		}
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("提交");
		progressDialog.setMessage("正在提交请求");
		updateBtn.setOnClickListener(this);
		image.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.updateBtn:
			commite();
			break;
		}
	}

	// 拍照上传
	public void gh_getpics(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("选择图片来源");
		builder.setItems(mItems, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					Intent cameraintent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					cameraintent.putExtra(MediaStore.EXTRA_OUTPUT,
							Uri.fromFile(tempFile));
					startActivityForResult(cameraintent,
							PHOTO_REQUEST_TAKEPHOTO);
					break;
				case 1:
					Intent getAlbum = new Intent(Intent.ACTION_GET_CONTENT);
					getAlbum.setType("image/*");
					startActivityForResult(getAlbum, PHOTO_REQUEST_GALLERY);
					break;
				default:
					break;
				}
			}
		});
		builder.create().show();
	}

	private void commite() {
		if (name.getText().toString().equals("")) {
			showToast("请填写昵称");
			return;
		}
		if (age.getText().toString().equals("")) {
			showToast("请填写年龄");
			return;
		}
		if (adress.getText().toString().equals("")) {
			showToast("请填写地址");
			return;
		}
		RequestParams requestParams = new RequestParams();
		requestParams.put("UserID", "10011");
		requestParams.put("Age", age.getText().toString());
		requestParams
				.put("Sex",
						radioGroup.getCheckedRadioButtonId() == R.id.radio0 ? "1"
								: "0");
		requestParams.put("NickName", name.getText().toString());
		requestParams.put("Address", adress.getText().toString());
		if (file != null) {
			try {
				requestParams.put("HeadImage", file, "image/jpeg");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			InputStream imInputStream = new ByteArrayInputStream(new byte[0]);
			requestParams.put("QuestionImage", imInputStream, "QuestionImage",
					"image/jpeg", true);
		}
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/setperinfo",
				requestParams, new Handler());
	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					finish();
					showToast("修改成功");
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	private void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 300);
		intent.putExtra("outputY", 300);
		intent.putExtra("noFaceDetection", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(corpFile));
		startActivityForResult(intent, PHOTO_REQUEST_CUT);
	}

	private String getPhotoFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'_yyyyMMdd_HHmmss");
		return dateFormat.format(date) + ".jpg";
	}

	private String getCorpFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'_yyyyMMdd_HHmmss");
		return dateFormat.format(date) + "_corp.jpg";
	}

	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;
	private static final int PHOTO_REQUEST_GALLERY = 2;
	private static final int PHOTO_REQUEST_CUT = 3;
	private File tempFile = new File(Environment.getExternalStorageDirectory(),
			getPhotoFileName());
	private File corpFile = new File(Environment.getExternalStorageDirectory(),
			getCorpFileName());

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case PHOTO_REQUEST_TAKEPHOTO:
				startPhotoZoom(Uri.fromFile(tempFile));
				break;
			case PHOTO_REQUEST_GALLERY:
				if (data != null) {
					startPhotoZoom(data.getData());
				}
				break;
			case PHOTO_REQUEST_CUT:
				if (data != null) {
					file = corpFile;
				}
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
