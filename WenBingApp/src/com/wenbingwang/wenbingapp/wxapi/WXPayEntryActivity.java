package com.wenbingwang.wenbingapp.wxapi;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.wenbingwang.app.application.Constants;
import com.wenbingwang.bean.PayResult_gh;
import com.wenbingwang.bean.RecvProblemItemInfo;
import com.wenbingwang.wenbingapp.R;

import de.greenrobot.event.EventBus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

	private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

	private IWXAPI api;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pay_result);

		api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
		EventBus.getDefault().register(this);
		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
	}
	
	public void onEventMainThread(RecvProblemItemInfo event) {

	}

	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onResp(BaseResp resp) {
		Log.d(TAG, "onPayFinish, errCode = " + resp.errCode);

		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.app_tip);
			String s = "";
			switch (resp.errCode) {
			case 0:
				s = "支付成功";
				sendPayResult(null, "0");
				break;
			case -1:
				s = "支付失败";
				sendPayResult(null, "1");
				break;
			case -2:
				s = "支付取消";
				sendPayResult(null, "2");
				break;
			default:
				break;
			}
			builder.setMessage(getString(R.string.pay_result_callback_msg, s));
			builder.setPositiveButton("确定", new AlertDialog.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					finish();
				}
			});
			builder.show();
		}

	}

	/**
	 * 
	 * @param id
	 * @param state 成功状态（0成功 ，1失败 ,2用户取消）
	 */
	private void sendPayResult(String id, String state) {
		PayResult_gh payResult = new PayResult_gh();
		payResult.id = id;
		payResult.state = state;
		EventBus.getDefault().post(payResult);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}
}