package com.wenbingwang.wenbingapp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.DepartmentInfo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.PaintDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;

public class FreeAskingActivity extends BaseActivity implements
		OnClickListener, OnItemClickListener {

	private Button yes_btn;
	private EditText editText1;
	private EditText editText2;
	private EditText editText3;
	private ProgressDialog progressDialog;
	private RadioGroup radioGroup;
	private File file;
	final String[] mItems = { "拍照", "相册" };
	private Adapter1 adapter1;
	private ListView roomList;
	private PopupWindow popupWindow;
	private String pageDep;
	private Button button2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.free_asking_activity);
		editText1 = (EditText) findViewById(R.id.editText1);
		editText2 = (EditText) findViewById(R.id.editText2);
		editText3 = (EditText) findViewById(R.id.editText3);
		yes_btn = (Button) findViewById(R.id.yes_btn);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		button2 = (Button) findViewById(R.id.button2);
		yes_btn.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("提交");
		progressDialog.setMessage("正在提交请求");
		adapter1 = new Adapter1();
		loadDepartment();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.yes_btn:
			commite();
			break;
		case R.id.cancle:
			popupWindow.dismiss();
			break;
		default:
			break;
		}
	}

	private void commite() {
		if (editText1.getText().toString().equals("")) {
			showToast("请填写年龄");
			return;
		}
		if (editText3.getText().toString().equals("")) {
			showToast("请填写问题标题");
			return;
		}
		if (editText2.getText().toString().equals("")) {
			showToast("请填写问题详情");
			return;
		}
		if (pageDep == null) {
			showToast("请选择提交科室");
			return;
		}
		RequestParams requestParams = new RequestParams();
		if (getMyInfo() == null) {
			return;
		}
		requestParams.put("UserID", getMyInfo().getUserID());
		requestParams.put("Age", editText1.getText().toString());
		requestParams
				.put("Sex",
						radioGroup.getCheckedRadioButtonId() == R.id.radio0 ? "1"
								: "0");
		requestParams.put("QuestionTitle", editText3.getText().toString());
		requestParams
				.put("QuestionDescription", editText2.getText().toString());
		requestParams.put("DepartmentID", pageDep);
		if (file != null) {
			try {
				requestParams.put("QuestionImage", file, "image/jpeg");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			InputStream imInputStream = new ByteArrayInputStream(new byte[0]);
			requestParams.put("QuestionImage", imInputStream, "QuestionImage",
					"image/jpeg", true);
		}
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/mftwinfo",
				requestParams, new Handler());
	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					finish();
					showToast("问题提交成功");
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	public void gh_getpic(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("选择图片来源");
		builder.setItems(mItems, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					Intent cameraintent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					cameraintent.putExtra(MediaStore.EXTRA_OUTPUT,
							Uri.fromFile(tempFile));
					startActivityForResult(cameraintent,
							PHOTO_REQUEST_TAKEPHOTO);
					break;
				case 1:
					Intent getAlbum = new Intent(Intent.ACTION_GET_CONTENT);
					getAlbum.setType("image/*");
					startActivityForResult(getAlbum, PHOTO_REQUEST_GALLERY);
					break;
				default:
					break;
				}
			}
		});
		builder.create().show();
	}

	public void gh_getroom(View v) {
		showPopup(v, R.layout.popupwin_1);
	}

	private void loadDepartment() {
		asyncHttpClient.get(this, "http://yisheng.wenbing.cn/Info/ksinfo",
				new DepHandler());
	}

	private class DepHandler extends JsonHttpResponseHandler {

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				adapter1.getList().clear();
				adapter1.notifyDataSetChanged();
				DepartmentInfo departmentInfos = new DepartmentInfo();
				departmentInfos.setDepartmentID("0");
				departmentInfos.setDepartmentName("所有科室");
				adapter1.getList().add(departmentInfos);
				for (int i = 0; i < response.length(); i++) {
					DepartmentInfo departmentInfo = new DepartmentInfo();
					departmentInfo.setDepartmentID(response.getJSONObject(i)
							.getString("DepartmentID"));
					departmentInfo.setDepartmentName(response.getJSONObject(i)
							.getString("DepartmentName"));
					adapter1.getList().add(departmentInfo);
				}
				adapter1.notifyDataSetChanged();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

	}

	private class Adapter1 extends BaseAdapter {
		private ArrayList<DepartmentInfo> list;

		public Adapter1() {
			list = new ArrayList<DepartmentInfo>();
		}

		public ArrayList<DepartmentInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(FreeAskingActivity.this).inflate(
					R.layout.fragment_two_room, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText(list.get(position).getDepartmentName());
			return convertView;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (parent == roomList) {
			pageDep = adapter1.getList().get(position).getDepartmentID();
			button2.setText(adapter1.getList().get(position)
					.getDepartmentName());
			popupWindow.dismiss();
		}

	}

	private void showPopup(View v, int res) {
		if (popupWindow == null) {
			View inflateview = LayoutInflater.from(this).inflate(res, null);
			popupWindow = new PopupWindow(inflateview);
			popupWindow.setWidth(this.getWindowManager().getDefaultDisplay()
					.getWidth() / 4 * 3);
			popupWindow.setHeight(this.getWindowManager().getDefaultDisplay()
					.getHeight() / 4 * 3);
			popupWindow.setFocusable(true);
			popupWindow.setBackgroundDrawable(new PaintDrawable());
			popupWindow.setAnimationStyle(R.style.popup_anim);
			popupWindow.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
					WindowManager.LayoutParams lp = getWindow().getAttributes();
					lp.alpha = 1f;
					getWindow().setAttributes(lp);
				}
			});
			roomList = (ListView) inflateview.findViewById(R.id.listView1);
			roomList.setAdapter(adapter1);
			roomList.setOnItemClickListener(this);
			((Button) inflateview.findViewById(R.id.cancle))
					.setOnClickListener(this);
		}
		WindowManager.LayoutParams lp = this.getWindow().getAttributes();
		lp.alpha = 0.4f;
		this.getWindow().setAttributes(lp);
		popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;
	private static final int PHOTO_REQUEST_GALLERY = 2;
	private static final int PHOTO_REQUEST_CUT = 3;
	private File tempFile = new File(Environment.getExternalStorageDirectory(),
			getPhotoFileName());
	private File corpFile = new File(Environment.getExternalStorageDirectory(),
			getCorpFileName());

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case PHOTO_REQUEST_TAKEPHOTO:
				startPhotoZoom(Uri.fromFile(tempFile));
				break;
			case PHOTO_REQUEST_GALLERY:
				if (data != null) {
					startPhotoZoom(data.getData());
				}
				break;
			case PHOTO_REQUEST_CUT:
				if (data != null) {
					file = corpFile;
				}
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private String getPhotoFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'_yyyyMMdd_HHmmss");
		return dateFormat.format(date) + ".jpg";
	}

	private String getCorpFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'_yyyyMMdd_HHmmss");
		return dateFormat.format(date) + "_corp.jpg";
	}

	private void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 300);
		intent.putExtra("outputY", 300);
		intent.putExtra("noFaceDetection", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(corpFile));
		startActivityForResult(intent, PHOTO_REQUEST_CUT);
	}

}
