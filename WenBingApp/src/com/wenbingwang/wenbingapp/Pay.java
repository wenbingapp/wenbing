package com.wenbingwang.wenbingapp;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;

import org.apache.http.Header;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.ECDevice.OnLogoutListener;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.wenbingwang.app.application.Constants;
import com.wenbingwang.bean.PayResult_gh;
import com.wenbingwang.bean.PayvipInfo;
import com.wenbingwang.bean.RecvProblemItemInfo;
import com.wenbingwang.bean.WeixinInfo;
import com.wenbingwang.bean.YinlianInfo;
import com.wenbingwang.zhifubao.PayResult;
import com.wenbingwang.zhifubao.SignUtils;

import de.greenrobot.event.EventBus;

public abstract class Pay extends BaseActivity implements Runnable {

	private static final int SDK_PAY_FLAG = 1;
	private static final int SDK_CHECK_FLAG = 2;
	private static final int SDK_YINLIAN_FLAG = 1000;
	private WeixinInfo weixininfo;
	private YinlianInfo info;
	public static final int PLUGIN_VALID = 0;
	public static final int PLUGIN_NOT_INSTALLED = -1;
	public static final int PLUGIN_NEED_UPGRADE = 2;
	private final String mMode = "00";
	private static final String TN_URL_01 = "http://202.101.25.178:8080/sim/gettn";
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SDK_PAY_FLAG:
				PayResult payResult = new PayResult((String) msg.obj);
				// 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
				String resultStatus = payResult.getResultStatus();
				// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
				if (TextUtils.equals(resultStatus, "9000")) {
					Toast.makeText(getApplicationContext(), "支付成功",
							Toast.LENGTH_SHORT).show();
					sendPayResult(null, "0");
				} else {
					// 判断resultStatus 为非“9000”则代表可能支付失败
					// “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
					if (TextUtils.equals(resultStatus, "8000")) {
						Toast.makeText(getApplicationContext(), "支付结果确认中",
								Toast.LENGTH_SHORT).show();
					} else if(TextUtils.equals(resultStatus, "6001")) {
						// 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
						Toast.makeText(getApplicationContext(), "支付取消",
								Toast.LENGTH_SHORT).show();
						sendPayResult(null, "2");
					}else
					{
						// 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
						Toast.makeText(getApplicationContext(), "支付失败",
								Toast.LENGTH_SHORT).show();
						sendPayResult(null, "1");
					}
				}
				break;
			case SDK_CHECK_FLAG:
				Toast.makeText(getApplicationContext(), "检查结果为：" + msg.obj,
						Toast.LENGTH_SHORT).show();
				break;
			case SDK_YINLIAN_FLAG:

				String tn = "";
				if (msg.obj == null || ((String) msg.obj).length() == 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getApplicationContext());
					builder.setTitle("错误提示");
					builder.setMessage("网络连接失败,请重试!");
					builder.setNegativeButton("确定",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							});
					builder.create().show();
				} else {
					tn = (String) msg.obj;
					/*************************************************
					 * 步骤2：通过银联工具类启动支付插件
					 ************************************************/
					doStartUnionPayPlugin(getApplicationContext(),
							info.getUnionpayFormID(), mMode);
				}
				break;
			default:
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	public void onEventMainThread(RecvProblemItemInfo event) {

	}

	/**
	 * 
	 * @param id
	 * @param state
	 *            成功状态（0成功 ，1失败 ,2用户取消）
	 */
	private void sendPayResult(String id, String state) {
		PayResult_gh payResult = new PayResult_gh();
		payResult.id = id;
		payResult.state = state;
		EventBus.getDefault().post(payResult);
	}

	public void doStartUnionPayPlugin(Context activity, String tn, String mode) {
		// mMode参数解释：
		// 0 - 启动银联正式环境
		// 1 - 连接银联测试环境
		int ret = UPPayAssistEx.startPay(this, null, null, tn, mode);
		if (ret == PLUGIN_NEED_UPGRADE || ret == PLUGIN_NOT_INSTALLED) {
			// 需要重新安装控件

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("提示");
			builder.setMessage("完成购买需要安装银联支付控件，是否安装？");
			builder.setNegativeButton("确定",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							UPPayAssistEx
									.installUPPayPlugin(getApplicationContext());
							dialog.dismiss();
						}
					});
			builder.setPositiveButton("取消",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			builder.create().show();

		}
	}

	protected String check(String s) {
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		try {
			return decimalFormat.format(Float.valueOf(s));
		} catch (Exception e) {
			return null;
		}
	}

	protected void getYue(String FormId, String FormTitle,
			String FormDescription, String FormPrice) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("FormID", FormId);
		requestParams.add("FormTitle", FormTitle);
		requestParams.add("FormDescription", FormDescription);
		requestParams.add("FormPrice", FormPrice);
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/balancepayinfo", requestParams,
				new YueHandler());
	}

	private class YueHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			System.out.println("aa");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				if (response.getString("SubmitResult").equals("1")) {
					showToast(response.getString("SubmitDescription")
							.toString());
					sendPayResult(null, "0");
				} else {
					showToast(response.getString("SubmitDescription")
							.toString());
					sendPayResult(null, "1");
				}

			} catch (Exception e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected void getWeixinPrice(String FormId, String FormTitle,
			String FormDescription, String FormPrice) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("FormID", FormId);
		requestParams.add("FormTitle", FormTitle);
		requestParams.add("FormDescription", FormDescription);
		requestParams.add("FormPrice", FormPrice);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/wxpayinfo",
				requestParams, new WeixinHandler());
	}

	protected void getYinLianPrice(String FormId, String FormTitle,
			String FormDescription, String FormPrice) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("FormID", FormId);
		requestParams.add("FormTitle", FormTitle);
		requestParams.add("FormDescription", FormDescription);
		requestParams.add("FormPrice", FormPrice);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/unionpay",
				requestParams, new YinLianHandler());
	}

	private class YinLianHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			System.out.println("aa");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				info = new YinlianInfo(response.toString());

				/*************************************************
				 * 步骤1：从网络开始,获取交易流水号即TN
				 ************************************************/

				new Thread(Pay.this).start();
			} catch (Exception e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*************************************************
		 * 步骤3：处理银联手机支付控件返回的支付结果
		 ************************************************/
		if (data == null) {
			return;
		}

		String msg = "";
		/*
		 * 支付控件返回字符串:success、fail、cancel 分别代表支付成功，支付失败，支付取消
		 */
		String str = data.getExtras().getString("pay_result");
		if (str.equalsIgnoreCase("success")) {
			msg = "支付成功";
			sendPayResult(null, "0");
		} else if (str.equalsIgnoreCase("fail")) {
			sendPayResult(null, "1");
			msg = "支付失败";
		} else if (str.equalsIgnoreCase("cancel")) {
			sendPayResult(null, "2");
			msg = "支付取消";
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("支付结果通知");
		builder.setMessage(msg);
		builder.setInverseBackgroundForced(true);
		// builder.setCustomTitle();
		builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}

	private class WeixinHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			System.out.println("aa");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				weixininfo = new WeixinInfo(response.toString());
				// 将该app注册到微信
				IWXAPI msgApi = WXAPIFactory.createWXAPI(
						getApplicationContext(), null);// 先向微信注册您的appid
				msgApi.registerApp(Constants.APP_ID);

				PayReq request = new PayReq();
				request.appId = Constants.APP_ID;
				request.partnerId = Constants.MCH_ID;
				request.prepayId = weixininfo.getPrepayID();// 订单id从服务器获得
				request.packageValue = "Sign=WXPay";// 固定写法
				request.nonceStr = weixininfo.getNonceString();// 随机字符串自己生成
				request.timeStamp = weixininfo.getTimeStamp();// 时间戳自己生成
				request.sign = weixininfo.getSign();
				Boolean isc = false;
				if (msgApi.isWXAppSupportAPI()) {
					isc = msgApi.sendReq(request);
					System.out.println(isc);
				}	

			} catch (Exception e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 微信支付begin
	 */

	/**
	 * 支付宝begin
	 */
	public void pay(String number, String name, String info, String price) {

		String orderInfo = SignUtils.getOrderInfo(number, name, info, price);
		// 对订单做RSA 签名
		String sign = SignUtils.sign(orderInfo);
		try {
			// 仅需对sign 做URL编码
			sign = URLEncoder.encode(sign, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// 完整的符合支付宝参数规范的订单信息
		final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
				+ SignUtils.getSignType();

		Runnable payRunnable = new Runnable() {

			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask alipay = new PayTask(Pay.this);
				// 调用支付接口，获取支付结果
				String result = alipay.pay(payInfo);

				Message msg = new Message();
				msg.what = SDK_PAY_FLAG;
				msg.obj = result;
				mHandler.sendMessage(msg);
			}
		};
		// 必须异步调用
		Thread payThread = new Thread(payRunnable);
		payThread.start();
	}

	@Override
	public void run() {
		String tn = null;
		InputStream is;
		try {

			String url = TN_URL_01;

			URL myURL = new URL(url);
			URLConnection ucon = myURL.openConnection();
			ucon.setConnectTimeout(120000);
			is = ucon.getInputStream();
			int i = -1;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while ((i = is.read()) != -1) {
				baos.write(i);
			}

			tn = baos.toString();
			is.close();
			baos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Message msg = mHandler.obtainMessage();
		msg.what = SDK_YINLIAN_FLAG;
		msg.obj = tn;
		mHandler.sendMessage(msg);
	}

}
