package com.wenbingwang.wenbingapp;

import java.text.DecimalFormat;
import org.apache.http.Header;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.PayResult_gh;

import de.greenrobot.event.EventBus;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LoginActivity2 extends BaseActivity {
	private Button login2Btn;
	private LinearLayout login2_li;
	private TextView login2_begin, login2_end;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		huiyuan();
		setContentView(R.layout.login_activity2);
		EventBus.getDefault().register(this);
		login2Btn = (Button) findViewById(R.id.login2Btn);
		login2_li = (LinearLayout) findViewById(R.id.login2_li);
		login2_begin = (TextView) findViewById(R.id.login2_begin);
		login2_end = (TextView) findViewById(R.id.login2_end);
		login2Btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(LoginActivity2.this, PayVIPActivity.class);
				startActivity(intent);
			}
		});
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}
	public void onEventMainThread(PayResult_gh event) {
		String s = event.state;
		if (s.equals("0")) {
			huiyuan();
		} else if (s.equals("1")) {
			return;
		} else if (s.equals("2")) {
			return;
		}

	}

	public void huiyuan() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/vipdetailinfo", requestParams,
				new MyHandler());
	}

	private class MyHandler extends JsonHttpResponseHandler {

		@Override
		public void onStart() {// 15618231400
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			showToast("没有更多数据");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				if (response.getString("VIPBeginTime").equals("")) {
					login2_li.setVisibility(View.INVISIBLE);
					login2Btn.setVisibility(View.VISIBLE);
				} else {
					login2_li.setVisibility(View.VISIBLE);
					login2Btn.setVisibility(View.INVISIBLE);
					login2_begin.setText("购买时间："+response.getString("VIPBeginTime"));
					login2_end.setText("到期时间："+response.getString("VIPEndTime"));
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

}
