package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.PayResult_gh;
import com.wenbingwang.zhifubao.SignUtils;

import de.greenrobot.event.EventBus;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * orderid oderprice oderhead 枚举： odername oderinfo
 * 
 * ordershow ordertitle
 * 
 * @author Administrator
 * 
 */

public class PayActivity extends Pay {
	private RadioButton pay_re1, pay_re2, pay_re3, pay_re4;
	private RadioButton r[] = {};
	private int priceBtn = 0;// 0余额 1支付宝，2微信，3银联
	private TextView name, day;
	private Button btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doctor_pay);
		name = (TextView) findViewById(R.id.name);
		day = (TextView) findViewById(R.id.day);
		pay_re1 = (RadioButton) findViewById(R.id.radioButton0_0);
		pay_re2 = (RadioButton) findViewById(R.id.radioButton1_1);
		pay_re3 = (RadioButton) findViewById(R.id.radioButton2_2);
		pay_re4 = (RadioButton) findViewById(R.id.radioButton3_3);
		btn = (Button) findViewById(R.id.pay_button1);
		r = new RadioButton[] { pay_re1, pay_re2, pay_re3, pay_re4 };
		name.setText(getIntent().getStringExtra("ordertitle"));
		day.setText(getIntent().getStringExtra("ordershow"));
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getPrice(priceBtn, getIntent().getStringExtra("oderprice"),
						getIntent().getStringExtra("oderhead"), getIntent()
								.getStringExtra("odername"), getIntent()
								.getStringExtra("oderinfo"));
			}
		});
	}

	public void onEventMainThread(PayResult_gh event) {
		String s = event.state;
		if (s.equals("0")) {
			finish();
		} else if (s.equals("1")) {
			return;
		} else if (s.equals("2")) {
			return;
		}

	}


	public void getPrice(int type, String moneys, String s, String name,
			String some) {
		if (priceBtn == 0) {// 余额
			getYue(s + "=" + getIntent().getStringExtra("orderid") + "="
					+ SignUtils.getOutTradeNo(), name, some, getIntent()
					.getStringExtra("oderprice"));
		}

		if (priceBtn == 1) {// 支付寶
			pay(s + "=" + getIntent().getStringExtra("orderid") + "="
					+ SignUtils.getOutTradeNo(), name, some, getIntent()
					.getStringExtra("oderprice"));
		} else if (priceBtn == 2) {

			getWeixinPrice(s + "=" + getIntent().getStringExtra("orderid")
					+ "=" + SignUtils.getOutTradeNo(), name, some
					+ getIntent().getStringExtra("oderprice"), getIntent()
					.getStringExtra("oderprice"));
		} else if (priceBtn == 3) {
			getYinLianPrice(s + "=" + getIntent().getStringExtra("orderid")
					+ "=" + SignUtils.getOutTradeNo(), name, some
					+ getIntent().getStringExtra("oderprice"), getIntent()
					.getStringExtra("oderprice"));
		}

	}

	public String getpriceBtn(View v) {
		for (int i = 0; i < r.length; i++) {
			if (v.getId() == r[i].getId()) {
				r[i].setChecked(true);
				priceBtn = i;
			} else {
				r[i].setChecked(false);
			}
		}
		return "" + priceBtn;

	}

}
