package com.wenbingwang.wenbingapp;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.HomeDocterFans;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class HomeDoctorFansActivity extends BaseActivity implements
		OnRefreshListener, OnItemClickListener {

	private TextView doc_textview;
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private TextView footer;
	private Button fans_back;
	private boolean hasmore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doctor);
		doc_textview = (TextView) findViewById(R.id.doc_textview);
		doc_textview.setText("我的关注");
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("Page", page + "");
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/hzgzinfo",
				requestParams, new MyHandler(s));

	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");

		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					HomeDocterFans problemInfo = new HomeDocterFans(response
							.getJSONObject(i).toString());
					adapter.getList().add(problemInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);

		}

	}

	private class Adapter extends BaseAdapter {
		private ArrayList<HomeDocterFans> list;

		public Adapter() {
			list = new ArrayList<HomeDocterFans>();
		}

		public ArrayList<HomeDocterFans> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(HomeDoctorFansActivity.this)
						.inflate(R.layout.fragment_two_doc, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.textView1);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.textView2);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.textView3);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.textView4);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText(list.get(position).getDoctorName());
			viewHolder.textView2
					.setText(list.get(position).getDoctorHospital());
			viewHolder.textView3.setText("擅长："
					+ list.get(position).getDoctorAbility());
			viewHolder.textView4.setText(list.get(position).getDoctorLevel());
			imageLoader.displayImage(
					list.get(position).getDoctorHeadImageUrl(),
					viewHolder.imageView, options);
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
		public TextView textView4;
		private ImageView imageView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		}
		Intent intent = new Intent();
		intent.setClass(HomeDoctorFansActivity.this, Activity5.class);
		intent.putExtra("UserID", adapter.getList().get(position).getDoctorID());
		startActivity(intent);
	}

}
