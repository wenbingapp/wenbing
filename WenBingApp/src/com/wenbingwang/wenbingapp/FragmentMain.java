package com.wenbingwang.wenbingapp;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.bean.Ad;
import com.wenbingwang.bean.City;
import com.wenbingwang.bean.DocInfo;
import com.wenbingwang.bean.Main_docinfo;
import com.wenbingwang.mywedgit.HorizontalListView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Toast;

public class FragmentMain extends BaseFragment implements OnClickListener {
	private View rootView;
	private ViewPager viewPager;
	private CirclePageIndicator pageIndicator;
	private LinearLayout button1;
	private LinearLayout button2;
	private LinearLayout button3;
	private Button location;
	private GridView gridView;
	private HorizontalListView horizontalListView;
	private Adapter1 adapter1;
	private Adapter2 adapter2;
	private Adapter3 adapter3;
	private ArrayList<Ad> ads;
	private ArrayList<City> cities;
	private ArrayList<Main_docinfo> main_docinfos;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			location = (Button) rootView.findViewById(R.id.button1);
			button1 = (LinearLayout) rootView.findViewById(R.id.button_2);
			button1.setOnClickListener(this);
			button2 = (LinearLayout) rootView.findViewById(R.id.button_3);
			button2.setOnClickListener(this);
			button3 = (LinearLayout) rootView.findViewById(R.id.button_4);
			button3.setOnClickListener(this);
			horizontalListView = (HorizontalListView) rootView
					.findViewById(R.id.hlvSimpleList);
			gridView = (GridView) rootView.findViewById(R.id.gridView1);
			pageIndicator = (CirclePageIndicator) rootView
					.findViewById(R.id.indicator);
			viewPager = (ViewPager) rootView.findViewById(R.id.pager);
			ads = new ArrayList<Ad>();
			cities = new ArrayList<City>();
			main_docinfos = new ArrayList<Main_docinfo>();
			adapter1 = new Adapter1();
			viewPager.setAdapter(adapter1);
			pageIndicator.setViewPager(viewPager);
			adapter2 = new Adapter2();
			gridView.setAdapter(adapter2);
			adapter3 = new Adapter3();
			horizontalListView.setAdapter(adapter3);
		}
		loadAds();
		// loadDoc();
		return rootView;
	}

	private void loadAds() {
		asyncHttpClient.get(getActivity(),
				"http://yisheng.wenbing.cn/Info/maininfo", new AdsHandler());
	}

	// private void loadDoc() {
	// asyncHttpClient.get(getActivity(),
	// "http://yisheng.wenbing.cn/Info/myinfo", new DocHandler());
	// }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	private class Adapter1 extends PagerAdapter {

		private ArrayList<View> mListViews;

		public Adapter1() {
			mListViews = new ArrayList<View>();
		}

		public ArrayList<View> getListView() {
			return mListViews;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mListViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(mListViews.get(position));
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(mListViews.get(position), 0);
			return mListViews.get(position);
		}

	}

	private class Adapter2 extends BaseAdapter {
		private int[] backgroundRes = { R.drawable.button_2_background,
				R.drawable.button_3_background, R.drawable.button_4_background,
				R.drawable.button_5_background, R.drawable.button_6_background,
				R.drawable.button_7_background };

		private int[] icons = { R.drawable.icon_pencil, R.drawable.icon_sun,
				R.drawable.icon_heart, R.drawable.icon_pin };

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cities.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return cities.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(getActivity()).inflate(
					R.layout.fragment_main_recommend, null);
			LinearLayout linearLayout = (LinearLayout) convertView
					.findViewById(R.id.gh);
			linearLayout.setBackgroundResource(backgroundRes[position % 6]);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText("预约" + cities.get(position).getCityName());
			ImageView imageView = (ImageView) convertView
					.findViewById(R.id.imageView1);
			imageView.setImageResource(icons[position % 4]);
			return convertView;
		}
	}

	private class Adapter3 extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return main_docinfos.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return main_docinfos.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(getActivity()).inflate(
					R.layout.fragment_main_doc, null);
			ImageView imageView1 = (ImageView) convertView
					.findViewById(R.id.imageView1);
			imageLoader.displayImage(main_docinfos.get(position)
					.getDoctorHeadImageUrl(), imageView1, options);
			TextView textView1 = (TextView) convertView
					.findViewById(R.id.textView1);
			textView1.setText(main_docinfos.get(position).getDoctorName());
			TextView textView2 = (TextView) convertView
					.findViewById(R.id.textView2);
			textView2
					.setText(main_docinfos.get(position).getDoctorDepartment());
			TextView textView3 = (TextView) convertView
					.findViewById(R.id.textView3);
			textView3.setText("擅长："
					+ main_docinfos.get(position).getDoctorAbility());
			TextView textView4 = (TextView) convertView
					.findViewById(R.id.textView4);
			textView4.setText(main_docinfos.get(position).getDoctorLevel());
			((ImageView) convertView.findViewById(R.id.imageView2))
					.setVisibility(main_docinfos.get(position)
							.getHomeDoctorOpen().equals("1") ? View.VISIBLE
							: View.GONE);
			((ImageView) convertView.findViewById(R.id.imageView3))
					.setVisibility(main_docinfos.get(position)
							.getImageWordOpen().equals("1") ? View.VISIBLE
							: View.GONE);
			((ImageView) convertView.findViewById(R.id.imageView4))
					.setVisibility(main_docinfos.get(position)
							.getTelephoneOpen().equals("1") ? View.VISIBLE
							: View.GONE);
			((ImageView) convertView.findViewById(R.id.imageView5))
					.setVisibility(main_docinfos.get(position)
							.getOutpatientOpen().equals("1") ? View.VISIBLE
							: View.GONE);
			return convertView;
		}
	}

	private class AdsHandler extends JsonHttpResponseHandler {

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				ads.clear();
				cities.clear();
				main_docinfos.clear();
				adapter2.notifyDataSetChanged();
				adapter3.notifyDataSetChanged();
				JSONArray jsonArray1 = response.getJSONArray("ADs");
				JSONArray jsonArray2 = response.getJSONArray("Cities");
				JSONArray jsonArray3 = response.getJSONArray("Doctors");
				for (int i = 0; i < jsonArray1.length(); i++) {
					ads.add(new Ad(jsonArray1.getJSONObject(i).toString()));
				}
				for (int i = 0; i < jsonArray2.length(); i++) {
					cities.add(new City(jsonArray2.getJSONObject(i).toString()));
					adapter2.notifyDataSetChanged();
				}
				for (int i = 0; i < jsonArray3.length(); i++) {
					main_docinfos.add(new Main_docinfo(jsonArray3
							.getJSONObject(i).toString()));
					adapter3.notifyDataSetChanged();
				}
				adapter1.getListView().clear();
				adapter1.notifyDataSetChanged();
				for (int i = 0; i < ads.size(); i++) {
					ImageView imageView = new ImageView(getActivity());
					imageView.setScaleType(ScaleType.CENTER_CROP);
					imageView.setLayoutParams(new LayoutParams(
							LayoutParams.MATCH_PARENT,
							LayoutParams.MATCH_PARENT));
					imageLoader.displayImage(ads.get(i).getADImageUrls(),
							imageView, options);
					adapter1.getListView().add(imageView);
					adapter1.notifyDataSetChanged();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button_2:
			Intent intent1 = new Intent(getActivity(), FreeAskingActivity.class);
			startActivity(intent1);
			break;
		case R.id.button_3:
			Intent intent2 = new Intent(getActivity(), SuperCalling.class);
			startActivity(intent2);
			break;
		case R.id.button_4:
			Intent intent3 = new Intent(getActivity(), SuperDateActivity.class);
			startActivity(intent3);
			break;

		default:
			break;
		}
	}

	// private class DocHandler extends JsonHttpResponseHandler {
	//
	// @Override
	// public void onSuccess(int statusCode, Header[] headers,
	// JSONArray response) {
	// try {
	// adapter3.getList().clear();
	// adapter3.notifyDataSetChanged();
	// for (int i = 0; i < response.length(); i++) {
	// DocInfo docInfo = new DocInfo();
	// docInfo.setDoctorAbility(response.getJSONObject(i)
	// .getString("DoctorAbility"));
	// docInfo.setDoctorBriefIntroduction(response
	// .getJSONObject(i).getString(
	// "DoctorBriefIntroduction"));
	// docInfo.setDoctorDepartment(response.getJSONObject(i)
	// .getString("DoctorDepartment"));
	// docInfo.setDoctorHeadImageUrl(response.getJSONObject(i)
	// .getString("DoctorHeadImageUrl"));
	// docInfo.setDoctorHospital(response.getJSONObject(i)
	// .getString("DoctorHospital"));
	// docInfo.setDoctorID(response.getJSONObject(i).getString(
	// "DoctorID"));
	// docInfo.setDoctorName(response.getJSONObject(i).getString(
	// "DoctorName"));
	// docInfo.setDoctorVoIP(response.getJSONObject(i).getString(
	// "DoctorVoIP"));
	// adapter3.getList().add(docInfo);
	// }
	// adapter3.notifyDataSetChanged();
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	//
	// @Override
	// public void onFailure(int statusCode, Header[] headers,
	// Throwable throwable, JSONObject errorResponse) {
	// showToast("网络连接失败，请检查网络");
	// }
	//
	// }
}
