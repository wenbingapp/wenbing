package com.wenbingwang.wenbingapp;

import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.mywedgit.ACache;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class LoginActivity3 extends BaseActivity implements OnClickListener {
	private RelativeLayout fragment_three_re1;
	private ToggleButton gh1, gh2;
	private Button Login3_button1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity3);
		fragment_three_re1 = (RelativeLayout) findViewById(R.id.login3_re);
		gh1 = (ToggleButton) findViewById(R.id.toggbutton3_1);
		gh2 = (ToggleButton) findViewById(R.id.toggbutton3_2);
		Login3_button1 = (Button) findViewById(R.id.Login3_button1);
		Login3_button1.setOnClickListener(this);
		fragment_three_re1.setOnClickListener(this);
		ACache mCache = ACache.get(LoginActivity3.this);
		if (mCache.getAsString("Vibrate") == null
				|| mCache.getAsString("Vibrate").equals("1")) {
			gh2.setToggleOn();
		}
		if (mCache.getAsString("Sound") == null
				|| mCache.getAsString("Sound").equals("1")) {
			gh1.setToggleOn();
		}

		gh1.setOnToggleChanged(new OnToggleChanged() {
			@Override
			public void onToggle(boolean on) {
				setSound(on);
			}
		});
		gh2.setOnToggleChanged(new OnToggleChanged() {
			@Override
			public void onToggle(boolean on) {
				setVibrate(on);
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.login3_re:
			intent.setClass(LoginActivity3.this, SetPassWordActivity.class);
			startActivity(intent);
			break;
		case R.id.Login3_button1:
			ACache mCache = ACache.get(LoginActivity3.this);
			mCache.remove("logininfo");
			((MyApplication) getApplication()).setMyInfo(null);
			intent.setClass(LoginActivity3.this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra("login", "");
			startActivity(intent);
			finish();
			break;
		}
	}

	public void setSound(boolean f) {
		ACache mCache = ACache.get(LoginActivity3.this);
		if (f) {
			mCache.put("Sound", "1");
		} else {
			mCache.put("Sound", "0");
		}
	}

	public void setVibrate(boolean f) {
		ACache mCache = ACache.get(LoginActivity3.this);
		if (f) {
			mCache.put("Vibrate", "1");
		} else {
			mCache.put("Vibrate", "0");
		}
	}

}
