package com.wenbingwang.wenbingapp;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.alipay.sdk.app.PayTask;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.unionpay.UPPayAssistEx;
import com.wenbingwang.app.application.Constants;
import com.wenbingwang.bean.HomeDocterPhones;
import com.wenbingwang.bean.PayvipInfo;
import com.wenbingwang.bean.WeixinInfo;
import com.wenbingwang.bean.YinlianInfo;
import com.wenbingwang.zhifubao.PayResult;
import com.wenbingwang.zhifubao.SignUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity1 extends Pay {
	private EditText Login1_price;
	private Button login1_button1;
	private RadioButton radioButton1, radioButton2, radioButton3;
	private RadioButton r[] = {};
	private int priceBtn = 1;// 1支付宝，2微信，3银联
	private TextView TextView;
	private String money;
	private PayvipInfo payVIPInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity1);
		login1_button1 = (Button) findViewById(R.id.login1_button1);
		Login1_price = (EditText) findViewById(R.id.Login1_price);
		TextView = (android.widget.TextView) findViewById(R.id.textView3);
		doc_price();
		radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
		radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
		radioButton3 = (RadioButton) findViewById(R.id.radioButton3);
		r = new RadioButton[] { radioButton1, radioButton2, radioButton3 };
		login1_button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getPrice(priceBtn, Login1_price.getText().toString(),
						"Recharge", "账号充值", "向账户充值");
			}
		});

	}

	public String radioButton(View v) {
		for (int i = 0; i < r.length; i++) {
			if (v.getId() == r[i].getId()) {
				r[i].setChecked(true);
				priceBtn = i + 1;
			} else {
				r[i].setChecked(false);
			}
		}
		return "" + priceBtn;
	}

	public void doc_price() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/zhbalanceinfo", requestParams,
				new MyHandler());
	}

	private class MyHandler extends JsonHttpResponseHandler {

		@Override
		public void onStart() {// 15618231400
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			showToast("没有更多数据");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				DecimalFormat decimalFormat = new DecimalFormat("0.00 元");
				TextView.setText(decimalFormat.format(Float.parseFloat(response
						.getString("Money"))));
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	void getPrice(int type, String moneys, String s, String name, String some) {
		this.money = check(moneys);
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		if (money == null || money.equals("")) {
			showToast("请输入有效金额");
			return;
		} else {
			requestParams.add("RechargeMoney", money);
			requestParams.add("PayType", type + "");
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/accountpayinfo",
					requestParams, new ZhifuHandler(s, name, some));
		}
	}

	private class ZhifuHandler extends JsonHttpResponseHandler {
		private String a;
		private String b;
		private String c;

		public ZhifuHandler(String a, String b, String c) {
			super();
			this.a = a;
			this.b = b;
			this.c = c;
		}

		@Override
		public void onStart() {
			// Initiated the request
			System.out.println("aa");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				payVIPInfo = new PayvipInfo(response.toString());// 订单(Recharge=id=随机数)
				if (response.getString("SubmitResult").equals("1")) {
					if (priceBtn == 0) {// 余额
						getYue(a + "=" + payVIPInfo.getRechargeID() + "="
								+ SignUtils.getOutTradeNo(), b, c, money);
					}

					if (priceBtn == 1) {// 支付寶
						pay(a + "=" + payVIPInfo.getRechargeID() + "="
								+ SignUtils.getOutTradeNo(), b, c, money);
					} else if (priceBtn == 2) {

						getWeixinPrice(a + "=" + payVIPInfo.getRechargeID()
								+ "=" + SignUtils.getOutTradeNo(), b,
								c + money, money);
					} else if (priceBtn == 3) {
						getYinLianPrice(a + "=" + payVIPInfo.getRechargeID()
								+ "=" + SignUtils.getOutTradeNo(), b,
								c + money, money);
					}
				} else {
					Toast.makeText(getApplicationContext(),
							response.getString("SubmitResult"),
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
