package com.wenbingwang.wenbingapp;

import java.util.HashMap;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.utils.UIHandler;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.app.application.Constants;
import com.wenbingwang.app.application.MyApplication;
import com.wenbingwang.bean.MyInfo;
import com.wenbingwang.mywedgit.ACache;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler.Callback;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentFourUnlogin extends BaseFragment implements
		OnClickListener, PlatformActionListener, Callback {
	private View rootView;
	private Button button1;
	private Button button2;
	private ImageButton button3;
	private ImageButton button4;
	private EditText editText1;
	private EditText editText2;
	private TextView textView;
	private ProgressDialog progressDialog;
	static final int MSG_SHOW_COMPLETE = 1;// 授权成功
	static final int MSG_SHOW_ERROR = 2;// 授权失败
	static final int MSG_SHOW_CANCEL = 3;// 取消授权
	private String ThirdType = "1";// 登录方式
	private String ThirdID,Nickname,HeadImage;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			ShareSDK.initSDK(getActivity());
			rootView = inflater.inflate(R.layout.fragment_four_unlogin,
					container, false);
			button1 = (Button) rootView.findViewById(R.id.un_button1);
			button2 = (Button) rootView.findViewById(R.id.un_button2);
			button3 = (ImageButton) rootView.findViewById(R.id.un_button3);
			button4 = (ImageButton) rootView.findViewById(R.id.un_button4);
			editText1 = (EditText) rootView.findViewById(R.id.editText1);
			editText2 = (EditText) rootView.findViewById(R.id.editText2);
			textView = (TextView) rootView.findViewById(R.id.get_textView2);
			button1.setOnClickListener(this);
			button2.setOnClickListener(this);
			button3.setOnClickListener(this);
			button4.setOnClickListener(this);
			textView.setOnClickListener(this);
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setTitle("登录");
			progressDialog.setMessage("正在登录");
		}
		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	private void Login() {
		if (editText1.getText().toString().equals("")) {
			showToast("请输入账号");
			return;
		}
		if (editText2.getText().toString().equals("")) {
			showToast("请输入密码");
			return;
		}
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserName", editText1.getText().toString());
		requestParams.add("Password", editText2.getText().toString());
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/logininfo", requestParams,
				new LoginHandler());
	}

	private void LoginQQ(String ThirdID, String ThirdType, String Nickname,
			String HeadImage) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("ThirdID", ThirdID);
		requestParams.add("ThirdType", ThirdType);
		requestParams.add("Nickname", Nickname);
		requestParams.add("HeadImage", HeadImage);
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/thirdLogininfo", requestParams,
				new LoginHandler());
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.un_button1:// 注册
			intent.setClass(getActivity(), PRegisterActivity.class);
			startActivity(intent);
			break;
		case R.id.un_button2:
			Login();
			break;
		case R.id.un_button3:// 新浪
			Platform weixinfd = ShareSDK.getPlatform(SinaWeibo.NAME);
			weixinfd.setPlatformActionListener(this);
			weixinfd.SSOSetting(true);
			weixinfd.showUser(null);
			break;
		case R.id.un_button4:// 騰訊
			Platform weixinfds = ShareSDK.getPlatform(QQ.NAME);
			weixinfds.setPlatformActionListener(this);
			weixinfds.SSOSetting(false);
			weixinfds.showUser(null);
			break;
		case R.id.get_textView2:
			intent.setClass(getActivity(), GetPassWord.class);
			startActivity(intent);
			break;
		}
	}

	@Override
	public void onCancel(Platform palt, int action) {
		// TODO Auto-generated method stub
		Message msg = new Message();
		msg.arg1 = 3;
		msg.arg2 = action;
		msg.obj = palt;
		UIHandler.sendMessage(msg, this);
	}

	@Override
	public void onComplete(Platform plat, int action,
			HashMap<String, Object> arg2) {
		// TODO Auto-generated method stub
		if (action == Platform.ACTION_USER_INFOR) {
			PlatformDb platDB = plat.getDb();// 获取平台数据DB
			platDB.getToken();
			platDB.getUserIcon();
			platDB.getUserId();
			platDB.getUserName();
			platDB.getUserGender();
			platDB.getPlatformNname();
			this.ThirdID=platDB.getUserId();
			this.ThirdType=platDB.getPlatformNname();
			this.Nickname=platDB.getUserName();
			this.HeadImage=platDB.getUserIcon();
		}
		Message msg = new Message();
		msg.arg1 = 1;
		msg.arg2 = action;
		msg.obj = plat;
		UIHandler.sendMessage(msg, this);
		plat.removeAccount(true);
	}
	
	//针对qq和微博的参数进行操作
	public void logins(){
		if (ThirdType.equals("QQ")) {
			ThirdType = "2";
		} else if (ThirdType.equals("SinaWeibo")) {
			ThirdType = "3";
		}
		LoginQQ(ThirdID, ThirdType, Nickname,
				HeadImage);
	}

	@Override
	public void onError(Platform palt, int action, Throwable t) {
		// TODO Auto-generated method stub
		t.printStackTrace();
		Message msg = new Message();
		msg.arg1 = 2;
		msg.arg2 = action;
		msg.obj = palt;
		UIHandler.sendMessage(msg, this);
	}

	@Override
	public boolean handleMessage(Message msg) {
		// TODO Auto-generated method stub
		Platform plat = (Platform) msg.obj;
		String text = "---";
		switch (msg.arg1) {
		case 1: {
			// 授权成功
			text = plat.getName() + " 授权成功 " + text;
			Toast.makeText(getActivity(), text, 10000).show();
			logins();
		}
			break;
		case 2: {
			// 授权失败
			text = plat.getName() + " 授权失败 " + text;
			Toast.makeText(getActivity(), "授权失败" + text, 10000).show();
		}
			break;
		case 3: {
			// 取消授权
			text = plat.getName() + " 取消授权" + text;
			Toast.makeText(getActivity(), text, 10000).show();
		}
			break;
		}

		return false;
	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					((MyApplication) getActivity().getApplication())
							.setMyInfo(new MyInfo(jsonObject.toString()));
					RequestParams requestParams = new RequestParams();
					requestParams.add("UserID", ((MyApplication) getActivity()
							.getApplication()).getMyInfo().getUserID());
					asyncHttpClient.post(getActivity(),
							"http://yisheng.wenbing.cn/Info/getperinfo",
							requestParams, new MyHandler1());
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				((MyApplication) getActivity().getApplication()).getMyInfo()
						.setUserInfo(jsonObject.toString());
				ACache mCache = ACache.get(getActivity());
				mCache.put("logininfo", ((MyApplication) getActivity()
						.getApplication()).getMyInfo().toString());
				((MainActivity) getActivity()).LoginStateChange();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

}
