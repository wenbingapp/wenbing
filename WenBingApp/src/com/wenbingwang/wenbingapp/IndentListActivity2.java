package com.wenbingwang.wenbingapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class IndentListActivity2 extends BaseActivity {
	private RadioGroup radioGroup;
	private FragmentMain_One fragmentMain_One;
	private FragmentMain_Two fragmentMain_Two;
	private ArrayList<Fragment> list;
	private int currentFragment = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_one);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		fragmentMain_One = new FragmentMain_One(getIntent().getStringExtra(
				"type"), getIntent().getStringExtra("url"));
		fragmentMain_Two = new FragmentMain_Two(getIntent().getStringExtra(
				"type"), getIntent().getStringExtra("url"));
		list = new ArrayList<Fragment>();
		list.add(fragmentMain_One);
		list.add(fragmentMain_Two);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.radio0:
					changeFragment(0);
					break;
				case R.id.radio1:
					changeFragment(1);
					break;
				default:
					break;
				}
			}
		});
		changeFragment(0);
	}

	private void changeFragment(int id) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		if (!list.get(id).isAdded()) {
			fragmentTransaction.add(R.id.container, list.get(id), "");
			fragmentTransaction.commit();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		}
		if (list.get(currentFragment).isAdded()
				&& !list.get(currentFragment).isDetached()) {
			fragmentTransaction.detach(list.get(currentFragment));
		}
		if (list.get(id).isDetached()) {
			fragmentTransaction.attach(list.get(id));
		}
		fragmentTransaction.commit();
		currentFragment = id;
	}

}
