package com.wenbingwang.wenbingapp;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.wenbingwang.bean.DepartmentInfo;
import com.wenbingwang.bean.DocInfo;
import com.wenbingwang.bean.MainTow_Docinfo;

import android.R.integer;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ImageView.ScaleType;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

public class FragmentTwo extends BaseFragment implements
		SwipeRefreshLayout.OnRefreshListener, OnItemClickListener,
		OnClickListener {
	private View rootView;
	private Button button1;
	private TextView title;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ListView docListView;
	private PopupWindow popupWindow;
	private ListView roomList;
	private Adapter1 adapter1;
	private Adapter2 adapter2;
	private int pageNum = 1;
	private String pageDep = "0";
	private String prePageDep = "0";
	private TextView footer;
	private boolean hasmore;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater
					.inflate(R.layout.fragment_two, container, false);
			title = (TextView) rootView.findViewById(R.id.textView1);
			button1 = (Button) rootView.findViewById(R.id.button1);
			button1.setOnClickListener(this);
			docListView = (ListView) rootView.findViewById(R.id.listView1);
			swipeRefreshLayout = (SwipeRefreshLayout) rootView
					.findViewById(R.id.swipe_container);
			swipeRefreshLayout.setOnRefreshListener(this);
			adapter2 = new Adapter2();
			adapter1 = new Adapter1();
			docListView.setAdapter(adapter2);
			addFooter(docListView, R.layout.fragment_two_doclist_bottom);
			docListView.setOnItemClickListener(this);
			title.setText("全部科室");
			loadDepartment();
			loadDoc(0);
		}
		return rootView;
	}

	private void loadDepartment() {
		asyncHttpClient.get(getActivity(),
				"http://yisheng.wenbing.cn/Info/ksinfo", new DepHandler());
	}

	private void loadDoc(int state) {
		hasmore = true;
		RequestParams requestParams = new RequestParams();
		requestParams.add("Page", pageNum + "");
		requestParams.add("DepartmentID", pageDep);
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/ksysinfo", requestParams,
				new DocHandler(state));
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			showPopup(v, R.layout.popupwin_1);
			break;
		case R.id.cancle:
			popupWindow.dismiss();
			break;
		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (parent == roomList) {
			prePageDep = pageDep;
			pageDep = adapter1.getList().get(position).getDepartmentID();
			title.setText(adapter1.getList().get(position).getDepartmentName());
			pageNum = 1;
			loadDoc(0);
			popupWindow.dismiss();
		}

		if (parent == docListView) {
			if (position == adapter2.getList().size()) {
				if (hasmore) {
					prePageDep = pageDep;
					pageNum++;
					loadDoc(1);
				}
			} else {
				Intent intent = new Intent(getActivity(), Activity5.class);
				intent.putExtra("UserID", adapter2.getList().get(position)
						.getDoctorID());
				startActivity(intent);
			}
		}
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		prePageDep = pageDep;
		pageNum = 1;
		loadDoc(0);
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(getActivity()).inflate(
				R.layout.fragment_two_doclist_bottom, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	private void showPopup(View v, int res) {
		if (popupWindow == null) {
			View inflateview = LayoutInflater.from(getActivity()).inflate(res,
					null);
			popupWindow = new PopupWindow(inflateview);
			popupWindow.setWidth(getActivity().getWindowManager()
					.getDefaultDisplay().getWidth() / 4 * 3);
			popupWindow.setHeight(getActivity().getWindowManager()
					.getDefaultDisplay().getHeight() / 4 * 3);
			popupWindow.setFocusable(true);
			popupWindow.setBackgroundDrawable(new PaintDrawable());
			popupWindow.setAnimationStyle(R.style.popup_anim);
			popupWindow.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
					WindowManager.LayoutParams lp = getActivity().getWindow()
							.getAttributes();
					lp.alpha = 1f;
					getActivity().getWindow().setAttributes(lp);
				}
			});
			roomList = (ListView) inflateview.findViewById(R.id.listView1);
			roomList.setAdapter(adapter1);
			roomList.setOnItemClickListener(this);
			((Button) inflateview.findViewById(R.id.cancle))
					.setOnClickListener(this);
		}
		WindowManager.LayoutParams lp = getActivity().getWindow()
				.getAttributes();
		lp.alpha = 0.4f;
		getActivity().getWindow().setAttributes(lp);
		popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	private class Adapter1 extends BaseAdapter {
		private ArrayList<DepartmentInfo> list;

		public Adapter1() {
			list = new ArrayList<DepartmentInfo>();
		}

		public ArrayList<DepartmentInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			convertView = LayoutInflater.from(getActivity()).inflate(
					R.layout.fragment_two_room, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.textView1);
			textView.setText(list.get(position).getDepartmentName());
			return convertView;
		}

	}

	private class Adapter2 extends BaseAdapter {
		private ArrayList<MainTow_Docinfo> list;

		public Adapter2() {
			list = new ArrayList<MainTow_Docinfo>();
		}

		public ArrayList<MainTow_Docinfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(getActivity()).inflate(
						R.layout.fragment_two_doc, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.textView1);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.textView2);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.textView3);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.textView4);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText(list.get(position).getDoctorName());
			viewHolder.textView2
					.setText(list.get(position).getDoctorHospital());
			viewHolder.textView3.setText(list.get(position).getDoctorAbility());
			viewHolder.textView4.setText(list.get(position).getDoctorLevel());
			imageLoader.displayImage(
					list.get(position).getDoctorHeadImageUrl(),
					viewHolder.imageView, options);
			return convertView;
		}

		private class ViewHolder {
			public TextView textView1;
			public TextView textView2;
			public TextView textView3;
			public TextView textView4;
			private ImageView imageView;
		}

	}

	private class DepHandler extends JsonHttpResponseHandler {

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				adapter1.getList().clear();
				adapter1.notifyDataSetChanged();
				DepartmentInfo departmentInfos = new DepartmentInfo();
				departmentInfos.setDepartmentID("0");
				departmentInfos.setDepartmentName("所有科室");
				adapter1.getList().add(departmentInfos);
				for (int i = 0; i < response.length(); i++) {
					DepartmentInfo departmentInfo = new DepartmentInfo();
					departmentInfo.setDepartmentID(response.getJSONObject(i)
							.getString("DepartmentID"));
					departmentInfo.setDepartmentName(response.getJSONObject(i)
							.getString("DepartmentName"));
					adapter1.getList().add(departmentInfo);
				}
				adapter1.notifyDataSetChanged();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

	}

	private class DocHandler extends JsonHttpResponseHandler {

		private int state;

		public DocHandler(int num) {
			state = num;
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			if (!prePageDep.equals(pageDep)) {
				adapter2.getList().clear();
				adapter2.notifyDataSetChanged();
			}
			hasmore = false;
			footer.setText("没有更多数据");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter2.getList().clear();
					adapter2.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					adapter2.getList().add(
							new MainTow_Docinfo(response.getJSONObject(i)
									.toString()));
					adapter2.notifyDataSetChanged();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

	}

}
