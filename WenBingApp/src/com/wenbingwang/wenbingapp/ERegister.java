package com.wenbingwang.wenbingapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ERegister extends BaseActivity implements OnClickListener {

	private Button ere_btn;
	private EditText ere_email, ere_password, ere_checkpassword;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eregister);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("用户注册");
		ere_btn = (Button) findViewById(R.id.ere_btn);
		ere_email = (EditText) findViewById(R.id.ere_email);
		ere_password = (EditText) findViewById(R.id.ere_password);
		ere_checkpassword = (EditText) findViewById(R.id.ere_checkpassword);
		ere_btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ere_btn:

			if (ere_password.getText().toString().equals(""))
				showToast("请输入您的密码");
			if (ere_checkpassword.getText().toString().equals("") != ere_password
					.getText().toString().equals(""))
				showToast("两次的密码不同");
			else {
				if (!isEmail(ere_email.getText().toString())) {
					showToast("请输入正确的邮箱");
				} else {
					setRegister();
				}

			}
			break;
		}
	}

	private void setRegister() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("Email", ere_email.getText().toString());
		requestParams.add("Password", ere_password.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/yxinfo",
				requestParams, new Pregister2Handler());

	}

	private class Pregister2Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	public static boolean isEmail(String strEmail) {
		// String strPattern =
		// "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		String strPattern = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
		Pattern p = Pattern.compile(strPattern);
		Matcher m = p.matcher(strEmail);
		return m.matches();
	}
}
