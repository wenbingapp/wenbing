package com.wenbingwang.wenbingapp;

import java.text.DateFormat.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.bean.HomeDoctorForms;
import com.wenbingwang.bean.PayResult_gh;
import com.wenbingwang.bean.RecvProblemItemInfo;

import de.greenrobot.event.EventBus;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class HomeDocterFormsActivity extends BaseActivity implements
		OnRefreshListener, OnItemClickListener {
	private TextView doc_textview;
	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private TextView footer;
	private boolean hasmore;
	private String type = "";// 作为对话框提醒目前操作到哪一步的参数
	private ArrayList<HomeDoctorForms> list;
	public static final int REQUSET = 1;
	private EditText et_search;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doctor);
		doc_textview = (TextView) findViewById(R.id.doc_textview);
		doc_textview.setText("家庭医生");
		EventBus.getDefault().register(this);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void loads(String phone, int position) {// 电话咨询
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("TelephoneType", "2");
		requestParams.add("TelephonePrice", "0.00");
		// requestParams.add("DepartmentID", "");
		requestParams.add("DoctorID", list.get(position).getDoctorID());
		requestParams.add("UserTelephone", phone);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/hzkjdhinfo",
				requestParams, new MyHandlers());

	}

	private class MyHandlers extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			showToast("正在加载数据");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				if (response.getString("SubmitResult").equals("1")) {
					showToast("提交成功");
				} else {
					showToast(response.getString("SubmitDescription")
							.toString());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private void load(int s) {// 家庭医生列表
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", getMyInfo().getUserID());
		requestParams.add("Page", page + "");
		hasmore = true;
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/jtyslistinfo", requestParams,
				new MyHandler(s));
	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					HomeDoctorForms indentListInfo = new HomeDoctorForms(
							response.getJSONObject(i).toString());
					adapter.getList().add(indentListInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private class Adapter extends BaseAdapter {

		public Adapter() {
			list = new ArrayList<HomeDoctorForms>();
		}

		public ArrayList<HomeDoctorForms> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(HomeDocterFormsActivity.this)
						.inflate(R.layout.doctors, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.fans_activity_name);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.fans_item_age);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.fans_item_time);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.fans_item_address);
				viewHolder.textView5 = (TextView) convertView
						.findViewById(R.id.fans_item_age1);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.fans_item_image);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			if ((list.get(position).getDoctorName()).length() > 3) {
				viewHolder.textView1.setText("医生："
						+ (list.get(position).getDoctorName()).substring(0, 2)
						+ "...");
			} else {
				viewHolder.textView1.setText("医生："
						+ list.get(position).getDoctorName());
			}
			// sex:1 男 2 女
			viewHolder.textView2.setText("科室："
					+ list.get(position).getDoctorDepartment());
			type = list.get(position).getFormStatus();
			if ((list.get(position).getFormStatus()).equals("0")) {
				viewHolder.textView3.setText("订单状态：待付款");
			}
			if ((list.get(position).getFormStatus()).equals("1")) {
				viewHolder.textView3.setText("订单状态：服务中");
			}
			if ((list.get(position).getFormStatus()).equals("2")) {
				viewHolder.textView3.setText("订单状态：待评价");
			}
			if ((list.get(position).getFormStatus()).equals("3")) {
				viewHolder.textView3.setText("订单状态：已完成");
			}
			if ((list.get(position).getFormStatus()).equals("0")) {
				viewHolder.textView4.setText("服务时间：付款成功之后开始计算");
			} else {
				viewHolder.textView4.setText("服务时间："
						+ time(list.get(position).getBeginTime()) + "至"
						+ time(list.get(position).getEndTime()));
			}
			if ((list.get(position).getHomeDoctorType()).equals("0")) {
				viewHolder.textView5.setText("购买服务：一周");
			} else if ((list.get(position).getHomeDoctorType()).equals("1")) {
				viewHolder.textView5.setText("购买服务：一月");
			}
			imageLoader.displayImage(list.get(position).getDoctorHeadImage(),
					viewHolder.imageView, options);
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
		public TextView textView4;
		public TextView textView5;
		private ImageView imageView;
	}

	public void onEventMainThread(PayResult_gh event) {
		String s = event.state;
		if (s.equals("0")) {
			onRefresh();
		} else if (s.equals("1")) {
			return;
		} else if (s.equals("2")) {
			return;
		}

	}

	public void onItemClick(AdapterView<?> parent, final View view,
			final int position, long id) {
		// TODO Auto-generated method stub
		final EditText editText = new EditText(this);
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		} else {
			if ((list.get(position).getFormStatus()).equals("0")) {
				new AlertDialog.Builder(this)
						.setTitle("家庭医生")
						.setMessage("该订单尚未付款,是否进行付款")
						.setPositiveButton("付款",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(
												HomeDocterFormsActivity.this,
												PayActivity.class);
										intent.putExtra("orderid",
												list.get(position)
														.getHomeDoctorID());
										intent.putExtra("oderprice",
												list.get(position)
														.getHomeDoctorPrice());
										intent.putExtra("oderhead",
												"HomeDoctor");
										intent.putExtra("odername", "家庭医生");
										intent.putExtra("oderinfo",
												"向医生进行了家庭医生");
										if ((list.get(position)
												.getHomeDoctorType())
												.equals("0")) {
											intent.putExtra(
													"ordershow",
													"时间：一周        "
															+ "金额："
															+ list.get(position)
																	.getHomeDoctorPrice()
															+ "元");
										} else if ((list.get(position)
												.getHomeDoctorType())
												.equals("1")) {
											intent.putExtra(
													"ordershow",
													"时间：一月        "
															+ "金额："
															+ list.get(position)
																	.getHomeDoctorPrice()
															+ "元");
										}
										intent.putExtra("ordertitle", "家庭医生");
										startActivity(intent);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("1")) {

				new AlertDialog.Builder(this)
						.setTitle("家庭医生")
						.setMessage("请选择你将要发起的服务")
						.setPositiveButton("图文咨询",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(
												HomeDocterFormsActivity.this,
												TuWenActivity.class);
										intent.putExtra("DoctorID",
												list.get(position)
														.getDoctorID());
										intent.putExtra("DoctorPrice", list
												.get(position)
												.getHomeDoctorPrice());
										intent.putExtra("DoctorName",
												list.get(position)
														.getDoctorName());
										startActivity(intent);
									}
								})
						.setNegativeButton("电话咨询",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										search(position);
									}
								}).show();
			}
			if ((list.get(position).getFormStatus()).equals("2")) {
				new AlertDialog.Builder(this)
						.setTitle("家庭医生")
						.setMessage("您尚未对该医生评价，请前往评价")
						.setPositiveButton("确定",
								new AlertDialog.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										Intent intent = new Intent();
										intent.setClass(
												HomeDocterFormsActivity.this,
												PingjiaActivity.class);
										intent.putExtra("DoctorID",
												list.get(position)
														.getDoctorID());
										intent.putExtra("HomeDoctorID", list
												.get(position)
												.getHomeDoctorID());
										intent.putExtra("HomeDoctorType", "1");
										startActivity(intent);
									}
								}).setNegativeButton("取消", null).show();
			}
			if ((list.get(position).getFormStatus()).equals("3")) {
				new AlertDialog.Builder(this)
						.setTitle("家庭医生")
						.setMessage(
								"您购买" + list.get(position).getDoctorName()
										+ "医生的家庭医生服务已经结束")
						.setPositiveButton("确定", null).show();
			}
		}
	}

	public void search(final int position) {// 电话咨询（弹框）
		Builder dialog = new AlertDialog.Builder(this);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout layout = (LinearLayout) inflater.inflate(
				R.layout.dialogview, null);
		dialog.setView(layout);
		et_search = (EditText) layout.findViewById(R.id.searchC);
		dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String searchC = et_search.getText().toString();
				if (isMobile(searchC)) {
					loads(searchC, position);
				} else {
					showToast("请输入正确的手机号码");
				}

			}
		});
		dialog.setTitle("家庭医生").setNegativeButton("取消", null);
		dialog.show();
	}

	/**
	 * 手机号验证
	 * 
	 * @param str
	 * @return 验证通过返回true
	 */
	public static boolean isMobile(String str) {
		Pattern p = null;
		Matcher m = null;
		boolean b = false;
		p = Pattern.compile("^[1][3,4,5,8][0-9]{9}$"); // 验证手机号
		m = p.matcher(str);
		b = m.matches();
		return b;
	}

	private String time(String nowTime) {// 2015-06-25 13:57:17
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str1 = formatter2.format(curDate);
		String dayStr = formatter1.format(curDate);
		// 当前时间
		String now = nowTime.substring(0, 10);// 输入的年月日
		String now1 = nowTime.substring(0, 7);// 输入的年月
		String nowyestoday = nowTime.substring(8, 10);// 输入的日
		String today = str1.substring(0, 10);// 当前的年月日
		String today2 = str1.substring(0, 7);// 当前的年月
		String today1 = str1.substring(8, 10);
		System.out.println(Integer.valueOf(today1).intValue());
		int yestodays = (Integer.parseInt(today1)) - 1;
		String yes = "" + yestodays;
		if (now.equals(today)) {
			return "今天 " + dayStr;
		} else if (nowyestoday.equals(yes) && now1.equals(today2)) {
			return "昨天 " + dayStr;
		} else {
			return nowTime.substring(5, 16);
		}
	}
}
