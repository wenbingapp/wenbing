package com.wenbingwang.wenbingapp;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.mywedgit.ACache;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class GetPassWord1 extends BaseActivity implements OnClickListener {
	private Button get1password_back;
	private EditText get_password1, get_password2, get_password3;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getpassword2_activity);
		get1password_back = (Button) findViewById(R.id.get1password_back);
		get_password1 = (EditText) findViewById(R.id.get_password1);
		get_password2 = (EditText) findViewById(R.id.get_password2);
		get_password3 = (EditText) findViewById(R.id.get_password3);
		get1password_back.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("忘记密码");
		progressDialog.setMessage("正在提交请求");

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.get1password_back:
			setPassword();
			break;
		}
	}

	private void setPassword() {
		if (get_password2.getText().toString()
				.equals(get_password2.getText().toString())) {
			RequestParams requestParams = new RequestParams();
			requestParams.add("UserName", getIntent().getStringExtra("phone"));
			requestParams
					.add("NewPassword", get_password2.getText().toString());
			requestParams.add("ConfirmationCode", get_password1.getText()
					.toString());
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/gzxgwjinfo", requestParams,
					new LoginHandler());
		} else {// 13022157610
			showToast("两次输入密码不一致");
		}

	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					ACache mCache = ACache.get(GetPassWord1.this);
					mCache.remove("logininfo");
					Intent intent = new Intent();
					intent.setClass(GetPassWord1.this, MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("login", "");
					startActivity(intent);
					finish();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

}
