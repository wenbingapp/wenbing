package com.wenbingwang.bean;

import org.json.JSONObject;

public class YinlianInfo {

	private String UnionpayFormID;
	private String SubmitResult;
	private String SubmitDescription;

	public YinlianInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			UnionpayFormID = jsonObject.getString("UnionpayFormID");
			if (UnionpayFormID.contains("_")) {
				UnionpayFormID = UnionpayFormID.split("_")[1];
			}
			SubmitResult = jsonObject.getString("SubmitResult");
			SubmitDescription = jsonObject.getString("SubmitDescription");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getUnionpayFormID() {
		return UnionpayFormID;
	}

	public void setUnionpayFormID(String unionpayFormID) {
		UnionpayFormID = unionpayFormID;
	}

	public String getSubmitResult() {
		return SubmitResult;
	}

	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}

	public String getSubmitDescription() {
		return SubmitDescription;
	}

	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}

}
