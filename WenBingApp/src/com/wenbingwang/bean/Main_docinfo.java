package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class Main_docinfo {

	public Main_docinfo() {

	}

	public Main_docinfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			DoctorID = jsonObject.getString("DoctorID");
			DoctorName = jsonObject.getString("DoctorName");
			DoctorHeadImageUrl = jsonObject.getString("DoctorHeadImageUrl");
			DoctorDepartment = jsonObject.getString("DoctorDepartment");
			DoctorLevel = jsonObject.getString("DoctorLevel");
			DoctorAbility = jsonObject.getString("DoctorAbility");
			HomeDoctorOpen = jsonObject.getString("HomeDoctorOpen");
			ImageWordOpen = jsonObject.getString("ImageWordOpen");
			TelephoneOpen = jsonObject.getString("TelephoneOpen");
			OutpatientOpen = jsonObject.getString("OutpatientOpen");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getDoctorID() {
		return DoctorID;
	}

	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}

	public String getDoctorHeadImageUrl() {
		return DoctorHeadImageUrl;
	}

	public void setDoctorHeadImageUrl(String doctorHeadImageUrl) {
		DoctorHeadImageUrl = doctorHeadImageUrl;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getDoctorDepartment() {
		return DoctorDepartment;
	}

	public void setDoctorDepartment(String doctorDepartment) {
		DoctorDepartment = doctorDepartment;
	}

	public String getDoctorLevel() {
		return DoctorLevel;
	}

	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}

	public String getDoctorAbility() {
		return DoctorAbility;
	}

	public void setDoctorAbility(String doctorAbility) {
		DoctorAbility = doctorAbility;
	}

	public String getHomeDoctorOpen() {
		return HomeDoctorOpen;
	}

	public void setHomeDoctorOpen(String homeDoctorOpen) {
		HomeDoctorOpen = homeDoctorOpen;
	}

	public String getImageWordOpen() {
		return ImageWordOpen;
	}

	public void setImageWordOpen(String imageWordOpen) {
		ImageWordOpen = imageWordOpen;
	}

	public String getTelephoneOpen() {
		return TelephoneOpen;
	}

	public void setTelephoneOpen(String telephoneOpen) {
		TelephoneOpen = telephoneOpen;
	}

	public String getOutpatientOpen() {
		return OutpatientOpen;
	}

	public void setOutpatientOpen(String outpatientOpen) {
		OutpatientOpen = outpatientOpen;
	}

	private String DoctorID;
	private String DoctorHeadImageUrl;
	private String DoctorName;
	private String DoctorDepartment;
	private String DoctorLevel;
	private String DoctorAbility;
	private String HomeDoctorOpen;
	private String ImageWordOpen;
	private String TelephoneOpen;
	private String OutpatientOpen;
}
