package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class HospitalInfo {

	public String getPurchaseNumber() {
		return PurchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		PurchaseNumber = purchaseNumber;
	}

	public String getPurchasePrice() {
		return PurchasePrice;
	}

	public void setPurchasePrice(String purchasePrice) {
		PurchasePrice = purchasePrice;
	}

	public String getHospitalDescription() {
		return HospitalDescription;
	}

	public void setHospitalDescription(String hospitalDescription) {
		HospitalDescription = hospitalDescription;
	}

	public String getHospitalHeadImageUrl() {
		return HospitalHeadImageUrl;
	}

	public void setHospitalHeadImageUrl(String hospitalHeadImageUrl) {
		HospitalHeadImageUrl = hospitalHeadImageUrl;
	}

	public String getHospitalName() {
		return HospitalName;
	}

	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}

	public String getHospitalTelephone() {
		return HospitalTelephone;
	}

	public void setHospitalTelephone(String hospitalTelephone) {
		HospitalTelephone = hospitalTelephone;
	}

	public String getHospitalAddress() {
		return HospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		HospitalAddress = hospitalAddress;
	}

	public String getCommentValue() {
		return CommentValue;
	}

	public void setCommentValue(String commentValue) {
		CommentValue = commentValue;
	}

	private String PurchaseNumber;
	private String PurchasePrice;
	private String HospitalDescription;
	private String HospitalHeadImageUrl;
	private String HospitalName;
	private String HospitalTelephone;
	private String HospitalAddress;
	private String CommentValue;

	public HospitalInfo() {

	}

	public HospitalInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			PurchaseNumber = jsonObject.getString("PurchaseNumber");
			PurchasePrice = jsonObject.getString("PurchasePrice");
			HospitalDescription = jsonObject.getString("HospitalDescription");
			HospitalHeadImageUrl = jsonObject.getString("HospitalHeadImageUrl");
			HospitalName = jsonObject.getString("HospitalName");
			HospitalTelephone = jsonObject.getString("HospitalTelephone");
			HospitalAddress = jsonObject.getString("HospitalAddress");
			CommentValue = jsonObject.getString("CommentValue");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
