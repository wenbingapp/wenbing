package com.wenbingwang.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeDocterOutpatients implements Serializable {
	private String OutpatientID;
	private String FormStatus;
	private String OutpatientPrice;
	private String DetailDescription;
	private String Sex;
	private String Age;
	private String ImageArray;
	private String OutpatientBeginTime;
	private String OutpatientEndTime;
	private String Telephone;
	private String OutpatientConfirmTime;
	private String DoctorID;
	private String SubmitTime;
	private String DoctorHeadImage;
	private String DoctorNickname;
	private String DoctorLevel;
	private String DoctorHospital;
	private String DoctorDepartment;

	public HomeDocterOutpatients(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			OutpatientID = jsonObject.getString("OutpatientID");
			if (OutpatientID.contains("_")) {
				OutpatientID = OutpatientID.split("_")[1];
			}
			FormStatus = jsonObject.getString("FormStatus");
			OutpatientPrice = jsonObject.getString("OutpatientPrice");
			DetailDescription = jsonObject.getString("DetailDescription");
			Sex = jsonObject.getString("Sex");
			Age = jsonObject.getString("Age");
			ImageArray = jsonObject.getString("ImageArray");
			OutpatientBeginTime = jsonObject.getString("OutpatientBeginTime");
			SubmitTime = jsonObject.getString("SubmitTime");
			OutpatientEndTime = jsonObject.getString("OutpatientEndTime");
			Telephone = jsonObject.getString("Telephone");
			OutpatientConfirmTime = jsonObject
					.getString("OutpatientConfirmTime");
			DoctorID = jsonObject.getString("DoctorID");
			DoctorHeadImage = jsonObject.getString("DoctorHeadImage");
			DoctorNickname = jsonObject.getString("DoctorNickname");
			DoctorLevel = jsonObject.getString("DoctorLevel");
			DoctorHospital = jsonObject.getString("DoctorHospital");
			DoctorDepartment = jsonObject.getString("DoctorDepartment");

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public String getOutpatientID() {
		return OutpatientID;
	}

	public void setOutpatientID(String outpatientID) {
		OutpatientID = outpatientID;
	}

	public String getFormStatus() {
		return FormStatus;
	}

	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}

	public String getOutpatientPrice() {
		return OutpatientPrice;
	}

	public void setOutpatientPrice(String outpatientPrice) {
		OutpatientPrice = outpatientPrice;
	}

	public String getDetailDescription() {
		return DetailDescription;
	}

	public void setDetailDescription(String detailDescription) {
		DetailDescription = detailDescription;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getImageArray() {
		return ImageArray;
	}

	public void setImageArray(String imageArray) {
		ImageArray = imageArray;
	}

	public String getOutpatientBeginTime() {
		return OutpatientBeginTime;
	}

	public void setOutpatientBeginTime(String outpatientBeginTime) {
		OutpatientBeginTime = outpatientBeginTime;
	}

	public String getOutpatientEndTime() {
		return OutpatientEndTime;
	}

	public void setOutpatientEndTime(String outpatientEndTime) {
		OutpatientEndTime = outpatientEndTime;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}

	public String getOutpatientConfirmTime() {
		return OutpatientConfirmTime;
	}

	public void setOutpatientConfirmTime(String outpatientConfirmTime) {
		OutpatientConfirmTime = outpatientConfirmTime;
	}

	public String getDoctorID() {
		return DoctorID;
	}

	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}

	public String getSubmitTime() {
		return SubmitTime;
	}

	public void setSubmitTime(String submitTime) {
		SubmitTime = submitTime;
	}

	public String getDoctorHeadImage() {
		return DoctorHeadImage;
	}

	public void setDoctorHeadImage(String doctorHeadImage) {
		DoctorHeadImage = doctorHeadImage;
	}

	public String getDoctorNickname() {
		return DoctorNickname;
	}

	public void setDoctorNickname(String doctorNickname) {
		DoctorNickname = doctorNickname;
	}

	public String getDoctorLevel() {
		return DoctorLevel;
	}

	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}

	public String getDoctorHospital() {
		return DoctorHospital;
	}

	public void setDoctorHospital(String doctorHospital) {
		DoctorHospital = doctorHospital;
	}

	public String getDoctorDepartment() {
		return DoctorDepartment;
	}

	public void setDoctorDepartment(String doctorDepartment) {
		DoctorDepartment = doctorDepartment;
	}

}
