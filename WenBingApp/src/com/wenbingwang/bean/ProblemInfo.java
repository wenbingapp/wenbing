package com.wenbingwang.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class ProblemInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String QuestionID;

	public String getQuestionID() {
		return QuestionID;
	}

	public void setQuestionID(String questionID) {
		QuestionID = questionID;
	}

	public String getSenderID() {
		return SenderID;
	}

	public void setSenderID(String senderID) {
		SenderID = senderID;
	}

	public String getSenderVoIP() {
		return SenderVoIP;
	}

	public void setSenderVoIP(String senderVoIP) {
		SenderVoIP = senderVoIP;
	}

	public String getSenderHeadImage() {
		return SenderHeadImage;
	}

	public void setSenderHeadImage(String senderHeadImage) {
		SenderHeadImage = senderHeadImage;
	}

	public String getSenderNickname() {
		return SenderNickname;
	}

	public void setSenderNickname(String senderNickname) {
		SenderNickname = senderNickname;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getQuestionTitle() {
		return QuestionTitle;
	}

	public void setQuestionTitle(String questionTitle) {
		QuestionTitle = questionTitle;
	}

	public String getQuestionImage() {
		return QuestionImage;
	}

	public void setQuestionImage(String questionImage) {
		QuestionImage = questionImage;
	}

	public String getQuestionDetail() {
		return QuestionDetail;
	}

	public void setQuestionDetail(String questionDetail) {
		QuestionDetail = questionDetail;
	}

	public String getSubmitTime() {
		return SubmitTime;
	}

	public void setSubmitTime(String submitTime) {
		SubmitTime = submitTime;
	}

	private String SenderID;
	private String SenderVoIP;
	private String SenderHeadImage;
	private String SenderNickname;
	private String Age;
	private String Sex;
	private String QuestionTitle;
	private String QuestionImage;
	private String QuestionDetail;
	private String SubmitTime;

	public String getFormStatus() {
		return FormStatus;
	}

	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}

	public String getAccepterTime() {
		return AccepterTime;
	}

	public void setAccepterTime(String accepterTime) {
		AccepterTime = accepterTime;
	}

	private String FormStatus;
	private String AccepterTime;
	public boolean hasRead;
	private String ImageWordPrice;

	public String getImageWordPrice() {
		return ImageWordPrice;
	}

	public void setImageWordPrice(String imageWordPrice) {
		ImageWordPrice = imageWordPrice;
	}

	public ProblemInfo(String s, String string) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			QuestionID = jsonObject.getString("QuestionID");
			if (string != null) {
				QuestionID = string + "_" + QuestionID;
			}
			SenderID = jsonObject.getString("AccepterID");
			SenderVoIP = jsonObject.getString("AccepterVoIP");
			SenderHeadImage = jsonObject.getString("AccepterHeadImage");
			SenderNickname = jsonObject.getString("AccepterNickname");
			Age = jsonObject.getString("Age");
			Sex = jsonObject.getString("Sex");
			QuestionTitle = jsonObject.getString("QuestionTitle");
			QuestionImage = jsonObject.getString("QuestionImage");
			QuestionDetail = jsonObject.getString("QuestionDetail");
			SubmitTime = jsonObject.getString("SubmitTime");
			AccepterTime = jsonObject.getString("AccepterTime");
			FormStatus = jsonObject.getString("FormStatus");
			ImageWordPrice = jsonObject.getString("ImageWordPrice");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
