package com.wenbingwang.bean;

public class SendProblemItemInfo {

	private String QuestionID;
	private ProblemInfo problemInfo;

	public ProblemInfo getProblemInfo() {
		return problemInfo;
	}

	public void setProblemInfo(ProblemInfo problemInfo) {
		this.problemInfo = problemInfo;
	}

	public String getQuestionID() {
		return QuestionID;
	}

	public void setQuestionID(String questionID) {
		QuestionID = questionID;
	}

}
