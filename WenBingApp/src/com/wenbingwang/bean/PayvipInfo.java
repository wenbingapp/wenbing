package com.wenbingwang.bean;

import org.json.JSONObject;

public class PayvipInfo {
	
	private String RechargeID;
	private String SubmitResult;
	private String SubmitDescription;
	
	public PayvipInfo(String s) {
		
		try {
			JSONObject jsonObject=new JSONObject(s);
			RechargeID=jsonObject.getString("RechargeID");
			SubmitResult=jsonObject.getString("SubmitResult");
			SubmitDescription=jsonObject.getString("SubmitDescription");
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public String getRechargeID() {
		return RechargeID;
	}
	public void setRechargeID(String rechargeID) {
		RechargeID = rechargeID;
	}
	public String getSubmitResult() {
		return SubmitResult;
	}
	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}
	public String getSubmitDescription() {
		return SubmitDescription;
	}
	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}
	
	

}
