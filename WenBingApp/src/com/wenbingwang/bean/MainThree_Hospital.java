package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class MainThree_Hospital {
	public String getHospitalID() {
		return HospitalID;
	}

	public void setHospitalID(String hospitalID) {
		HospitalID = hospitalID;
	}

	public String getHospitalHeadImageUrl() {
		return HospitalHeadImageUrl;
	}

	public void setHospitalHeadImageUrl(String hospitalHeadImageUrl) {
		HospitalHeadImageUrl = hospitalHeadImageUrl;
	}

	public String getHospitalName() {
		return HospitalName;
	}

	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}

	public String getHospitalTelephone() {
		return HospitalTelephone;
	}

	public void setHospitalTelephone(String hospitalTelephone) {
		HospitalTelephone = hospitalTelephone;
	}

	public String getHospitalAddress() {
		return HospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		HospitalAddress = hospitalAddress;
	}

	private String HospitalID;
	private String HospitalHeadImageUrl;
	private String HospitalName;
	private String HospitalTelephone;
	private String HospitalAddress;

	public MainThree_Hospital() {

	}

	public MainThree_Hospital(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			HospitalID = jsonObject.getString("HospitalID");
			HospitalHeadImageUrl = jsonObject.getString("HospitalHeadImageUrl");
			HospitalName = jsonObject.getString("HospitalName");
			HospitalTelephone = jsonObject.getString("HospitalTelephone");
			HospitalAddress = jsonObject.getString("HospitalAddress");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
