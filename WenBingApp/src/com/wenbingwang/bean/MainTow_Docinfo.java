package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class MainTow_Docinfo {

	public String getDoctorAbility() {
		return DoctorAbility;
	}

	public void setDoctorAbility(String doctorAbility) {
		DoctorAbility = doctorAbility;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getDoctorID() {
		return DoctorID;
	}

	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}

	public String getDoctorLevel() {
		return DoctorLevel;
	}

	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}

	public String getDoctorHeadImageUrl() {
		return DoctorHeadImageUrl;
	}

	public void setDoctorHeadImageUrl(String doctorHeadImageUrl) {
		DoctorHeadImageUrl = doctorHeadImageUrl;
	}

	private String DoctorAbility;
	private String DoctorName;
	private String DoctorID;
	private String DoctorLevel;
	private String DoctorHeadImageUrl;
	private String DoctorHospital;

	public String getDoctorHospital() {
		return DoctorHospital;
	}

	public void setDoctorHospital(String doctorHospital) {
		DoctorHospital = doctorHospital;
	}

	public MainTow_Docinfo() {

	}

	public MainTow_Docinfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			DoctorAbility = jsonObject.getString("DoctorAbility");
			DoctorName = jsonObject.getString("DoctorName");
			DoctorID = jsonObject.getString("DoctorID");
			DoctorLevel = jsonObject.getString("DoctorLevel");
			DoctorHeadImageUrl = jsonObject.getString("DoctorHeadImageUrl");
			DoctorHospital = jsonObject.getString("DoctorHospital");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
