package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class City {

	public City() {

	}

	public City(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			CityID = jsonObject.getString("CityID");
			CityName = jsonObject.getString("CityName");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getCityID() {
		return CityID;
	}

	public void setCityID(String cityID) {
		CityID = cityID;
	}

	public String getCityName() {
		return CityName;
	}

	public void setCityName(String cityName) {
		CityName = cityName;
	}

	private String CityID;
	private String CityName;
}
