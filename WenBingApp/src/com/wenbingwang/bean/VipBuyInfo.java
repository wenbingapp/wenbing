package com.wenbingwang.bean;

import org.json.JSONObject;

public class VipBuyInfo {
	
	private String VIPID;
	private String SubmitResult;
	private String SubmitDescription;
	public String getVIPID() {
		return VIPID;
	}
	public void setVIPID(String vIPID) {
		VIPID = vIPID;
	}
	public String getSubmitResult() {
		return SubmitResult;
	}
	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}
	public String getSubmitDescription() {
		return SubmitDescription;
	}
	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}
	public VipBuyInfo(String s) {
		try {
			JSONObject jsonObject=new JSONObject(s);
			VIPID=jsonObject.getString("VIPID");
			SubmitResult=jsonObject.getString("SubmitResult");
			SubmitDescription=jsonObject.getString("SubmitDescription");
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
}
