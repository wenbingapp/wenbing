package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class ProblemItemInfo {

	public ProblemItemInfo(String s, String d) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			QuestionID = jsonObject.getString("QuestionID");
			if (d != null) {
				QuestionID = d + "_" + QuestionID;
			}
			SenderID = jsonObject.getString("SenderID");
			SenderVoIP = jsonObject.getString("SenderVoIP");
			TextContent = jsonObject.getString("TextContent");
			SendTime = jsonObject.getString("SendTime");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ProblemItemInfo() {

	}

	private String QuestionID;

	public String getQuestionID() {
		return QuestionID;
	}

	public void setQuestionID(String questionID) {
		QuestionID = questionID;
	}

	public String getSenderID() {
		return SenderID;
	}

	public void setSenderID(String senderID) {
		SenderID = senderID;
	}

	public String getSenderVoIP() {
		return SenderVoIP;
	}

	public void setSenderVoIP(String senderVoIP) {
		SenderVoIP = senderVoIP;
	}

	public String getTextContent() {
		return TextContent;
	}

	public void setTextContent(String textContent) {
		TextContent = textContent;
	}

	public String getSendTime() {
		return SendTime;
	}

	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}

	private String SenderID;
	private String SenderVoIP;
	private String TextContent;
	private String SendTime;
}
