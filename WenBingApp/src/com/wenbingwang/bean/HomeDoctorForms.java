package com.wenbingwang.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeDoctorForms implements Serializable {

	private String HomeDoctorID;
	private String DoctorID;
	private String DoctorHeadImage;
	private String DoctorName;
	private String DoctorDepartment;
	private String HomeDoctorType;
	private String FormStatus;
	private String HomeDoctorPrice;
	private String BeginTime;
	private String EndTime;

	public HomeDoctorForms(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			HomeDoctorID = jsonObject.getString("HomeDoctorID");
			if (HomeDoctorID.contains("_")) {
				HomeDoctorID = HomeDoctorID.split("_")[1];
			}
			DoctorID = jsonObject.getString("DoctorID");
			DoctorHeadImage = jsonObject.getString("DoctorHeadImage");
			DoctorName = jsonObject.getString("DoctorName");
			DoctorDepartment = jsonObject.getString("DoctorDepartment");
			HomeDoctorType = jsonObject.getString("HomeDoctorType");
			FormStatus = jsonObject.getString("FormStatus");
			HomeDoctorPrice = jsonObject.getString("HomeDoctorPrice");
			BeginTime = jsonObject.getString("BeginTime");
			EndTime = jsonObject.getString("EndTime");

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public String getHomeDoctorID() {
		return HomeDoctorID;
	}

	public void setHomeDoctorID(String homeDoctorID) {
		HomeDoctorID = homeDoctorID;
	}

	public String getDoctorID() {
		return DoctorID;
	}

	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}

	public String getDoctorHeadImage() {
		return DoctorHeadImage;
	}

	public void setDoctorHeadImage(String doctorHeadImage) {
		DoctorHeadImage = doctorHeadImage;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getDoctorDepartment() {
		return DoctorDepartment;
	}

	public void setDoctorDepartment(String doctorDepartment) {
		DoctorDepartment = doctorDepartment;
	}

	public String getHomeDoctorType() {
		return HomeDoctorType;
	}

	public void setHomeDoctorType(String homeDoctorType) {
		HomeDoctorType = homeDoctorType;
	}

	public String getFormStatus() {
		return FormStatus;
	}

	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}

	public String getHomeDoctorPrice() {
		return HomeDoctorPrice;
	}

	public void setHomeDoctorPrice(String homeDoctorPrice) {
		HomeDoctorPrice = homeDoctorPrice;
	}

	public String getBeginTime() {
		return BeginTime;
	}

	public void setBeginTime(String beginTime) {
		BeginTime = beginTime;
	}

	public String getEndTime() {
		return EndTime;
	}

	public void setEndTime(String endTime) {
		EndTime = endTime;
	}

}
