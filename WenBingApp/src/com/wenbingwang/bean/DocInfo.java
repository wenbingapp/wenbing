package com.wenbingwang.bean;

public class DocInfo {
	public String getDoctorID() {
		return DoctorID;
	}

	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}

	public String getDoctorVoIP() {
		return DoctorVoIP;
	}

	public void setDoctorVoIP(String doctorVoIP) {
		DoctorVoIP = doctorVoIP;
	}

	public String getDoctorHeadImageUrl() {
		return DoctorHeadImageUrl;
	}

	public void setDoctorHeadImageUrl(String doctorHeadImageUrl) {
		DoctorHeadImageUrl = doctorHeadImageUrl;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getDoctorDepartment() {
		return DoctorDepartment;
	}

	public void setDoctorDepartment(String doctorDepartment) {
		DoctorDepartment = doctorDepartment;
	}

	public String getDoctorHospital() {
		return DoctorHospital;
	}

	public void setDoctorHospital(String doctorHospital) {
		DoctorHospital = doctorHospital;
	}

	public String getDoctorAbility() {
		return DoctorAbility;
	}

	public void setDoctorAbility(String doctorAbility) {
		DoctorAbility = doctorAbility;
	}

	public String getDoctorBriefIntroduction() {
		return DoctorBriefIntroduction;
	}

	public void setDoctorBriefIntroduction(String doctorBriefIntroduction) {
		DoctorBriefIntroduction = doctorBriefIntroduction;
	}

	private String DoctorID;
	private String DoctorVoIP;
	private String DoctorHeadImageUrl;
	private String DoctorName;
	private String DoctorDepartment;
	private String DoctorHospital;
	private String DoctorAbility;
	private String DoctorBriefIntroduction;
	private String DoctorLevel;

	public String getDoctorLevel() {
		return DoctorLevel;
	}

	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}
}
