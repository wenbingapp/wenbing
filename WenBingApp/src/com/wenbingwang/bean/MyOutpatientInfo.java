package com.wenbingwang.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class MyOutpatientInfo implements Serializable{
	
	private String DiagnosisID;
	private String FormStatus;
	private String DiagnosisPrice;
	private String HospitalID;
	private String HospitalHeadImage;
	private String HospitalName;
	private String HospitalDepartment;
	private String Telephone;
	private String DetailDescription;
	
	public MyOutpatientInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			DiagnosisID = jsonObject.getString("DiagnosisID");
			if (DiagnosisID.contains("_")) {
				DiagnosisID = DiagnosisID.split("_")[1];
			}
			FormStatus = jsonObject.getString("FormStatus");
			DiagnosisPrice = jsonObject.getString("DiagnosisPrice");
			HospitalID = jsonObject.getString("HospitalID");
			HospitalHeadImage = jsonObject.getString("HospitalHeadImage");
			HospitalName = jsonObject.getString("HospitalName");
			HospitalDepartment = jsonObject.getString("HospitalDepartment");
			Telephone = jsonObject.getString("Telephone");
			DetailDescription = jsonObject.getString("DetailDescription");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getDiagnosisID() {
		return DiagnosisID;
	}
	public void setDiagnosisID(String diagnosisID) {
		DiagnosisID = diagnosisID;
	}
	public String getFormStatus() {
		return FormStatus;
	}
	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}
	public String getDiagnosisPrice() {
		return DiagnosisPrice;
	}
	public void setDiagnosisPrice(String diagnosisPrice) {
		DiagnosisPrice = diagnosisPrice;
	}
	public String getHospitalID() {
		return HospitalID;
	}
	public void setHospitalID(String hospitalID) {
		HospitalID = hospitalID;
	}
	public String getHospitalHeadImage() {
		return HospitalHeadImage;
	}
	public void setHospitalHeadImage(String hospitalHeadImage) {
		HospitalHeadImage = hospitalHeadImage;
	}
	public String getHospitalName() {
		return HospitalName;
	}
	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}
	public String getHospitalDepartment() {
		return HospitalDepartment;
	}
	public void setHospitalDepartment(String hospitalDepartment) {
		HospitalDepartment = hospitalDepartment;
	}
	public String getTelephone() {
		return Telephone;
	}
	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	public String getDetailDescription() {
		return DetailDescription;
	}
	public void setDetailDescription(String detailDescription) {
		DetailDescription = detailDescription;
	}
	
	

}
