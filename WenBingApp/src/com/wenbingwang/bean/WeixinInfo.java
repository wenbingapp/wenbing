package com.wenbingwang.bean;

import org.json.JSONObject;

public class WeixinInfo {

	private String PrepayID;
	private String NonceString;
	private String TimeStamp;
	private String Sign;
	private String SubmitResult;
	private String SubmitDescription;

	public WeixinInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			PrepayID = jsonObject.getString("PrepayID");
			if (PrepayID.contains("_")) {
				PrepayID = PrepayID.split("_")[1];
			}
			NonceString = jsonObject.getString("NonceString");
			TimeStamp = jsonObject.getString("TimeStamp");
			Sign = jsonObject.getString("Sign");
			SubmitResult = jsonObject.getString("SubmitResult");
			SubmitDescription = jsonObject.getString("SubmitDescription");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getPrepayID() {
		return PrepayID;
	}

	public void setPrepayID(String prepayID) {
		PrepayID = prepayID;
	}

	public String getNonceString() {
		return NonceString;
	}

	public void setNonceString(String nonceString) {
		NonceString = nonceString;
	}

	public String getTimeStamp() {
		return TimeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		TimeStamp = timeStamp;
	}

	public String getSign() {
		return Sign;
	}

	public void setSign(String sign) {
		Sign = sign;
	}

	public String getSubmitResult() {
		return SubmitResult;
	}

	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}

	public String getSubmitDescription() {
		return SubmitDescription;
	}

	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}

}
