package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class MyInfo {

	private String UserID;

	public MyInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			UserID = jsonObject.getString("UserID");
			SubAccount = jsonObject.getString("SubAccount");
			SubPassword = jsonObject.getString("SubPassword");
			VoIPAccount = jsonObject.getString("VoIPAccount");
			VoIPPassword = jsonObject.getString("VoIPPassword");
			SubmitResult = jsonObject.getString("SubmitResult");
			SubmitDescription = jsonObject.getString("SubmitDescription");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUserHeadImageUrl() {
		return UserHeadImageUrl;
	}

	public void setUserHeadImageUrl(String userHeadImageUrl) {
		UserHeadImageUrl = userHeadImageUrl;
	}

	public String getNickName() {
		return NickName;
	}

	public void setNickName(String nickName) {
		NickName = nickName;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getVip() {
		return Vip;
	}

	public void setVip(String vip) {
		Vip = vip;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	private String UserHeadImageUrl;
	private String NickName;
	private String Sex;
	private String Age;
	private String Vip;
	private String Address;

	public void setUserInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			UserHeadImageUrl = jsonObject.getString("UserHeadImageUrl");
			NickName = jsonObject.getString("NickName");
			Sex = jsonObject.getString("Sex");
			Age = jsonObject.getString("Age");
			Vip = jsonObject.getString("Vip");
			Address = jsonObject.getString("Address");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getSubAccount() {
		return SubAccount;
	}

	public void setSubAccount(String subAccount) {
		SubAccount = subAccount;
	}

	public String getSubPassword() {
		return SubPassword;
	}

	public void setSubPassword(String subPassword) {
		SubPassword = subPassword;
	}

	public String getVoIPAccount() {
		return VoIPAccount;
	}

	public void setVoIPAccount(String voIPAccount) {
		VoIPAccount = voIPAccount;
	}

	public String getVoIPPassword() {
		return VoIPPassword;
	}

	public void setVoIPPassword(String voIPPassword) {
		VoIPPassword = voIPPassword;
	}

	public String getSubmitResult() {
		return SubmitResult;
	}

	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}

	public String getSubmitDescription() {
		return SubmitDescription;
	}

	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}

	private String SubAccount;
	private String SubPassword;
	private String VoIPAccount;
	private String VoIPPassword;
	private String SubmitResult;
	private String SubmitDescription;

	@Override
	public String toString() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("UserID", UserID);
			jsonObject.put("SubAccount", SubAccount);
			jsonObject.put("SubPassword", SubPassword);
			jsonObject.put("VoIPAccount", VoIPAccount);
			jsonObject.put("VoIPPassword", VoIPPassword);
			jsonObject.put("SubmitResult", SubmitResult);
			jsonObject.put("SubmitDescription", SubmitDescription);
			jsonObject.put("UserHeadImageUrl", UserHeadImageUrl);
			jsonObject.put("NickName", NickName);
			jsonObject.put("Sex", Sex);
			jsonObject.put("Age", Age);
			jsonObject.put("Vip", Vip);
			jsonObject.put("Address", Address);
			return jsonObject.toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
