package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class RecvProblemItemInfo {

	public String getSenderNickname() {
		return SenderNickname;
	}

	public void setSenderNickname(String senderNickname) {
		SenderNickname = senderNickname;
	}

	public String getSenderHeadImage() {
		return SenderHeadImage;
	}

	public void setSenderHeadImage(String senderHeadImage) {
		SenderHeadImage = senderHeadImage;
	}

	private String SenderNickname;
	private String SenderHeadImage;

	public RecvProblemItemInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			QuestionID = jsonObject.getString("QuestionID");
			SenderID = jsonObject.getString("SenderID");
			SenderVoIP = jsonObject.getString("SenderVoIP");
			TextContent = jsonObject.getString("TextContent");
			SendTime = jsonObject.getString("SendTime");
			SenderHeadImage = jsonObject.getString("SenderHeadImage");
			SenderNickname = jsonObject.getString("SenderNickname");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public RecvProblemItemInfo() {

	}

	private String QuestionID;

	public String getQuestionID() {
		return QuestionID;
	}

	public void setQuestionID(String questionID) {
		QuestionID = questionID;
	}

	public String getSenderID() {
		return SenderID;
	}

	public void setSenderID(String senderID) {
		SenderID = senderID;
	}

	public String getSenderVoIP() {
		return SenderVoIP;
	}

	public void setSenderVoIP(String senderVoIP) {
		SenderVoIP = senderVoIP;
	}

	public String getTextContent() {
		return TextContent;
	}

	public void setTextContent(String textContent) {
		TextContent = textContent;
	}

	public String getSendTime() {
		return SendTime;
	}

	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}

	private String SenderID;
	private String SenderVoIP;
	private String TextContent;
	private String SendTime;

	public ProblemItemInfo toProblemItemInfo() {
		ProblemItemInfo problemItemInfo = new ProblemItemInfo();
		problemItemInfo.setQuestionID(QuestionID);
		problemItemInfo.setSenderID(SenderID);
		problemItemInfo.setSenderVoIP(SenderVoIP);
		problemItemInfo.setSendTime(SendTime);
		problemItemInfo.setTextContent(TextContent);
		return problemItemInfo;
	}
}
