package com.wenbingwang.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeDocterFans implements Serializable{

	private String DoctorID;
	private String DoctorHeadImageUrl;
	private String DoctorName;
	private String DoctorLevel;
	private String DoctorHospital;
	private String DoctorAbility;

	public HomeDocterFans(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			DoctorID = jsonObject.getString("DoctorID");
			if (DoctorID.contains("_")) {
				DoctorID = DoctorID.split("_")[1];
			}
			DoctorHeadImageUrl = jsonObject.getString("DoctorHeadImageUrl");
			DoctorName = jsonObject.getString("DoctorName");
			DoctorLevel = jsonObject.getString("DoctorLevel");
			DoctorHospital = jsonObject.getString("DoctorHospital");
			DoctorAbility = jsonObject.getString("DoctorAbility");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public String getDoctorID() {
		return DoctorID;
	}

	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}

	public String getDoctorHeadImageUrl() {
		return DoctorHeadImageUrl;
	}

	public void setDoctorHeadImageUrl(String doctorHeadImageUrl) {
		DoctorHeadImageUrl = doctorHeadImageUrl;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getDoctorLevel() {
		return DoctorLevel;
	}

	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}

	public String getDoctorHospital() {
		return DoctorHospital;
	}

	public void setDoctorHospital(String doctorHospital) {
		DoctorHospital = doctorHospital;
	}

	public String getDoctorAbility() {
		return DoctorAbility;
	}

	public void setDoctorAbility(String doctorAbility) {
		DoctorAbility = doctorAbility;
	}

}
