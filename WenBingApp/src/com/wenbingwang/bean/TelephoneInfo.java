package com.wenbingwang.bean;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class TelephoneInfo implements Serializable{
	
	private String TelephoneID;
	private String FormStatus;
	private String TelephoneType;
	private String TelephonePrice;
	private String DoctorDepartment;
	private String UserTelephone;
	private String SubmitTime;
	private String DoctorID;
	private String DoctorHeadImage;
	private String DoctorNickname;
	
	public TelephoneInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			TelephoneID = jsonObject.getString("TelephoneID");
			if (TelephoneID.contains("_")) {
				TelephoneID = TelephoneID.split("_")[1];
			}
			FormStatus = jsonObject.getString("FormStatus");
			TelephoneType = jsonObject.getString("TelephoneType");
			TelephonePrice = jsonObject.getString("TelephonePrice");
			DoctorDepartment = jsonObject.getString("DoctorDepartment");
			UserTelephone = jsonObject.getString("UserTelephone");
			SubmitTime = jsonObject.getString("SubmitTime");
			DoctorHeadImage = jsonObject.getString("DoctorHeadImage");
			DoctorNickname = jsonObject.getString("DoctorNickname");

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	public String getTelephoneID() {
		return TelephoneID;
	}
	public void setTelephoneID(String telephoneID) {
		TelephoneID = telephoneID;
	}
	public String getFormStatus() {
		return FormStatus;
	}
	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}
	public String getTelephoneType() {
		return TelephoneType;
	}
	public void setTelephoneType(String telephoneType) {
		TelephoneType = telephoneType;
	}
	public String getTelephonePrice() {
		return TelephonePrice;
	}
	public void setTelephonePrice(String telephonePrice) {
		TelephonePrice = telephonePrice;
	}
	public String getDoctorDepartment() {
		return DoctorDepartment;
	}
	public void setDoctorDepartment(String doctorDepartment) {
		DoctorDepartment = doctorDepartment;
	}
	public String getUserTelephone() {
		return UserTelephone;
	}
	public void setUserTelephone(String userTelephone) {
		UserTelephone = userTelephone;
	}
	public String getSubmitTime() {
		return SubmitTime;
	}
	public void setSubmitTime(String submitTime) {
		SubmitTime = submitTime;
	}
	public String getDoctorID() {
		return DoctorID;
	}
	public void setDoctorID(String doctorID) {
		DoctorID = doctorID;
	}
	public String getDoctorHeadImage() {
		return DoctorHeadImage;
	}
	public void setDoctorHeadImage(String doctorHeadImage) {
		DoctorHeadImage = doctorHeadImage;
	}
	public String getDoctorNickname() {
		return DoctorNickname;
	}
	public void setDoctorNickname(String doctorNickname) {
		DoctorNickname = doctorNickname;
	}
	
	

}
