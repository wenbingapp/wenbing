package com.wenbingwang.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class Ad {

	private static String url = "http://yisheng.wenbing.cn/Datarele/advert?id=";

	public Ad() {

	}

	public Ad(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			ADID = jsonObject.getString("ADID");
			ADImageUrls = jsonObject.getString("ADImageUrls");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUrl() {
		return url + ADID;
	}

	public String getADID() {
		return ADID;
	}

	public void setADID(String aDID) {
		ADID = aDID;
	}

	public String getADImageUrls() {
		return ADImageUrls;
	}

	public void setADImageUrls(String aDImageUrls) {
		ADImageUrls = aDImageUrls;
	}

	private String ADID;
	private String ADImageUrls;

}
