package com.wenbingwang.app.application;

public class Constants {

	/**
	 * 微信支付需要的参数
	 */
	// appid
	// 请同时修改 androidmanifest.xml里面，.PayActivityd里的属性<data
	// android:scheme="wxb4ba3c02aa476ea1"/>为新设置的appid
	public static final String APP_ID = "wx4cf3841f6481855a";
	// 商户号
	public static final String MCH_ID = "1238755502";
	// API密钥，在商户平台设置
	public static final String API_KEY = "192006250b4c09247ec02edce69f6a2d";

	/**
	 * 支付宝需要的参数
	 */
	// 商户PID
	public static final String PARTNER = "2088911368315364";// pid=2088911368315364
	// 商户收款账号
	public static final String SELLER = "wenbingwang_ios@sina.com";// appid=2015042300049110
	// 商户私钥，pkcs8格式
	public static final String RSA_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJmdKGX9n3R9IoAmm4wIAqfHa0kH5QC0xNXyqCS65jtWJfxguSIXuC14jR3QQacrCM12JXCTmDJY7G3dUs35sZrh08tclXRMcAp6Mw7s/5XWz8V4r40f9RNF/Xtr3FynKfzbWh5uQyVCoy85SDn6SIptKJuUb3g8waOrHa364l69AgMBAAECgYBVMApPRS9AcoTHfKGWLnsKgFTG01Hz6kueOkt00xEKpuO56gnBFlZVIR1hAzTld8AR8QsiOzeT9CxpYOuivm33En3B9a3TwH1GY1PpZ/6ZACYD6QvuiaY5s+gbk6gZTnrsIntstvTLDfWwFDugWc6JJy+1sE9RROZrduO7qw08IQJBAMkCaSGD3LOMgk2aHXTbNCsqTkIjBsm3WtnqU0xalmoaShrDEtqc5ZxGKnjvepm+n7IhDcn1C4/4NLQnKCVr8kMCQQDDo21AT5FMPrb/RoaLhtGXXfwYEIAZWV1mtXMZVr544C0p6X3Wsh/ZTIWODlUstEWG9KRjJxIz6tjA30nURdr/AkEAhS7qt4EmF3Uu1PobMFUZQMx8I8gPdFHfiWmAHaQb61tV5FuCP4Ubgarf81dBVWL/bsgF2d1Z9t9PFFqpd/oI/wJAYV93Iho4PFW+qWIF125b5VX/Ak88IuHKzjMjvYniNbGGaSTw4Co6FeKzWpghpqV3MYuUQlGRCswp5B1+1quKKwJBAMHzKUxKVUmJL/GS02KsfH2/bPrcYbBlxlpiyBmmgm4g7Eor9N3K95b39k680WSahqTEKuZdCRuyj2yFKRmiTKg=";
	// 支付宝公钥
	public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCZnShl/Z90fSKAJpuMCAKnx2tJ"
			+ "B+UAtMTV8qgkuuY7ViX8YLkiF7gteI0d0EGnKwjNdiVwk5gyWOxt3VLN+bGa4dPL"
			+ "XJV0THAKejMO7P+V1s/FeK+NH/UTRf17a9xcpyn821oebkMlQqMvOUg5+kiKbSib"
			+ "lG94PMGjqx2t+uJevQIDAQAB";

}
