package com.wenbingwang.app.application;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.ECInitialize;
import com.speedtong.sdk.ECDevice.InitListener;
import com.wenbingwang.bean.MyInfo;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {

	private AsyncHttpClient asyncHttpClient;
	private MyInfo myInfo;
	public String question;
	public ECInitialize params;
	public String checkupdate = "0";
	public String forceupdate = "0";
	public String url;

	public MyInfo getMyInfo() {
		return myInfo;
	}

	public void setMyInfo(MyInfo m) {
		this.myInfo = m;
		if (m != null) {
			ECInitialize params = new ECInitialize();
			params.setServerIP("https://app.cloopen.com");
			params.setServerPort(8883);
			params.setSid(m.getVoIPAccount());
			params.setSidToken(m.getVoIPPassword());
			params.setSubId(m.getSubAccount());
			params.setSubToken(m.getSubPassword());
			this.params = params;
		} else {
			this.params = null;
		}
	}

	private void init() {
		ECDevice.initial(this, new InitListener() {

			@Override
			public void onInitialized() {
				// TODO Auto-generated method stub
			}

			@Override
			public void onError(Exception arg0) {
				// TODO Auto-generated method stub
				int i = 0;
				i++;
			}
		});
	}

	public AsyncHttpClient getAsyncHttpClient() {
		return asyncHttpClient;
	}

	private PersistentCookieStore myCookieStore;

	@Override
	public void onCreate() {
		super.onCreate();
		asyncHttpClient = new AsyncHttpClient();
		myCookieStore = new PersistentCookieStore(this);
		asyncHttpClient.setCookieStore(myCookieStore);
		asyncHttpClient.setTimeout(5000);
		initImageLoader(getApplicationContext());
		init();
	}

	private void initImageLoader(Context context) {
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(
				context);
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.writeDebugLogs(); // Remove for release app

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config.build());
	}

}
