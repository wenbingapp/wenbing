package com.wenbingwang.mywidget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class MyLinerLayout extends LinearLayout {
	public MyLinerLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public MyLinerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		heightMeasureSpec = MeasureSpec.makeMeasureSpec(
				MeasureSpec.getSize(widthMeasureSpec),
				MeasureSpec.getMode(heightMeasureSpec));
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
}
