package com.wenbingwang.mywidget;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MyArrayAdapter<T> extends ArrayAdapter<T> {

	public MyArrayAdapter(Context context, int resource, T[] objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
	}

	public MyArrayAdapter(Context context, int resource, List<T> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
	}

	private boolean gh;
	private T s;

	@Override
	public T getItem(int position) {
		if (gh == true) {
			return s;
		} else {
			return super.getItem(position);
		}
	}

	public void setShowText(T ss) {
		s = ss;
		gh = true;
		notifyDataSetChanged();
	}

	// @Override
	// public long getItemId(int position) {
	// if (position < 0) {
	// return 0;
	// } else {
	// return position;
	// }
	// }

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		gh = false;
		return super.getDropDownView(position, convertView, parent);
	}

}
