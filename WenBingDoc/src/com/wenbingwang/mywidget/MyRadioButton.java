package com.wenbingwang.mywidget;

import java.util.Locale;

import com.wenbingwang.wenbingdoc.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;

public class MyRadioButton extends RadioButton {

	private Drawable iconDrawable;
	private String text;
	private boolean dot = false;
	private boolean isleft;
	final float scale = getResources().getDisplayMetrics().density;

	public MyRadioButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public MyRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.com_wenbingwang_mywedgit_MyRadioButton, 0, 0);
		iconDrawable = a
				.getDrawable(R.styleable.com_wenbingwang_mywedgit_MyRadioButton_myicon);
		text = a.getString(R.styleable.com_wenbingwang_mywedgit_MyRadioButton_mytext);
		isleft = a
				.getBoolean(
						R.styleable.com_wenbingwang_mywedgit_MyRadioButton_isleft,
						true);
		a.recycle();
	}

	public MyRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG
				| Paint.FILTER_BITMAP_FLAG));
		int w = (int) (getWidth()  - 35 * scale);
		int offset = 0;
		if (!isleft) {
			offset = (int) (35 * scale);
		}
		int h = getHeight();
		Paint textPaint = new Paint();
		textPaint.setColor(getCurrentTextColor());
		textPaint.setTextSize(getTextSize());
		int middleh = w / 2;
		int middlev = h / 2;
		int contentLength = (int) (textPaint.measureText(text)
				+ (int) (25 * scale) + textPaint.getTextSize() * 2 / 5 + textPaint
				.measureText(text) / 6);
		if (text != null) {
			canvas.drawText(text, middleh - contentLength / 2
					+ (int) (25 * scale) + textPaint.getTextSize() * 2 / 5
					+ offset, middlev + textPaint.getTextSize() / 2, textPaint);
		}
		if (iconDrawable != null) {
			iconDrawable.setBounds(middleh - contentLength / 2 + offset,
					middlev - (int) (12.5 * scale), middleh - contentLength / 2
							+ (int) (25 * scale) + offset, middlev
							+ (int) (12.5 * scale));
			iconDrawable.draw(canvas);
		}
		if (dot == true) {
			textPaint.setColor(0xffff0000);
			canvas.drawCircle(
					middleh - contentLength / 2 + textPaint.measureText(text)
							+ (int) (25 * scale) + textPaint.getTextSize() * 2
							/ 5 + textPaint.measureText(text) / 6 + offset,
					middlev - textPaint.getTextSize() / 2,
					textPaint.getTextSize() * 2 / 5, textPaint);
		}
	}

	public void setRedDot(boolean f) {
		dot = f;
		postInvalidate();
	}

	@Override
	protected void drawableStateChanged() {
		if (iconDrawable != null) {
			int[] myDrawableState = getDrawableState();
			// Set the state of the Drawable
			iconDrawable.setState(myDrawableState);
		}
		super.drawableStateChanged();
	}

}
