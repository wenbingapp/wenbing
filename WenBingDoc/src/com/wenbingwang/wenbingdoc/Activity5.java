package com.wenbingwang.wenbingdoc;

import java.text.DecimalFormat;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.DocInfo;
import com.wenbingwang.doc.application.HomeInfo;
import com.wenbingwang.doc.application.MyApplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Activity5 extends BaseActivity implements OnClickListener,
		OnRefreshListener {
	private Button activity5_back;
	private HomeInfo homeInfo;
	private ImageView activity5_item_image;
	private TextView activity5_item_name, activity5_item_time,
			activity5_hospital, activity5_shanchang1, activity5_shanchang3,
			activity5_name1, activity5_man, activity5_names, activity5_names3,
			activity5_names4, activity5_man2, activity5_man3, activity5_man4;
	private RelativeLayout activity5_relative, activity5_relative2,
			activity5_relative3, activity5_relative4;
	private String a, b, c, d;// 判断服务是否开启
	private SwipeRefreshLayout swipeRefreshLayout;
	private View zy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		doble("11.0153");
		setContentView(R.layout.activity5);
		a = "0";
		d = "0";
		c = "0";
		b = "0";
		zy = findViewById(R.id.zy);
		activity5_back = (Button) findViewById(R.id.activity5_back);
		activity5_item_image = (ImageView) findViewById(R.id.activity5_item_image);
		activity5_item_name = (TextView) findViewById(R.id.activity5_item_name);
		activity5_item_time = (TextView) findViewById(R.id.activity5_item_time);
		activity5_hospital = (TextView) findViewById(R.id.activity5_hospital);
		activity5_shanchang1 = (TextView) findViewById(R.id.activity5_shanchang1);
		activity5_shanchang3 = (TextView) findViewById(R.id.activity5_shanchang3);
		activity5_name1 = (TextView) findViewById(R.id.activity5_name1);
		activity5_names = (TextView) findViewById(R.id.activity5_names);
		activity5_names3 = (TextView) findViewById(R.id.activity5_names3);
		activity5_names4 = (TextView) findViewById(R.id.activity5_names4);
		activity5_man = (TextView) findViewById(R.id.activity5_man);
		activity5_man2 = (TextView) findViewById(R.id.activity5_man2);
		activity5_man3 = (TextView) findViewById(R.id.activity5_man3);
		activity5_man4 = (TextView) findViewById(R.id.activity5_man4);
		activity5_relative = (RelativeLayout) findViewById(R.id.activity5_relative);
		activity5_relative2 = (RelativeLayout) findViewById(R.id.activity5_relative2);
		activity5_relative3 = (RelativeLayout) findViewById(R.id.activity5_relative3);
		activity5_relative4 = (RelativeLayout) findViewById(R.id.activity5_relative4);
		activity5_relative.setOnClickListener(this);
		activity5_relative2.setOnClickListener(this);
		activity5_relative3.setOnClickListener(this);
		activity5_relative4.setOnClickListener(this);
		activity5_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Activity5.this.finish();
			}
		});
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		onRefresh();
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/zyylinfo",
				requestParams, new MyHandler1());
	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				homeInfo = new HomeInfo(jsonObject.toString());
				show();
				zy.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private void show() {
		imageLoader.displayImage(homeInfo.getDoctorHeadImageUrl(),
				activity5_item_image, options);
		activity5_item_name.setText(homeInfo.getDoctorNickName().toString());
		activity5_item_time.setText(homeInfo.getDoctorLevel().toString());
		activity5_hospital.setText(homeInfo.getDoctorHospital().toString());
		activity5_shanchang1.setText(homeInfo.getDoctorAbility().toString());
		activity5_shanchang3.setText(homeInfo.getDoctorBriefIntroduction()
				.toString());
		a = homeInfo.getHomeDoctorOpen().toString();
		b = homeInfo.getImageWordOpen().toString();
		c = homeInfo.getTelephoneOpen().toString();
		d = homeInfo.getOutpatientOpen().toString();
		if (a.equals("1")) {
			activity5_name1.setText("￥"
					+ doble(homeInfo.getHomeDoctorWeekPrice().toString())
					+ "/周   " + "￥"
					+ doble(homeInfo.getHomeDoctorMonthPrice().toString())
					+ "/月   ");
		}
		if (b.equals("1")) {
			activity5_names.setText("￥"
					+ doble(homeInfo.getImageWordPrice().toString()) + "/次   ");
		}
		if (c.equals("1")) {
			activity5_names3.setText("￥"
					+ doble(homeInfo.getTelephonePrice().toString()) + "/次   ");
		}
		if (d.equals("1")) {
			activity5_names4
					.setText("￥"
							+ doble(homeInfo.getOutpatientPrice().toString())
							+ "/次   ");
		}
		activity5_man.setText(homeInfo.getHomeDoctorBuys().toString() + "人购买");
		activity5_man2.setText(homeInfo.getImageWordBuys().toString() + "人购买");
		activity5_man3.setText(homeInfo.getTelephoneBuys().toString() + "人购买");
		activity5_man4.setText(homeInfo.getOutpatientBuys().toString() + "人购买");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.activity5_relative:
			if (a.equals("1")) {
				intent.setClass(Activity5.this, Activity5_item.class);
				intent.putExtra("getHomeDoctorWeekPrice", "￥"
						+ doble(homeInfo.getHomeDoctorWeekPrice().toString())
						+ "/周   " + "￥"
						+ doble(homeInfo.getHomeDoctorMonthPrice().toString())
						+ "/月   ");
				intent.putExtra("getHomeDoctorBuys", homeInfo
						.getHomeDoctorBuys().toString() + "人购买");
				intent.putExtra("getHomeDoctorValue",
						homeInfo.getHomeDoctorValue());
				intent.putExtra("getImageWordPrice", doble(homeInfo
						.getImageWordPrice().toString()) + "/次   ");
				intent.putExtra("getTelephonePrice", doble(homeInfo
						.getTelephonePrice().toString()) + "/次   ");
				startActivity(intent);
			} else {
				showToast("该服务未开启");
			}
			break;
		case R.id.activity5_relative2:
			if (b.equals("1")) {
				intent.setClass(Activity5.this, Activity5_item2.class);
				intent.putExtra(
						"price2",
						(homeInfo.getImageWordPrice()).subSequence(0,
								(homeInfo.getImageWordPrice()).indexOf(".")));
				intent.putExtra("buys2", homeInfo.getImageWordBuys());
				intent.putExtra("value2", homeInfo.getImageWordValue());
				startActivity(intent);
			} else {
				showToast("该服务未开启");
			}
			break;
		case R.id.activity5_relative3:
			if (c.equals("1")) {
				intent.setClass(Activity5.this, Activity5_item3.class);
				intent.putExtra("price3", homeInfo.getTelephonePrice());
				intent.putExtra("buys3", homeInfo.getTelephoneBuys());
				intent.putExtra("value3", homeInfo.getTelephoneValue());
				startActivity(intent);
			} else {
				showToast("该服务未开启");
			}
			break;
		case R.id.activity5_relative4:
			if (d.equals("1")) {
				intent.setClass(Activity5.this, Activity5_item4.class);
				intent.putExtra("price4", homeInfo.getOutpatientPrice());
				intent.putExtra("buys4", homeInfo.getOutpatientBuys());
				intent.putExtra("value4", homeInfo.getOutpatientValue());
				startActivity(intent);
			} else {
				showToast("该服务未开启");
			}
			break;
		}
	}

}
