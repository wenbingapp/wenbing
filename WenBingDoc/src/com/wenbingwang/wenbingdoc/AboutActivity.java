package com.wenbingwang.wenbingdoc;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends Activity {

	private Button about_back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_activity);
		about_back = (Button) findViewById(R.id.about_back);
		about_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AboutActivity.this.finish();
			}
		});
		((TextView) findViewById(R.id.t1)).setText("当前版本：" + getVersionName(this));
		((TextView) findViewById(R.id.t2)).setText("2000-# 问病网  wenbing.cn"
				.replace("#", (new Date().getYear() + 1900) + ""));
	}

	public static String getVersionName(Context c) {
		try {
			PackageManager manager = c.getPackageManager();
			PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
			String version = info.versionName;
			return version;
		} catch (Exception e) {
			e.printStackTrace();
			return "未知";
		}
	}
	
	public static String getVersionCode(Context c) {
		try {
			PackageManager manager = c.getPackageManager();
			PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
			String version = info.versionCode+"";
			return version;
		} catch (Exception e) {
			e.printStackTrace();
			return "未知";
		}
	}

}
