package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.mywidget.MyArrayAdapter;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class Activity3 extends BaseActivity implements OnClickListener,
		OnRefreshListener {
	private static final String[] m = { "请选择价格", "0.00元/次", "1.00元/次",
			"2.00元/次", "6.00元/次", "12.00元/次", "18.00元/次", "25.00元/次", "自定义其他金额" };
	private Spinner spinner;
	private MyArrayAdapter<String> adapter;
	private Button activity3_btn, activity3_back, activity3_button1;
	private int onclik = 0;// 0 未选中 1选中
	private String phonePrice = null;
	private String phoneTime = "00:00,23:59|00:00,23:59|00:00,23:59|00:00,23:59|00:00,23:59|00:00,23:59|00:00,23:59";
	private ToggleButton activity3_toggle;
	private EditText activity3_textwenzi2;
	private SwipeRefreshLayout swipeRefreshLayout;
	private View zy;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity3);
		zy=findViewById(R.id.zy);
		activity3_button1 = (Button) findViewById(R.id.activity3_button1);
		activity3_btn = (Button) findViewById(R.id.activity3_btn);
		activity3_back = (Button) findViewById(R.id.activity3_back);
		activity3_toggle = (ToggleButton) findViewById(R.id.activity3_toggle);
		activity3_textwenzi2 = (EditText) findViewById(R.id.activity3_textwenzi2);
		activity3_back.setOnClickListener(this);
		activity3_btn.setOnClickListener(this);
		activity3_button1.setOnClickListener(this);
		spinner = (Spinner) findViewById(R.id.spinner3);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		// 将可选内容与ArrayAdapter连接起来
		adapter = new MyArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, m);

		// 设置下拉列表的风格
		adapter.setDropDownViewResource(R.layout.spinner_item);

		// 将adapter 添加到spinner中
		spinner.setAdapter(adapter);

		// 添加事件Spinner事件监听
		spinner.setOnItemSelectedListener(new SpinnerSelectedListener());

		// 开关切换事件
		activity3_toggle.setOnToggleChanged(new OnToggleChanged() {
			@Override
			public void onToggle(boolean on) {

				if (on == true) {
					onclik = 1;
				} else {
					onclik = 0;
				}
			}
		});
		onRefresh();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.activity3_btn:
			Intent intent = new Intent();
			intent.setClass(Activity3.this, SetTimeActivity.class);
			intent.putExtra("time", phoneTime);
			startActivityForResult(intent, 0);
			break;
		case R.id.activity3_back:
			Activity3.this.finish();
			break;
		case R.id.activity3_button1:// 提交
			getPhone();
			break;
		}

	}

	// 使用数组形式操作
	class SpinnerSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			switch (arg2) {
			case 1:
				phonePrice = "0";
				break;
			case 2:
				phonePrice = "1";
				break;
			case 3:
				phonePrice = "2";
				break;
			case 4:
				phonePrice = "6";
				break;
			case 5:
				phonePrice = "12";
				break;
			case 6:
				phonePrice = "18";
				break;
			case 7:
				phonePrice = "25";
				break;
			case 8:
				showDialog();
				break;
			}
		}

		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}

	private void getPhone() {
		String phones = activity3_textwenzi2.getText().toString();
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("TelephoneOpen", onclik + "");
		requestParams.add("TelephonePrice", phonePrice);
		requestParams.add("TelephoneTime", phoneTime);
		if (activity3_textwenzi2.getText().toString() == null
				|| activity3_textwenzi2.getText().toString().length() == 0) {
			showToast("请输正确的手机号码");
		} else if (activity3_textwenzi2.getText().toString().length() == 11) {
			requestParams.add("TelephoneNumber", activity3_textwenzi2.getText()
					.toString());
			asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/dhinfo",
					requestParams, new Handler());
		} else {
			showToast("请输正确的手机号码");
		}

	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onRefresh() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(Activity3.this,
				"http://yisheng.wenbing.cn/Info/showdhinfo", requestParams,
				new Handler1());
	}

	private void showDialog() {
		LayoutInflater inflater = LayoutInflater.from(this);
		final View textEntryView = inflater.inflate(R.layout.dialog2, null);
		final EditText edtInput1 = (EditText) textEntryView
				.findViewById(R.id.editText1);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false);
		builder.setTitle("电话咨询服务价格自定义");
		builder.setView(textEntryView);
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (!edtInput1.getText().toString().equals("")) {
					phonePrice = edtInput1.getText().toString();
				} else {
					phonePrice = "0.00";
				}
				spinner.setSelection(0, false);
				adapter.setShowText(phonePrice + "元/次 ");
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		builder.show();
	}

	private class Handler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("TelephoneOpen").equals("1")) {
					activity3_toggle.setToggleOn();
					onclik = 1;
				} else {
					activity3_toggle.setToggleOff();
					onclik = 0;
				}
				phonePrice = jsonObject.getString("TelephonePrice");
				phoneTime = jsonObject.getString("TelephoneTime");
				activity3_textwenzi2.setText(jsonObject
						.getString("TelephoneNumber"));
				adapter.setShowText(phonePrice + "元/次 ");
				zy.setVisibility(View.VISIBLE);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			phoneTime = data.getStringExtra("time");
		}
	}

}
