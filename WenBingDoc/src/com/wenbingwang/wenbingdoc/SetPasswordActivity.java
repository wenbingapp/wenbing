package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.MyInfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SetPasswordActivity extends BaseActivity implements
		OnClickListener {

	private Button setpassword_back, yes_btn;
	private EditText old_password, new_password, check_password;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setpassword_activity);
		setpassword_back = (Button) findViewById(R.id.setpassword_back);
		yes_btn = (Button) findViewById(R.id.yes_btn);
		old_password = (EditText) findViewById(R.id.old_password);
		new_password = (EditText) findViewById(R.id.new_password);
		check_password = (EditText) findViewById(R.id.check_password);
		setpassword_back.setOnClickListener(this);
		yes_btn.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("修改密码");
		progressDialog.setMessage("正在提交请求");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.yes_btn:
			setPassword();
			break;
		case R.id.setpassword_back:
			SetPasswordActivity.this.finish();
			break;
		}
	}

	private void setPassword() {
		if (new_password.getText().toString()
				.equals(check_password.getText().toString())) {
			RequestParams requestParams = new RequestParams();
			requestParams.add("UserID", myInfo.getUserID());
			requestParams.add("OldPassword", old_password.getText().toString());
			requestParams.add("NewPassword", new_password.getText().toString());
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/ysxginfo", requestParams,
					new LoginHandler());
		} else {
			showToast("两次输入密码不一致");
		}

	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					finish();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

}
