package com.wenbingwang.wenbingdoc;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.ECDevice.OnECDeviceConnectListener;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.speedtong.sdk.ECError;
import com.speedtong.sdk.OnChatReceiveListener;
import com.wenbingwang.doc.application.DocInfo;
import com.wenbingwang.doc.application.ProblemInfo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class FragmentMain extends BaseFragment {
	private View rootView;
	private RadioGroup radioGroup;
	private FragmentMain_One fragmentMain_One;
	private FragmentMain_Two fragmentMain_Two;
	private ArrayList<Fragment> list;
	private int currentFragment = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater
					.inflate(R.layout.fragment_one, container, false);
			radioGroup = (RadioGroup) rootView.findViewById(R.id.radioGroup1);
			fragmentMain_One = new FragmentMain_One("0",
					"http://yisheng.wenbing.cn/Info/mflistinfo");
			fragmentMain_Two = new FragmentMain_Two("0",
					"http://yisheng.wenbing.cn/Info/mflistinfo");
			list = new ArrayList<Fragment>();
			list.add(fragmentMain_One);
			list.add(fragmentMain_Two);
			radioGroup
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(RadioGroup group,
								int checkedId) {
							// TODO Auto-generated method stub
							FragmentTransaction fragmentTransaction;
							switch (checkedId) {
							case R.id.radio0:
								changeFragment(0);
								break;
							case R.id.radio1:
								changeFragment(1);
								break;
							default:
								break;
							}
						}
					});
			changeFragment(0);
		}
		return rootView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	private void changeFragment(int id) {
		FragmentManager fragmentManager = getChildFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		if (!list.get(id).isAdded()) {
			fragmentTransaction.add(R.id.container, list.get(id), "");
			fragmentTransaction.commit();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		}
		if (list.get(currentFragment).isAdded()
				&& !list.get(currentFragment).isDetached()) {
			fragmentTransaction.detach(list.get(currentFragment));
		}
		if (list.get(id).isDetached()) {
			fragmentTransaction.attach(list.get(id));
		}
		fragmentTransaction.commit();
		currentFragment = id;
	}

}
