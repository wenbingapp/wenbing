package com.wenbingwang.wenbingdoc;

import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;

public class SetTimeActivity extends BaseActivity {
	int[] buttonID = new int[] { R.id.activity_timestart1,
			R.id.activity_timestop1, R.id.activity_timestart2,
			R.id.activity_timestop2, R.id.activity_timestart3,
			R.id.activity_timestop3, R.id.activity_timestart4,
			R.id.activity_timestop4, R.id.activity_timestart5,
			R.id.activity_timestop5, R.id.activity_timestart6,
			R.id.activity_timestop6, R.id.activity_timestart7,
			R.id.activity_timestop7 };
	private String[] s = new String[14];
	private Button activity_time_btn, time_back;
	private Button[] btnstop;
	private int i = 0;
	private String startString, stopString;
	private Button temp;
	private String time = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time);
		String ss = getIntent().getStringExtra("time");
		String sss[] = ss.split("\\|");
		for (int i = 0; i < sss.length; i++) {
			s[i * 2] = sss[i].split(",")[0];
			s[i * 2 + 1] = sss[i].split(",")[1];
		}
		btnstop = new Button[14];
		for (i = 0; i < buttonID.length; i++) {
			btnstop[i] = (Button) findViewById(buttonID[i]);
			if (i % 2 == 0) {
				btnstop[i].setText("始：" + s[i]);
			} else {
				btnstop[i].setText("终：" + s[i]);
			}
		}
		activity_time_btn = (Button) findViewById(R.id.activity_time_btn);
		time_back = (Button) findViewById(R.id.time_back);
		time_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SetTimeActivity.this.finish();
			}
		});
		activity_time_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				for (int t = 0; t < s.length; t++) {
					if (s[t] == null) {
						if (t % 2 == 0) {
							s[t] = "00:00" + ",";
						} else if (t == 13) {
							s[t] = "23:59";
						} else {
							s[t] = "23:59" + "|";
						}
					} else {
						if (t % 2 == 0) {
							s[t] = s[t] + ",";
						} else if (t == 13) {
							s[t] = s[t];
						} else {
							s[t] = s[t] + "|";
						}
					}
					time += s[t];
				}
				Intent intent = new Intent();
				intent.putExtra("time", time);
				SetTimeActivity.this.setResult(0, intent);
				SetTimeActivity.this.finish();
			}
		});
	}

	// 设置时间
	public void setTimeOnclick(final View v) {
		temp = (Button) v;
		TimePickerDialog time = new TimePickerDialog(SetTimeActivity.this,
				new OnTimeSetListener() {
					public void onTimeSet(TimePicker view, int hourOfDay,
							int minute) {
						startString = hourOfDay + "";
						stopString = minute + "";
						temp.setText("始：" + hourOfDay + ":" + minute);
						for (i = 0; i < 14; i++) {
							if (v.getId() == buttonID[i]) {
								break;
							}
						}
						s[i] = startString + ":" + stopString;
					}
				}, 18, 25, true);
		time.show();
	}

}
