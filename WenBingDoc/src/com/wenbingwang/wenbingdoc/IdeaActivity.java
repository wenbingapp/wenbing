package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.MyInfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class IdeaActivity extends BaseActivity implements OnClickListener {
	private Button idea_back, idea_btn;
	private ProgressDialog progressDialog;
	private EditText idea_Text1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.idea_atcitity);
		idea_back = (Button) findViewById(R.id.idea_back);
		idea_btn = (Button) findViewById(R.id.idea_btn);
		idea_Text1 = (EditText) findViewById(R.id.idea_Text1);
		idea_back.setOnClickListener(this);
		idea_btn.setOnClickListener(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("建议");
		progressDialog.setMessage("正在提交");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.idea_back:
			IdeaActivity.this.finish();
			break;

		case R.id.idea_btn:

			setIdea();
			break;
		}
	}

	private void setIdea() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("Suggestion", idea_Text1.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/ysopininfo",
				requestParams, new LoginHandler());

	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					finish();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

}
