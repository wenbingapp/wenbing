package com.wenbingwang.wenbingdoc;

import java.util.ArrayList;

import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.ECDevice.OnLogoutListener;
import com.speedtong.sdk.ECError;
import com.speedtong.sdk.OnChatReceiveListener;
import com.speedtong.sdk.ECDevice.OnECDeviceConnectListener;
import com.speedtong.sdk.core.ECMessageManager;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.wenbingwang.doc.application.ACache;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.ProblemItemInfo;
import com.wenbingwang.doc.application.RecvProblemItemInfo;
import com.wenbingwang.mywidget.MyRadioButton;
import com.wenbingwang.wenbingdoc.data.DataProvider;
import com.wenbingwang.wenbingdoc.data.DatabaseHelper;

import de.greenrobot.event.EventBus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MainActivity extends BaseActivity {
	private RadioGroup radioGroup;
	private ArrayList<Fragment> list;
	private FragmentMain fragmentOne;
	private FragmentTwo fragmentTwo;
	private FragmentThree fragmentThree;
	private int currentFragment = 1;
	private ImageButton middleButton;
	private MyRadioButton myRadioButton;
	private int soundId;
	private SoundPool soundPool;
	private boolean gh = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		soundPool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
		soundId = soundPool.load(this, R.raw.message, 1);
		initFragment();
		loginDevice(new OnECDeviceConnectListener() {

			@Override
			public void onDisconnect(ECError arg0) {
				// TODO Auto-generated method stub
				if (arg0.errorCode.equals("175004")) {
					ACache mCache = ACache.get(MainActivity.this);
					mCache.remove("logininfo");
					mCache.remove("Docinfo");
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, LoginActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("gh", "");
					startActivity(intent);
					finish();
				}
			}

			@Override
			public void onConnect() {
				// TODO Auto-generated method stub
			}
		}, new OnChatReceiveListener() {

			@Override
			public void OnReceivedReport(ECReport arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void OnReceivedMessage(ECMessage arg0) {
				// TODO Auto-generated method stub
				RecvProblemItemInfo recvProblemItemInfo = new RecvProblemItemInfo(
						arg0.getBody()
								.toString()
								.substring(19,
										arg0.getBody().toString().length() - 1));
				DataProvider.saveProblemItemInfo(
						DataProvider.getinstant(getApplicationContext()),
						recvProblemItemInfo);
				ACache mCache = ACache.get(MainActivity.this);
				if (mCache.getAsString("Vibrate") == null
						|| mCache.getAsString("Vibrate").equals("1")) {
					playVibrate();
				}
				if (mCache.getAsString("Sound") == null
						|| mCache.getAsString("Sound").equals("1")) {
					playSound();
				}

				if (((MyApplication) getApplication()).question == null
						|| !((MyApplication) getApplication()).question
								.equals(recvProblemItemInfo.getQuestionID())) {
					DataProvider.setProblemItemInfoUnread(
							DataProvider.getinstant(getApplicationContext()),
							recvProblemItemInfo.getQuestionID(), true);
					myRadioButton.setRedDot(true);
				}
				EventBus.getDefault().post(recvProblemItemInfo);
			}

			@Override
			public void OnReceiveGroupNoticeMessage(ECGroupNotice arg0) {
				// TODO Auto-generated method stub
			}
		});
		checkUpdate();
		EventBus.getDefault().register(this);
	}

	private void playSound() {
		soundPool.stop(soundId);
		soundPool.play(soundId, 1, 1, 0, 0, 1);
	}

	private void playVibrate() {
		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.cancel();
		long[] pattern = { 100, 250, 100, 250 };
		vibrator.vibrate(pattern, -1);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (gh) {
				finish();
			} else {
				gh = true;
				showToast("请再按一次返回退出应用");
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						gh = false;
					}
				}, 1000);
			}

		}
		return false;

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ECDevice.logout(new OnLogoutListener() {

			@Override
			public void onLogout() {
				// TODO Auto-generated method stub
				showToast("断开");
			}
		});
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		myRadioButton.setRedDot(DataProvider.checkProblemItemInfoHasUnread(
				DataProvider.getinstant(getApplicationContext()), "0"));
	}

	private void initFragment() {
		list = new ArrayList<Fragment>();
		fragmentOne = new FragmentMain();
		fragmentTwo = new FragmentTwo();
		fragmentThree = new FragmentThree();
		list.add(fragmentOne);
		list.add(fragmentTwo);
		list.add(fragmentThree);
		radioGroup = (RadioGroup) findViewById(R.id.radio);
		radioGroup.clearCheck();
		middleButton = (ImageButton) findViewById(R.id.radio1);
		myRadioButton = (MyRadioButton) findViewById(R.id.radio0);
		middleButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				radioGroup.clearCheck();
				if (currentFragment != 1) {
					changeFragment(1);
				}
			}
		});

		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int i = -1;
				switch (checkedId) {
				case R.id.radio0:
					if (((RadioButton) findViewById(checkedId)).isChecked()) {
						i = 0;
					}
					break;
				case R.id.radio2:
					if (((RadioButton) findViewById(checkedId)).isChecked()) {
						i = 2;
					}
					break;
				default:
					break;
				}
				if (i >= 0) {
					changeFragment(i);
				}
			}
		});
		changeFragment(currentFragment);
	}

	private void changeFragment(int id) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		if (!list.get(id).isAdded()) {
			fragmentTransaction.add(R.id.container, list.get(id), getTag(id));
			fragmentTransaction.commit();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		}
		if (list.get(currentFragment).isAdded()
				&& !list.get(currentFragment).isDetached()) {
			fragmentTransaction.detach(list.get(currentFragment));
		}
		if (list.get(id).isDetached()) {
			fragmentTransaction.attach(list.get(id));
		}
		fragmentTransaction.commit();
		currentFragment = id;
	}

	private String getTag(int id) {
		switch (id) {
		case 0:
			return "问答";
		case 1:
			return "main";
		case 2:
			return "账号";
		default:
			return "";
		}
	}

	/**
	 * 服务项目和其他的点击事件
	 * 
	 * @param v
	 */
	public void f1(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, Activity1.class);
		startActivity(intent);
	}

	public void f2(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, Activity2.class);
		startActivity(intent);
	}

	public void f3(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, Activity3.class);
		startActivity(intent);
	}

	public void f4(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, Activity4.class);
		startActivity(intent);
	}

	public void f5(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, Activity5.class);
		startActivity(intent);
	}

	public void f6(View v) {
		Intent intent = new Intent();
		intent.setClass(MainActivity.this, Activity6.class);
		startActivity(intent);
	}

	public void onEventMainThread(RecvProblemItemInfo event) {

	}
}
