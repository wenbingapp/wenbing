package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.ACache;
import com.wenbingwang.doc.application.DocInfo;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.MyInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends BaseActivity implements OnClickListener {
	private EditText editText1;
	private EditText editText2;
	private Button button;
	private TextView textView;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginactivity);
		editText1 = (EditText) findViewById(R.id.editText1);
		editText2 = (EditText) findViewById(R.id.editText2);
		button = (Button) findViewById(R.id.button1);
		textView = (TextView) findViewById(R.id.textView1);
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("��¼");
		progressDialog.setMessage("���ڵ�¼");
		button.setOnClickListener(this);
		textView.setOnClickListener(this);
		if (getIntent().getStringExtra("gh") != null) {
			new AlertDialog.Builder(this).setTitle("����")
					.setMessage("���˺��������豸�ϵ�¼").setPositiveButton("ȷ��", null)
					.show();
		}
		checkUpdate();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			Login();
			break;
		case R.id.textView1:
			Intent intent = new Intent(getApplicationContext(),
					GetPassWord1.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	private void Login() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("Telephone", editText1.getText().toString());
		requestParams.add("Password", editText2.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/ysdlinfo",
				requestParams, new LoginHandler());// http://other805.3322.org:65432/
		// asyncHttpClient.post(this,
		// "http://other805.3322.org:65432/Info/ysdlinfo",
		// requestParams, new LoginHandler());
	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					ACache mCache = ACache.get(LoginActivity.this);
					mCache.put("logininfo", jsonObject.toString());
					setMyInfo(new MyInfo(jsonObject.toString()));
					RequestParams requestParams = new RequestParams();
					requestParams.add("UserID",
							((MyApplication) getApplication()).myInfo
									.getUserID());
					asyncHttpClient.post(LoginActivity.this,
							"http://yisheng.wenbing.cn/Info/zyylinfo",
							requestParams, new MyHandler1());
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				DocInfo docInfo = new DocInfo(jsonObject.toString());
				((MyApplication) getApplication()).myDocInfo = docInfo;
				RequestParams requestParams = new RequestParams();
				requestParams.add("UserID",
						((MyApplication) getApplication()).myInfo.getUserID());
				asyncHttpClient.post(LoginActivity.this,
						"http://yisheng.wenbing.cn/Info/xxzlinfo",
						requestParams, new MyHandler2());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class MyHandler2 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				((MyApplication) getApplication()).myDocInfo
						.setFanCount(jsonObject.getString("FanCount"));
				((MyApplication) getApplication()).myDocInfo
						.setFormCount(jsonObject.getString("FormCount"));
				ACache mCache = ACache.get(LoginActivity.this);
				mCache.put("Docinfo",
						((MyApplication) getApplication()).myDocInfo.toString());
				Intent intent = new Intent(getApplicationContext(),
						MainActivity.class);
				startActivity(intent);
				finish();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}
}
