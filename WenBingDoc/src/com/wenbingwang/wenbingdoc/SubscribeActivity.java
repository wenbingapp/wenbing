package com.wenbingwang.wenbingdoc;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.core.im.IMNativeObject;
import com.wenbingwang.doc.application.ACache;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.MyInfo;
import com.wenbingwang.mywidget.MyImageView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class SubscribeActivity extends BaseActivity {
	private ImageView imageView1;
	private TextView textView1;
	private TextView textView2;
	private TextView textView3;
	private TextView textView4;
	private TextView textView5;
	private TextView textView6;
	private Button button1;
	private Button button2;
	private String stuts;
	private Date picDate;
	private ProgressDialog progressDialog;
	private String OutpatientID;
	private Date date1;
	private Date date2;
	private SimpleDateFormat simpleDateFormat1;
	private SimpleDateFormat simpleDateFormat2;
	private String states;
	private LinearLayout pic;
	private String[] s;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.subscribe_activity);
		simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		simpleDateFormat2 = new SimpleDateFormat("MM月dd日 HH:mm");
		try {
			date1 = simpleDateFormat1.parse(getIntent().getStringExtra("3-1"));
			date2 = simpleDateFormat1.parse(getIntent().getStringExtra("3-2"));
			picDate = simpleDateFormat1.parse(getIntent().getStringExtra("6"));
			pic = (LinearLayout) findViewById(R.id.pic);
			imageView1 = (ImageView) findViewById(R.id.imageView1);
			progressDialog = new ProgressDialog(this);
			progressDialog.setTitle("提交");
			progressDialog.setMessage("正在提交请求");
			OutpatientID = getIntent().getStringExtra("8");
			findViewById(R.id.about_back).setOnClickListener(
					new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							finish();
						}
					});
			textView1 = (TextView) findViewById(R.id.textView1);
			textView2 = (TextView) findViewById(R.id.textView2);
			textView3 = (TextView) findViewById(R.id.textView3);
			textView4 = (TextView) findViewById(R.id.textView4);
			textView5 = (TextView) findViewById(R.id.textView6);
			textView6 = (TextView) findViewById(R.id.textView7);
			button1 = (Button) findViewById(R.id.button1);
			button2 = (Button) findViewById(R.id.button2);
			imageLoader.displayImage(getIntent().getStringExtra("0"),
					imageView1, options);
			s = getIntent().getStringExtra("-1").split(",");
			for (String string : s) {
				final String ss = string;
				MyImageView imageView = new MyImageView(this);
				imageView.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(SubscribeActivity.this,
								PicActivity.class);
						intent.putExtra("pic", ss);
						startActivity(intent);
					}
				});
				pic.addView(imageView, new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				imageLoader.displayImage(string, imageView, options);
			}
			textView1.setText(getIntent().getStringExtra("1"));
			textView2.setText(getIntent().getStringExtra("2"));
			textView3.setText("预约时间：" + simpleDateFormat2.format(date1) + " 至 "
					+ simpleDateFormat2.format(date2));
			textView4.setText(getIntent().getStringExtra("4"));
			textView5.setText(getIntent().getStringExtra("5"));
			textView6.setText("预约时间：" + simpleDateFormat2.format(picDate));
			stuts = getIntent().getStringExtra("7");
			if (stuts.equals("3") || stuts.equals("2")) {
				button2.setVisibility(View.GONE);
				button1.setText("该门诊预约单已确诊");
			}
			if (stuts.equals("1")) {
				button1.setText("拒绝预约");
				button2.setText("确认预约");
				textView6.setTextColor(0xff0099ff);
				textView6.setText("设置预约时间");
				button1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						RequestParams requestParams = new RequestParams();
						requestParams.add("OutpatientID", OutpatientID);
						requestParams.add("Type", "0");
						requestParams.add("OutpatientConfirmTime",
								"2015-01-01 00:00:00");
						states = "-1";
						asyncHttpClient.post(SubscribeActivity.this,
								"http://yisheng.wenbing.cn/Info/updatemzinfo",
								requestParams, new Handler());
					}
				});
				button2.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (picDate != null) {
							if (picDate.getTime() <= date2.getTime()
									&& picDate.getTime() >= date1.getTime()) {
								RequestParams requestParams = new RequestParams();
								requestParams.add("OutpatientID", OutpatientID);
								requestParams.add("Type", "1");
								requestParams.add("OutpatientConfirmTime",
										simpleDateFormat1.format(picDate));
								states = "2";
								asyncHttpClient
										.post(SubscribeActivity.this,
												"http://yisheng.wenbing.cn/Info/updatemzinfo",
												requestParams, new Handler());
							} else {
								showToast("确诊时间必须在预约时间之内");
							}
						} else {
							showToast("请输入确诊时间");
						}
					}
				});
				textView6.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Date date = new Date();
						picDate = new Date();
						TimePickerDialog time = new TimePickerDialog(
								SubscribeActivity.this,
								new OnTimeSetListener() {
									boolean mFired = false;

									@Override
									public void onTimeSet(TimePicker view,
											int hourOfDay, int minute) {
										if (mFired) {
											picDate.setHours(hourOfDay);
											picDate.setMinutes(minute);
											Date date = new Date();
											DatePickerDialog daypicker = new DatePickerDialog(
													SubscribeActivity.this,
													new OnDateSetListener() {
														@Override
														public void onDateSet(
																DatePicker view,
																int year,
																int monthOfYear,
																int dayOfMonth) {
															// TODO
															// Auto-generated
															// method stub
															picDate.setYear(year - 1900);
															picDate.setMonth(monthOfYear);
															picDate.setDate(dayOfMonth);
															SimpleDateFormat sdf1 = new SimpleDateFormat(
																	"就诊时间：yyyy-MM-dd hh:mm");
															textView6
																	.setText(sdf1
																			.format(picDate));
														}
													}, date.getYear() + 1900,
													date.getMonth(), date
															.getDate());
											daypicker.setTitle("请选择确诊日期：");
											daypicker.show();
										} else {
											mFired = true;
										}
									}
								}, date.getHours(), date.getMinutes(), true);
						time.setTitle("请选择确诊时间：");
						time.show();
					}
				});
			}
			if (stuts.equals("-1")) {
				button2.setVisibility(View.GONE);
				textView6.setVisibility(View.GONE);
				button1.setText("您已拒绝该门诊预约单");
			}
		} catch (Exception e) {
			showToast("服务器数据有误，请联系维护人员");
			finish();
		}
	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			if (!progressDialog.isShowing()) {
				progressDialog.show();
			}
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					Intent intent = new Intent();
					intent.putExtra("id", OutpatientID);
					intent.putExtra("states", states);
					intent.putExtra("time", simpleDateFormat1.format(picDate));
					setResult(0, intent);
					finish();
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

	}

}
