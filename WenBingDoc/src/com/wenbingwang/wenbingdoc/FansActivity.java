package com.wenbingwang.wenbingdoc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.wenbingwang.doc.application.FansInfo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class FansActivity extends BaseActivity implements OnRefreshListener,
		OnItemClickListener {
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private TextView footer;
	private Button fans_back;
	private boolean hasmore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fans_activity);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.listView1);
		fans_back = (Button) findViewById(R.id.fans_back);
		listView.setOnItemClickListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
		fans_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FansActivity.this.finish();
			}
		});
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("Page", page + "");
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/fslistinfo",
				requestParams, new MyHandler(s));

	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");

		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					FansInfo problemInfo = new FansInfo(response.getJSONObject(
							i).toString());
					adapter.getList().add(problemInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);

		}

	}

	private class Adapter extends BaseAdapter {
		private ArrayList<FansInfo> list;

		public Adapter() {
			list = new ArrayList<FansInfo>();
		}

		public ArrayList<FansInfo> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(FansActivity.this).inflate(
						R.layout.fans_activity_item, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.fans_activity_name);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.fans_item_age);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.fans_item_time);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.fans_item_address);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.fans_item_image);
				viewHolder.huiyuan = (ImageView) convertView
						.findViewById(R.id.huiyuan);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.textView1.setText(list.get(position).getNickname());
			// sex:1 男 2 女
			if ((list.get(position).getSex()).equals("1")) {
				viewHolder.textView2.setText("年龄:"
						+ list.get(position).getAge() + " 性别:男");
			} else if ((list.get(position).getSex()).equals("2")) {
				viewHolder.textView2.setText("年龄:"
						+ list.get(position).getAge() + " 性别:女");
			} else {
				viewHolder.textView2.setText("年龄:"
						+ list.get(position).getAge());
			}
			viewHolder.textView3.setText(time(list.get(position).getTime()));
			viewHolder.textView4.setText(list.get(position).getAddress());
			imageLoader.displayImage(list.get(position).getHeadImage(),
					viewHolder.imageView, options);
			viewHolder.huiyuan.setVisibility(list.get(position).getVIP()
					.equals("1") ? View.VISIBLE : View.INVISIBLE);
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
		public TextView textView4;
		private ImageView imageView;
		private ImageView huiyuan;
	}

	private String time(String nowTime) {// 2015-06-25 13:57:17
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str1 = formatter2.format(curDate);
		String dayStr = formatter1.format(curDate);
		// 当前时间
		String now = nowTime.substring(0, 10);// 输入的年月日
		String now1 = nowTime.substring(0, 7);// 输入的年月
		String nowyestoday = nowTime.substring(8, 10);// 输入的日
		String today = str1.substring(0, 10);// 当前的年月日
		String today2 = str1.substring(0, 7);// 当前的年月
		String today1 = str1.substring(8, 10);
		System.out.println(Integer.valueOf(today1).intValue());
		int yestodays = (Integer.parseInt(today1)) - 1;
		String yes = "" + yestodays;
		if (now.equals(today)) {
			return "今天 " + dayStr;
		} else if (nowyestoday.equals(yes) && now1.equals(today2)) {
			return "昨天 " + dayStr;
		} else {
			return nowTime.substring(5, 16);
		}
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		}
	}
}
