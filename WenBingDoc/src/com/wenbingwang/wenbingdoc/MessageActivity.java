package com.wenbingwang.wenbingdoc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.ECChatManager;
import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECMessage.Direction;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.ECTextMessageBody;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.ProblemInfo;
import com.wenbingwang.doc.application.ProblemItemInfo;
import com.wenbingwang.doc.application.RecvProblemItemInfo;
import com.wenbingwang.doc.application.SendProblemItemInfo;
import com.wenbingwang.wenbingdoc.data.DataProvider;
import com.wenbingwang.wenbingdoc.data.DatabaseHelper;

import de.greenrobot.event.EventBus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MessageActivity extends BaseActivity implements OnClickListener,
		OnRefreshListener {
	private ProblemInfo problemInfo;
	private ListView listView;
	private SwipeRefreshLayout swipeRefreshLayout;
	private Button button1;
	private EditText editText;
	private Button button2;
	private Button button3;
	private Adapter adapter;
	private String head;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_activity);
		problemInfo = (ProblemInfo) getIntent().getSerializableExtra(
				"probleminfo");
		head = problemInfo.getQuestionID().split("_")[0];
		listView = (ListView) findViewById(R.id.listView1);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		button1 = (Button) findViewById(R.id.button1);
		editText = (EditText) findViewById(R.id.editText1);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);
		swipeRefreshLayout.setOnRefreshListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		EventBus.getDefault().register(this);
		((MyApplication) getApplication()).question = problemInfo
				.getQuestionID();
		DataProvider.setProblemItemInfoUnread(
				DataProvider.getinstant(getApplicationContext()),
				problemInfo.getQuestionID(), false);
		ArrayList<ProblemItemInfo> problemItemInfos = DataProvider
				.getProblemItemInfo(
						DataProvider.getinstant(getApplicationContext()),
						problemInfo.getQuestionID());
		if (problemItemInfos.size() == 0) {
			onRefresh();
		} else {
			adapter.setList(problemItemInfos);
			listView.setSelection(adapter.list.size() - 1);
			adapter.notifyDataSetChanged();
		}
	}

	private void load() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("QuestionID",
				problemInfo.getQuestionID().split("_")[1]);
		if (head.equals("0")) {
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/mftwxqinfo", requestParams,
					new MyHandler());
		} else {
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/fftwxqinfo", requestParams,
					new MyHandler());
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
		((MyApplication) getApplication()).question = null;
	}

	private class MyHandler extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			adapter.getList().clear();
			adapter.notifyDataSetChanged();
			ProblemItemInfo problemItemInfo = new ProblemItemInfo();
			problemItemInfo.setQuestionID(problemInfo.getQuestionID());
			problemItemInfo.setSenderID(problemInfo.getSenderID());
			problemItemInfo.setTextContent("年龄：" + problemInfo.getAge()
					+ " 岁 性别："
					+ (problemInfo.getSex().equals("1") ? "男\n" : "女\n")
					+ "标题：" + problemInfo.getQuestionTitle() + "\n详情："
					+ problemInfo.getQuestionDetail() + "\n时间："
					+ problemInfo.getSubmitTime());
			adapter.getList().add(0, problemItemInfo);
			adapter.notifyDataSetChanged();
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				adapter.getList().clear();
				adapter.notifyDataSetChanged();
				for (int i = 0; i < response.length(); i++) {
					ProblemItemInfo problemInfo = new ProblemItemInfo(response
							.getJSONObject(i).toString(), head);
					adapter.getList().add(problemInfo);
					adapter.notifyDataSetChanged();
				}
				DataProvider.saveProblemItemInfo(
						DataProvider.getinstant(getApplicationContext()),
						adapter.getList());
				ProblemItemInfo problemItemInfo = new ProblemItemInfo();
				problemItemInfo.setQuestionID(problemInfo.getQuestionID());
				problemItemInfo.setSenderID(problemInfo.getSenderID());
				problemItemInfo.setTextContent("年龄：" + problemInfo.getAge()
						+ " 岁 性别："
						+ (problemInfo.getSex().equals("1") ? "男\n" : "女\n")
						+ "标题：" + problemInfo.getQuestionTitle() + "\n详情："
						+ problemInfo.getQuestionDetail() + "\n时间："
						+ problemInfo.getSubmitTime());
				adapter.getList().add(0, problemItemInfo);
				listView.setSelection(adapter.list.size() - 1);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private class MyHandler1 extends JsonHttpResponseHandler {

		@Override
		public void onStart() {
			// Initiated the request
			button3.setClickable(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			button3.setClickable(true);
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject response) {
			try {
				if (response.getString("SubmitResult").equals("1")) {
					button3.setClickable(true);
					handleSendTextMessage(editText.getText().toString());
				} else {
					showToast(response.getString("SubmitDescription"));
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private void handleSendTextMessage(CharSequence text) {
		if (text == null) {
			return;
		}
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("SenderNickname",
					((MyApplication) getApplication()).myDocInfo
							.getDoctorNickName());
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					"yyyy-MM-dd hh:mm");
			jsonObject.put("SendTime", simpleDateFormat.format(new Date()));
			jsonObject.put("QuestionID", problemInfo.getQuestionID());
			jsonObject.put("SenderID",
					((MyApplication) getApplication()).myInfo.getUserID());
			jsonObject.put("SenderVoIP",
					((MyApplication) getApplication()).myInfo.getVoIPAccount());
			jsonObject.put("SenderHeadImage",
					((MyApplication) getApplication()).myDocInfo
							.getDoctorHeadImageUrl());
			jsonObject.put("TextContent", text);
			ECMessage msg = ECMessage.createECMessage(ECMessage.Type.TXT);
			msg.setMsgTime(System.currentTimeMillis());
			msg.setForm(((MyApplication) getApplication()).myInfo
					.getVoIPAccount());
			msg.setSessionId(((MyApplication) getApplication()).myInfo
					.getVoIPAccount());
			msg.setTo(problemInfo.getSenderVoIP());
			msg.setDirection(Direction.SEND);
			ECTextMessageBody msgBody = new ECTextMessageBody(
					jsonObject.toString());
			msg.setBody(msgBody);
			ECChatManager chatManager = ECDevice.getECChatManager();
			chatManager.sendMessage(msg, null);
			editText.setText("");
			RecvProblemItemInfo recvproblemItemInfo = new RecvProblemItemInfo();
			recvproblemItemInfo.setQuestionID(problemInfo.getQuestionID());
			recvproblemItemInfo
					.setSenderHeadImage(((MyApplication) getApplication()).myDocInfo
							.getDoctorHeadImageUrl());
			recvproblemItemInfo
					.setSenderID(((MyApplication) getApplication()).myInfo
							.getUserID());
			recvproblemItemInfo
					.setSenderNickname(((MyApplication) getApplication()).myDocInfo
							.getDoctorNickName());
			recvproblemItemInfo
					.setSenderVoIP(((MyApplication) getApplication()).myInfo
							.getVoIPAccount());
			recvproblemItemInfo
					.setSendTime(simpleDateFormat.format(new Date()));
			recvproblemItemInfo.setTextContent(text.toString());
			DataProvider.saveProblemItemInfo(
					DataProvider.getinstant(getApplicationContext()),
					recvproblemItemInfo);
			adapter.list.add(recvproblemItemInfo.toProblemItemInfo());
			adapter.notifyDataSetChanged();
			SendProblemItemInfo sendProblemItemInfo = new SendProblemItemInfo();
			sendProblemItemInfo.setQuestionID(problemInfo.getQuestionID());
			EventBus.getDefault().post(sendProblemItemInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			finish();
			break;
		case R.id.button2:
			if (problemInfo.getQuestionImage().equals(
					"http://yisheng.wenbing.cn")) {
				showToast("该患者在提问时没有上传病情图");
			} else {
				Intent intent = new Intent(MessageActivity.this,
						PicActivity.class);
				intent.putExtra("pic", problemInfo.getQuestionImage());
				startActivity(intent);
			}
			break;
		case R.id.button3:
			if (!editText.getText().toString().equals("")) {
				RequestParams requestParams = new RequestParams();
				requestParams.add("QuestionID", problemInfo.getQuestionID()
						.split("_")[1]);
				requestParams.add("SenderID",
						((MyApplication) getApplication()).myInfo.getUserID());
				requestParams.add("SenderVoIP",
						((MyApplication) getApplication()).myInfo
								.getVoIPAccount());
				requestParams.add("QuestionDetail", editText.getText()
						.toString());
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						"yyyy-MM-dd hh:mm:ss");
				requestParams.add("SendTime",
						simpleDateFormat.format(new Date()));
				if (head.equals("0")) {
					asyncHttpClient.post(this,
							"http://yisheng.wenbing.cn/Info/mftwhfinfo",
							requestParams, new MyHandler1());
				} else {
					asyncHttpClient.post(this,
							"http://yisheng.wenbing.cn/Info/fftwhfinfo",
							requestParams, new MyHandler1());
				}
			} else {
				showToast("不能发送空消息");
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		load();
	}

	private class Adapter extends BaseAdapter {
		private ArrayList<ProblemItemInfo> list;

		public Adapter() {
			list = new ArrayList<ProblemItemInfo>();
		}

		public ArrayList<ProblemItemInfo> getList() {
			return list;
		}

		public void setList(ArrayList<ProblemItemInfo> l) {
			list = l;
			ProblemItemInfo problemItemInfo = new ProblemItemInfo();
			problemItemInfo.setQuestionID(problemInfo.getQuestionID());
			problemItemInfo.setSenderID(problemInfo.getSenderID());
			problemItemInfo.setTextContent("年龄：" + problemInfo.getAge()
					+ " 岁 性别："
					+ (problemInfo.getSex().equals("1") ? "男\n" : "女\n")
					+ "标题：" + problemInfo.getQuestionTitle() + "\n详情："
					+ problemInfo.getQuestionDetail() + "\n时间："
					+ problemInfo.getSubmitTime());
			list.add(0, problemItemInfo);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView textView;
			ImageView imageView;
			if (list.get(position).getSenderID()
					.equals(problemInfo.getSenderID())) {
				convertView = LayoutInflater.from(MessageActivity.this)
						.inflate(R.layout.message_activity_item, null);
				textView = (TextView) convertView.findViewById(R.id.textView1);
				imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				imageLoader.displayImage(problemInfo.getSenderHeadImage(),
						imageView, options);
			} else {
				convertView = LayoutInflater.from(MessageActivity.this)
						.inflate(R.layout.message_item_right, null);
				textView = (TextView) convertView.findViewById(R.id.textView1);
				imageView = (ImageView) convertView
						.findViewById(R.id.imageView1);
				imageLoader.displayImage(
						((MyApplication) getApplication()).myDocInfo
								.getDoctorHeadImageUrl(), imageView, options);
			}
			textView.setText(list.get(position).getTextContent());
			return convertView;
		}
	}

	public void onEventMainThread(RecvProblemItemInfo event) {
		ProblemItemInfo problemItemInfo = new ProblemItemInfo();
		problemItemInfo.setQuestionID(event.getQuestionID());
		problemItemInfo.setSenderID(event.getSenderID());
		problemItemInfo.setSenderVoIP(event.getSenderVoIP());
		problemItemInfo.setSendTime(event.getSendTime());
		problemItemInfo.setTextContent(event.getTextContent());
		adapter.getList().add(problemItemInfo);
		adapter.notifyDataSetChanged();
	}
}
