package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.MyInfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class GetPassWord1 extends BaseActivity implements OnClickListener {
	private Button idea_back, idea_btn;
	private EditText idea_Text1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getpassword1_activity);
		idea_back = (Button) findViewById(R.id.idea_back);
		idea_btn = (Button) findViewById(R.id.button1);
		idea_Text1 = (EditText) findViewById(R.id.idea_Text1);
		idea_back.setOnClickListener(this);
		idea_btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.idea_back:
			GetPassWord1.this.finish();
			break;
		case R.id.button1:
			setIdea();
			break;
		}
	}

	private void setIdea() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("Telephone", idea_Text1.getText().toString());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/ysfindinfo",
				requestParams, new LoginHandler());
	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
					Intent intent = new Intent(GetPassWord1.this,
							GetPassWord2.class);
					intent.putExtra("phone", idea_Text1.getText().toString());
					startActivity(intent);
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
