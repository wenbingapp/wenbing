package com.wenbingwang.wenbingdoc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class IndentActivity extends Activity implements OnClickListener {

	private Button indent_back;
	private RelativeLayout activity_item_relative, activity_item_relative2,
			activity_item_relative3, activity_item_relative4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.indent);
		indent_back = (Button) findViewById(R.id.indent_back);
		activity_item_relative = (RelativeLayout) findViewById(R.id.activity_item_relative);
		activity_item_relative2 = (RelativeLayout) findViewById(R.id.activity_item_relative2);
		activity_item_relative3 = (RelativeLayout) findViewById(R.id.activity_item_relative3);
		activity_item_relative4 = (RelativeLayout) findViewById(R.id.activity_item_relative4);
		activity_item_relative.setOnClickListener(this);
		activity_item_relative2.setOnClickListener(this);
		activity_item_relative3.setOnClickListener(this);
		activity_item_relative4.setOnClickListener(this);
		indent_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.indent_back:
			IndentActivity.this.finish();
			break;

		case R.id.activity_item_relative:
			intent.setClass(IndentActivity.this, IndentListActivity.class);
			startActivity(intent);
			break;
		case R.id.activity_item_relative2:
			intent.setClass(IndentActivity.this, IndentListActivity2.class);
			startActivity(intent);
			break;
		case R.id.activity_item_relative3:
			intent.setClass(IndentActivity.this, IndentListActivity3.class);
			startActivity(intent);
			break;
		case R.id.activity_item_relative4:
			intent.setClass(IndentActivity.this, IndentListActivity4.class);
			startActivity(intent);
			break;
		}
	}

}
