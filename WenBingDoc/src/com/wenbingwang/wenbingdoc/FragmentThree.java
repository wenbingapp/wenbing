package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.ACache;
import com.wenbingwang.doc.application.MyInfo;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

public class FragmentThree extends BaseFragment implements OnClickListener {
	private View rootView;
	private RelativeLayout re1, re2, re3;
	SharedPreferences.Editor editor = null;
	private Button button1;
	private ToggleButton toggbutton;
	private ToggleButton gh1;
	private ToggleButton gh2;
	private int onclik = 0;
	private boolean f = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_three, container,
					false);
			re1 = (RelativeLayout) rootView
					.findViewById(R.id.fragment_three_re1);
			re2 = (RelativeLayout) rootView
					.findViewById(R.id.fragment_three_re2);
			re3 = (RelativeLayout) rootView
					.findViewById(R.id.fragment_three_re3);
			button1 = (Button) rootView.findViewById(R.id.button1);
			toggbutton = (ToggleButton) rootView.findViewById(R.id.toggbutton);
			gh1 = (ToggleButton) rootView.findViewById(R.id.gh1);
			gh2 = (ToggleButton) rootView.findViewById(R.id.gh2);
			button1.setOnClickListener(this);
			re1.setOnClickListener(this);
			re2.setOnClickListener(this);
			re3.setOnClickListener(this);
			ACache mCache = ACache.get(getActivity());
			if (mCache.getAsString("Vibrate") == null
					|| mCache.getAsString("Vibrate").equals("1")) {
				gh2.setToggleOn();
			}
			if (mCache.getAsString("Sound") == null
					|| mCache.getAsString("Sound").equals("1")) {
				gh1.setToggleOn();
			}

			// �����л��¼�
			toggbutton.setOnToggleChanged(new OnToggleChanged() {
				@Override
				public void onToggle(boolean on) {
					if (f) {
						if (on == true) {
							onclik = 1;
							getPhoto();
						} else {
							onclik = 0;
							getPhoto();
						}
					}
				}
			});
			gh1.setOnToggleChanged(new OnToggleChanged() {
				@Override
				public void onToggle(boolean on) {
					setSound(on);
				}
			});
			gh2.setOnToggleChanged(new OnToggleChanged() {
				@Override
				public void onToggle(boolean on) {
					setVibrate(on);
				}
			});
			getPhotos();
		}
		return rootView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	public void setVibrate(boolean f) {
		ACache mCache = ACache.get(getActivity());
		if (f) {
			mCache.put("Vibrate", "1");
		} else {
			mCache.put("Vibrate", "0");
		}
	}

	public void setSound(boolean f) {
		ACache mCache = ACache.get(getActivity());
		if (f) {
			mCache.put("Sound", "1");
		} else {
			mCache.put("Sound", "0");
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.fragment_three_re1:// �޸��ܴa
			intent.setClass(getActivity(), SetPasswordActivity.class);
			startActivity(intent);
			break;

		case R.id.fragment_three_re2:// �������
			intent.setClass(getActivity(), IdeaActivity.class);
			startActivity(intent);
			break;
		case R.id.fragment_three_re3:// �����ʲ�
			intent.setClass(getActivity(), AboutActivity.class);
			startActivity(intent);
			break;
		case R.id.button1:
			ACache mCache = ACache.get(getActivity());
			mCache.remove("logininfo");
			mCache.remove("Docinfo");
			intent.setClass(getActivity(), LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			getActivity().finish();
			break;
		}
	}

	private void getPhotos() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/getorderinfo", requestParams,
				new Handlers());
	}

	private void getPhoto() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("FormMessageWarningOpen", onclik + "");
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/isorderinfo", requestParams,
				new Handler());
	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private class Handlers extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
			f = true;
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {

				if (jsonObject.getString("FormMessageWarningOpen").equals("1")) {
					toggbutton.toggleOn();
				} else {
					toggbutton.toggleOff();
				}
				f = true;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}