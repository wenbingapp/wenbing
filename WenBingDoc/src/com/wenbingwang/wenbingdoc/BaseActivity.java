package com.wenbingwang.wenbingdoc;

import java.text.DecimalFormat;

import com.loopj.android.http.AsyncHttpClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.ECDevice.OnECDeviceConnectListener;
import com.speedtong.sdk.ECError;
import com.speedtong.sdk.ECInitialize;
import com.speedtong.sdk.OnChatReceiveListener;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.MyInfo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

@SuppressLint("NewApi")
public class BaseActivity extends FragmentActivity {
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader;
	protected AsyncHttpClient asyncHttpClient;
	protected Toast toast;
	protected MyInfo myInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		asyncHttpClient = ((MyApplication) getApplication())
				.getAsyncHttpClient();
		myInfo = ((MyApplication) getApplication()).myInfo;
		imageLoader = ImageLoader.getInstance();
		ColorDrawable colorDrawable = new ColorDrawable(0x969696);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(colorDrawable)
				.showImageOnFail(colorDrawable).cacheInMemory(true)
				.cacheOnDisk(true).displayer(new BitmapDisplayer() {
					@Override
					public void display(Bitmap bitmap, ImageAware imageAware,
							LoadedFrom loadedFrom) {
						// TODO Auto-generated method stub
						imageAware.setImageBitmap(bitmap);
					}
				}).build();
	}

	protected void showToast(String s) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
		toast.show();
	}

	protected String doble(String a) {
		DecimalFormat df = new DecimalFormat("0");
		double price = Double.parseDouble(a);
		return df.format(price);
	}

	protected String dobles(String a) {
		DecimalFormat df = new DecimalFormat("0.00");
		double price = Double.parseDouble(a);
		return df.format(price);
	}

	public void setMyInfo(MyInfo m) {
		((MyApplication) getApplication()).myInfo = m;
		ECInitialize params = new ECInitialize();
		params.setServerIP("https://app.cloopen.com");
		params.setServerPort(8883);
		params.setSid(m.getVoIPAccount());
		params.setSidToken(m.getVoIPPassword());
		params.setSubId(m.getSubAccount());
		params.setSubToken(m.getSubPassword());
		((MyApplication) getApplication()).params = params;
	}

	protected void loginDevice(OnECDeviceConnectListener b,
			OnChatReceiveListener c) {
		if (((MyApplication) getApplication()).params != null) {
			((MyApplication) getApplication()).params
					.setOnChatReceiveListener(c);
			((MyApplication) getApplication()).params
					.setOnECDeviceConnectListener(b);
			ECDevice.login(((MyApplication) getApplication()).params);
		}
	}

	protected void checkUpdate() {
		if (((MyApplication) getApplication()).checkupdate.equals("1")) {
			if (((MyApplication) getApplication()).forceupdate.equals("1")) {
				new AlertDialog.Builder(this)
						.setTitle("更新提示")
						.setMessage("有新的版本发布,是否更新?")
						.setPositiveButton(
								"现在更新",
								new update(
										((MyApplication) getApplication()).url))
						.setNegativeButton("退出程序",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										finish();
									}
								}).show();
			} else {
				new AlertDialog.Builder(this)
						.setTitle("更新提示")
						.setMessage("有新的版本发布,是否更新?")
						.setPositiveButton(
								"现在更新",
								new update(
										((MyApplication) getApplication()).url))
						.setNegativeButton("稍后更新", null).show();
			}
		}
	}

	class update implements DialogInterface.OnClickListener {
		private String ss;

		public update(String s) {
			ss = s;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			DownloadManager dManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
			Uri uri = Uri.parse(ss);
			DownloadManager.Request request = new Request(uri);
			request.setDestinationInExternalPublicDir("download", "叮当医生.apk");
			request.setDescription("叮当医生");
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
			request.setMimeType("application/vnd.android.package-archive");
			request.allowScanningByMediaScanner();
			request.setVisibleInDownloadsUi(true);
			long refernece = dManager.enqueue(request);
			SharedPreferences sPreferences = getSharedPreferences(
					"downloadcomplete", 0);
			sPreferences.edit().putLong("refernece", refernece).commit();
			finish();
		}

	}
}
