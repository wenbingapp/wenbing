package com.wenbingwang.wenbingdoc.data;

import java.util.ArrayList;

import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.MyInfo;
import com.wenbingwang.doc.application.ProblemItemInfo;
import com.wenbingwang.doc.application.RecvProblemItemInfo;
import com.wenbingwang.wenbingdoc.SplashActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DataProvider {
	private static DatabaseHelper databaseHelper;

	public static DatabaseHelper getinstant(Context c) {
		if (databaseHelper == null) {
			String s = SplashActivity.CheckHasLogin(c);
			if (s != null) {
				MyInfo myInfo = new MyInfo(s);
				databaseHelper = new DatabaseHelper(c, myInfo.getUserID());
			}
		}
		return databaseHelper;
	}

	public static void saveProblemItemInfo(DatabaseHelper d,
			RecvProblemItemInfo p) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getWritableDatabase();
			ContentValues contentValues = new ContentValues();
			contentValues.put("QuestionID", p.getQuestionID());
			contentValues.put("SenderID", p.getSenderID());
			contentValues.put("SenderVoIP", p.getSenderVoIP());
			contentValues.put("TextContent", p.getTextContent());
			contentValues.put("SendTime", p.getSendTime());
			db.insert("ProblemItemInfo", null, contentValues);
			db.close();
		}
	}

	public static void saveProblemItemInfo(DatabaseHelper d,
			ArrayList<ProblemItemInfo> p) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getWritableDatabase();
			if (p.size() > 0) {
				db.delete("ProblemItemInfo", "QuestionID=?", new String[] { p
						.get(0).getQuestionID() });
				for (ProblemItemInfo problemItemInfo : p) {
					ContentValues contentValues = new ContentValues();
					contentValues.put("QuestionID",
							problemItemInfo.getQuestionID());
					contentValues
							.put("SenderID", problemItemInfo.getSenderID());
					contentValues.put("SenderVoIP",
							problemItemInfo.getSenderVoIP());
					contentValues.put("TextContent",
							problemItemInfo.getTextContent());
					contentValues
							.put("SendTime", problemItemInfo.getSendTime());
					db.insert("ProblemItemInfo", null, contentValues);
				}
			}

			db.close();
		}
	}

	public static boolean checkProblemItemInfoHasUnread(DatabaseHelper d,
			String head) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getReadableDatabase();
			Cursor cursor = db.query("ProblemItemInfoUnread", null, null, null,
					null, null, null);
			boolean f = false;
			while (cursor.moveToNext()) {
				if (cursor.getString(cursor.getColumnIndex("QuestionID"))
						.split("_")[0].equals(head)) {
					f = true;
					break;
				}
			}
			cursor.close();
			db.close();
			return f;
		}
	}

	public static ArrayList<ProblemItemInfo> getProblemItemInfo(
			DatabaseHelper d, String s) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getReadableDatabase();
			Cursor cursor = db.query("ProblemItemInfo", null, "QuestionID=?",
					new String[] { s }, null, null, null);
			ArrayList<ProblemItemInfo> list = new ArrayList<ProblemItemInfo>();
			while (cursor.moveToNext()) {
				ProblemItemInfo problemItemInfo = new ProblemItemInfo();
				problemItemInfo.setQuestionID(cursor.getString(cursor
						.getColumnIndex("QuestionID")));
				problemItemInfo.setSenderID(cursor.getString(cursor
						.getColumnIndex("SenderID")));
				problemItemInfo.setSenderVoIP(cursor.getString(cursor
						.getColumnIndex("SenderVoIP")));
				problemItemInfo.setSendTime(cursor.getString(cursor
						.getColumnIndex("SendTime")));
				problemItemInfo.setTextContent(cursor.getString(cursor
						.getColumnIndex("TextContent")));
				list.add(problemItemInfo);
			}
			cursor.close();
			db.close();
			return list;
		}
	}

	public static ArrayList<String> getProblemItemInfoUnread(DatabaseHelper d) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getReadableDatabase();
			Cursor cursor = db.query("ProblemItemInfoUnread", null, null, null,
					null, null, null);
			ArrayList<String> list = new ArrayList<String>();
			while (cursor.moveToNext()) {
				list.add(cursor.getString(cursor.getColumnIndex("QuestionID")));
			}
			cursor.close();
			db.close();
			return list;
		}
	}

	public static boolean checkProblemItemInfoUnread(DatabaseHelper d, String s) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getReadableDatabase();
			Cursor cursor = db.query("ProblemItemInfoUnread", null,
					"QuestionID=?", new String[] { s }, null, null, null);
			boolean f = true;
			while (cursor.moveToNext()) {
				f = false;
				break;
			}
			cursor.close();
			db.close();
			return f;
		}
	}

	/**
	 * @param d
	 * @param s
	 * @param f
	 *            trueΪδ����falseΪ�Ѷ�
	 */
	public static void setProblemItemInfoUnread(DatabaseHelper d, String s,
			boolean f) {
		synchronized (databaseHelper) {
			SQLiteDatabase db = null;
			db = d.getWritableDatabase();
			if (f) {
				Cursor cursor = db.query("ProblemItemInfoUnread", null,
						"QuestionID=?", new String[] { s }, null, null, null);
				if (!cursor.moveToNext()) {
					ContentValues contentValues = new ContentValues();
					contentValues.put("QuestionID", s);
					db.insert("ProblemItemInfoUnread", null, contentValues);
				}
				cursor.close();
			} else {
				db.delete("ProblemItemInfoUnread", "QuestionID=?",
						new String[] { s });
			}
			db.close();
		}
	}
}
