package com.wenbingwang.wenbingdoc;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.MyInfo;
import com.wenbingwang.mywidget.MyArrayAdapter;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * 家庭医生
 * 
 * @author Administrator
 * 
 */
public class Activity1 extends BaseActivity implements OnClickListener,
		OnRefreshListener {
	private Spinner spinner;
	private MyArrayAdapter<String> adapter;
	private Button activity1_back, activity1_button1;
	private int onclik = 0;// 0 未选中 1选中
	private ToggleButton activity1_toggle;
	private String weekPrice = null;
	private String monthPrice = null;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ArrayList<String> m = new ArrayList<String>();
	private View zy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity1);
		m.add("请设置价格");
		m.add("18.00元/周  68.00元/月");
		m.add("30.00元/周  108.00元/月");
		m.add("50.00元/周  188.00元/月");
		m.add("自定义其他金额");
		zy = findViewById(R.id.zy);
		activity1_toggle = (ToggleButton) findViewById(R.id.activity1_toggle);
		activity1_button1 = (Button) findViewById(R.id.activity1_button1);
		activity1_back = (Button) findViewById(R.id.activity1_image);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		spinner = (Spinner) findViewById(R.id.spinner1);
		// 将可选内容与ArrayAdapter连接起来
		adapter = new MyArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, m);
		// 设置下拉列表的风格
		adapter.setDropDownViewResource(R.layout.spinner_item);
		// 将adapter 添加到spinner中
		spinner.setAdapter(adapter);
		// 添加事件Spinner事件监听
		spinner.setOnItemSelectedListener(new SpinnerSelectedListener());
		// 开关切换事件
		activity1_toggle.setOnToggleChanged(new OnToggleChanged() {
			@Override
			public void onToggle(boolean on) {

				if (on == true) {
					onclik = 1;
				} else {
					onclik = 0;
				}
			}
		});
		activity1_button1.setOnClickListener(this);
		activity1_back.setOnClickListener(this);
		swipeRefreshLayout.setOnRefreshListener(this);
		onRefresh();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.activity1_button1:
			getfamily();
			break;

		case R.id.activity1_image:
			Activity1.this.finish();
			break;
		}
	}

	// 使用数组形式操作
	class SpinnerSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			switch (arg2) {
			case 1:
				weekPrice = "18";
				monthPrice = "68";
				break;
			case 2:
				weekPrice = "30";
				monthPrice = "108";
				break;
			case 3:
				weekPrice = "50";
				monthPrice = "188";
				break;
			case 4:
				showDialog();
				break;
			}

		}

		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}

	private void getfamily() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("HomeDoctorOpen", onclik + "");
		requestParams.add("HomeDoctorWeekPrice", weekPrice);
		requestParams.add("HomeDoctorMonthPrice", monthPrice);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/jtysinfo",
				requestParams, new Handler());
	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private class Handler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("HomeDoctorOpen").equals("1")) {
					activity1_toggle.setToggleOn();
					onclik = 1;
				} else {
					activity1_toggle.setToggleOff();
					onclik = 0;
				}
				weekPrice = jsonObject.getString("HomeDoctorWeekPrice");
				monthPrice = jsonObject.getString("HomeDoctorMonthPrice");
				adapter.setShowText(jsonObject.getString("HomeDoctorWeekPrice")
						+ "元/周 " + jsonObject.getString("HomeDoctorMonthPrice")
						+ "元/月");
				zy.setVisibility(View.VISIBLE);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(Activity1.this,
				"http://yisheng.wenbing.cn/Info/showjtinfo", requestParams,
				new Handler1());

	}

	private void showDialog() {
		LayoutInflater inflater = LayoutInflater.from(this);
		final View textEntryView = inflater.inflate(R.layout.dialog1, null);
		final EditText edtInput1 = (EditText) textEntryView
				.findViewById(R.id.editText1);
		final EditText edtInput2 = (EditText) textEntryView
				.findViewById(R.id.editText2);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false);
		builder.setTitle("家庭医生服务价格自定义");
		builder.setView(textEntryView);
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (!edtInput1.getText().toString().equals("")) {
					weekPrice = edtInput1.getText().toString();
				} else {
					weekPrice = "0.00";
				}
				if (!edtInput2.getText().toString().equals("")) {
					monthPrice = edtInput2.getText().toString();
				} else {
					monthPrice = "0.00";
				}
				spinner.setSelection(0, false);
				adapter.setShowText(weekPrice + "元/周 " + monthPrice + "元/月");
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		builder.show();
	}

}
