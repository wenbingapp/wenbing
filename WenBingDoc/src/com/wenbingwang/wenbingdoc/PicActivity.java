package com.wenbingwang.wenbingdoc;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class PicActivity extends BaseActivity {

	private Button about_back;
	private ImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pic_activity);
		about_back = (Button) findViewById(R.id.about_back);
		about_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		imageView = (ImageView) findViewById(R.id.imageView1);
		imageLoader.displayImage(getIntent().getStringExtra("pic"), imageView,
				options);
	}
}
