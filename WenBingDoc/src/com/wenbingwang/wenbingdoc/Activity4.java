package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.mywidget.MyArrayAdapter;
import com.zcw.togglebutton.ToggleButton;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class Activity4 extends BaseActivity implements OnClickListener,
		OnRefreshListener {
	private static final String[] m = { "请选择价格", "10.00元/次", "30.00元/次",
			"50.00元/次", "自定义其他金额" };
	private Spinner spinner;
	private MyArrayAdapter<String> adapter;
	private Button activity4_back, activity4_button1;
	private ToggleButton activity4_toggle;
	private int onclik = 0;
	private String outpatientPrice = null;
	private SwipeRefreshLayout swipeRefreshLayout;
	private View zy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity4);
		spinner = (Spinner) findViewById(R.id.spinner4);
		// 将可选内容与ArrayAdapter连接起来
		adapter = new MyArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, m);

		// 设置下拉列表的风格
		adapter.setDropDownViewResource(R.layout.spinner_item);

		// 将adapter 添加到spinner中
		spinner.setAdapter(adapter);

		// 添加事件Spinner事件监听
		spinner.setOnItemSelectedListener(new SpinnerSelectedListener());

		// 设置默认值
		zy = findViewById(R.id.zy);
		activity4_back = (Button) findViewById(R.id.activity4_back);
		activity4_button1 = (Button) findViewById(R.id.activity4_button1);
		activity4_toggle = (ToggleButton) findViewById(R.id.activity4_toggle);
		activity4_back.setOnClickListener(this);
		activity4_button1.setOnClickListener(this);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		// 切换开关
		// 开关切换事件
		activity4_toggle.setOnToggleChanged(new OnToggleChanged() {
			@Override
			public void onToggle(boolean on) {

				if (on == true) {
					onclik = 1;
				} else {
					onclik = 0;
				}
			}
		});
		onRefresh();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.activity4_back:
			Activity4.this.finish();
			break;

		case R.id.activity4_button1:
			getPhoto();
			break;
		}
	}

	// 使用数组形式操作
	class SpinnerSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			switch (arg2) {
			case 1:
				outpatientPrice = "10";
				break;
			case 2:
				outpatientPrice = "30";
				break;
			case 3:
				outpatientPrice = "50";
				break;

			case 4:
				showDialog();
				break;
			}
		}

		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}

	private void showDialog() {
		LayoutInflater inflater = LayoutInflater.from(this);
		final View textEntryView = inflater.inflate(R.layout.dialog2, null);
		final EditText edtInput1 = (EditText) textEntryView
				.findViewById(R.id.editText1);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false);
		builder.setTitle("门诊预约服务价格自定义");
		builder.setView(textEntryView);
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				if (!edtInput1.getText().toString().equals("")) {
					outpatientPrice = edtInput1.getText().toString();
				} else {
					outpatientPrice = "0.00";
				}
				spinner.setSelection(0, false);
				adapter.setShowText(outpatientPrice + "元/次 ");
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		builder.show();
	}

	private void getPhoto() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("OutpatientOpen", onclik + "");
		requestParams.add("OutpatientPrice", outpatientPrice);
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/mzinfo",
				requestParams, new Handler());

	}

	private class Handler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("SubmitResult").equals("1")) {
					showToast(jsonObject.getString("SubmitDescription"));
				} else {
					showToast(jsonObject.getString("SubmitDescription"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(Activity4.this,
				"http://yisheng.wenbing.cn/Info/showmzinfo", requestParams,
				new Handler1());
	}

	private class Handler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				if (jsonObject.getString("OutpatientOpen").equals("1")) {
					activity4_toggle.setToggleOn();
					onclik = 1;
				} else {
					activity4_toggle.setToggleOff();
					onclik = 0;
				}
				outpatientPrice = jsonObject.getString("OutpatientPrice");
				adapter.setShowText(outpatientPrice + "元/次 ");
				zy.setVisibility(View.VISIBLE);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}
	}
}
