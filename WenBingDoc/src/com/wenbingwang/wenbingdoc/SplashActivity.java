package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.ACache;
import com.wenbingwang.doc.application.DocInfo;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.MyInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends BaseActivity {
	private int i = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashactivity);
		final String s = CheckHasLogin(SplashActivity.this);
		checkUpdates();
		if (s != null) {
			setMyInfo(new MyInfo(s));
			setDocInfo();
		}
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				if (s != null) {
					intent.setClass(SplashActivity.this, MainActivity.class);
				} else {
					intent.setClass(SplashActivity.this, LoginActivity.class);
					i++;
				}
				if (i == 1) {
					startActivity(intent);
					finish();
				} else {
					AlertDialog.Builder builder = new Builder(
							SplashActivity.this);
					builder.setMessage("�뱣������ͨ��");
					builder.setTitle("��ʾ");
					builder.setPositiveButton("ȷ��", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							SplashActivity.this.finish();
						}
					});
					builder.create().show();
				}

			}
		}, 3000);

	}

	private void checkUpdates() {
		RequestParams requestParams = new RequestParams();
		requestParams.add("VersionID", AboutActivity.getVersionCode(this));
		requestParams.add("VersionType", "1");
		asyncHttpClient.post(this,
				"http://yisheng.wenbing.cn/Info/versioncheck", requestParams,
				new MyHandler3());
	}

	public static String CheckHasLogin(Context c) {
		ACache mCache = ACache.get(c);
		return mCache.getAsString("logininfo");
	}

	private void setDocInfo() {
		ACache mCache = ACache.get(SplashActivity.this);
		String s = mCache.getAsString("Docinfo");
		if (s == null) {
			RequestParams requestParams = new RequestParams();
			requestParams.add("UserID",
					((MyApplication) getApplication()).myInfo.getUserID());
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/zyylinfo", requestParams,
					new MyHandler1());
		} else {
			((MyApplication) getApplication()).myDocInfo = new DocInfo(s);
			i++;
		}

	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				DocInfo docInfo = new DocInfo(jsonObject.toString());
				((MyApplication) getApplication()).myDocInfo = docInfo;
				RequestParams requestParams = new RequestParams();
				requestParams.add("UserID",
						((MyApplication) getApplication()).myInfo.getUserID());
				asyncHttpClient.post(SplashActivity.this,
						"http://yisheng.wenbing.cn/Info/xxzlinfo",
						requestParams, new MyHandler2());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class MyHandler2 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				((MyApplication) getApplication()).myDocInfo
						.setFanCount(jsonObject.getString("FanCount"));
				((MyApplication) getApplication()).myDocInfo
						.setFormCount(jsonObject.getString("FormCount"));
				ACache mCache = ACache.get(SplashActivity.this);
				mCache.put("Docinfo",
						((MyApplication) getApplication()).myDocInfo.toString());
				i++;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	private class MyHandler3 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				((MyApplication) getApplication()).checkupdate = jsonObject
						.getString("CheckUpdate");
				((MyApplication) getApplication()).forceupdate = jsonObject
						.getString("ForceUpdate");
				((MyApplication) getApplication()).url = jsonObject
						.getString("DownloadUrl");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

}
