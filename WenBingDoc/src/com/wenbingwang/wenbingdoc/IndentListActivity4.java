package com.wenbingwang.wenbingdoc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.speedtong.sdk.im.ECMessage;
import com.speedtong.sdk.im.ECReport;
import com.speedtong.sdk.im.group.ECGroupNotice;
import com.wenbingwang.doc.application.IndentList4Info;
import com.wenbingwang.doc.application.IndentlistInfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class IndentListActivity4 extends BaseActivity implements
		OnRefreshListener, OnItemClickListener {
	private Adapter adapter;
	private ListView listView;
	private int page = 1;
	private SwipeRefreshLayout swipeRefreshLayout;// ABCD1587602961ab
	private TextView footer, activity1_text;
	private Button indentlist_back;
	private boolean hasmore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.indent_list);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		listView = (ListView) findViewById(R.id.indent_list);
		indentlist_back = (Button) findViewById(R.id.indentlist_back);
		activity1_text = (TextView) findViewById(R.id.activity1_text);
		activity1_text.setText("门诊预约");
		indentlist_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				IndentListActivity4.this.finish();
			}
		});
		listView.setOnItemClickListener(this);
		adapter = new Adapter();
		listView.setAdapter(adapter);
		addFooter(listView, R.layout.fragment_two_doclist_bottom);
		onRefresh();
	}

	private void addFooter(ListView v, int res) {
		View view = LayoutInflater.from(this).inflate(res, null);
		v.addFooterView(view);
		footer = (TextView) view.findViewById(R.id.textView1);
	}

	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		load(0);
	}

	private void load(int s) {
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		requestParams.add("Page", page + "");
		hasmore = true;
		try {
			asyncHttpClient.post(this,
					"http://yisheng.wenbing.cn/Info/mzlistinfo", requestParams,
					new MyHandler(s));
		} catch (Exception e) {
			// TODO: handle exception 1172.5652+496.3904
			System.out.println(e);
		}
	}

	private class MyHandler extends JsonHttpResponseHandler {
		private int state;

		public MyHandler(int s) {
			state = s;
		}

		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
			footer.setText("数据加载中");

		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				String responseString, Throwable throwable) {
			footer.setText("没有更多数据");
			hasmore = false;
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("网络连接失败，请检查网络");// ABCD15876029651a
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONArray response) {
			try {
				if (state == 0) {
					adapter.getList().clear();
					adapter.notifyDataSetChanged();
				}
				if (response.length() < 10) {
					footer.setText("没有更多数据");
					hasmore = false;
				} else {
					footer.setText("点击加载更多");
				}
				for (int i = 0; i < response.length(); i++) {
					IndentList4Info indentListInfo = new IndentList4Info(
							response.getJSONObject(i).toString());
					adapter.getList().add(indentListInfo);
					adapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);

		}

	}

	private class Adapter extends BaseAdapter {
		private ArrayList<IndentList4Info> list;

		public Adapter() {
			list = new ArrayList<IndentList4Info>();
		}

		public ArrayList<IndentList4Info> getList() {
			return list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(IndentListActivity4.this)
						.inflate(R.layout.fans_activity_item, null);
				viewHolder.textView1 = (TextView) convertView
						.findViewById(R.id.fans_activity_name);
				viewHolder.textView2 = (TextView) convertView
						.findViewById(R.id.fans_item_age);
				viewHolder.textView3 = (TextView) convertView
						.findViewById(R.id.fans_item_time);
				viewHolder.textView4 = (TextView) convertView
						.findViewById(R.id.fans_item_address);
				viewHolder.textView5 = (TextView) convertView
						.findViewById(R.id.fans_item_age1);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.fans_item_image);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			if ((list.get(position).getNickname()).length() > 3) {
				viewHolder.textView1.setText("患者："
						+ (list.get(position).getNickname()).substring(0, 2)
						+ "...");
			} else {
				viewHolder.textView1.setText("患者："
						+ list.get(position).getNickname());
			}

			// sex:1 男 2 女
			if ((list.get(position).getSex()).equals("1")) {
				viewHolder.textView2.setText("年龄:"
						+ list.get(position).getAge() + " 性别:男");
			} else if ((list.get(position).getSex()).equals("2")) {
				viewHolder.textView2.setText("年龄:"
						+ list.get(position).getAge() + " 性别:女");
			} else {
				viewHolder.textView2.setText("年龄:"
						+ list.get(position).getAge());
			}
			viewHolder.textView3.setText("电话:"
					+ list.get(position).getTelephone());
			viewHolder.textView4.setText("详情："
					+ list.get(position).getDetailDescription());
			if ((list.get(position).getFormStatus()).equals("0")) {
				viewHolder.textView5.setText("状态：待付款");
			} else if ((list.get(position).getFormStatus()).equals("1")) {
				viewHolder.textView5.setText("状态：待确诊");
			} else if ((list.get(position).getFormStatus()).equals("2")) {
				viewHolder.textView5.setText("状态：待评价");
			} else if ((list.get(position).getFormStatus()).equals("3")) {
				viewHolder.textView5.setText("状态：已完成");
			} else {
				viewHolder.textView5.setText("状态：已拒绝");
			}
			imageLoader.displayImage(list.get(position).getHeadImage(),
					viewHolder.imageView, options);
			return convertView;
		}
	}

	private class ViewHolder {
		public TextView textView1;
		public TextView textView2;
		public TextView textView3;
		public TextView textView4;
		public TextView textView5;
		private ImageView imageView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (position == adapter.getList().size()) {
			if (hasmore) {
				page++;
				load(1);
			}
		} else {
			Intent intent = new Intent(this, SubscribeActivity.class);
			intent.putExtra("-1", adapter.getList().get(position)
					.getImageArray());
			intent.putExtra("0", adapter.getList().get(position).getHeadImage());
			intent.putExtra("1", "患者："
					+ adapter.getList().get(position).getNickname());
			intent.putExtra(
					"2",
					"年龄："
							+ adapter.getList().get(position).getAge()
							+ "岁 性别："
							+ (adapter.getList().get(position).getSex()
									.equals("1") ? "男" : "女"));
			intent.putExtra("3-1", adapter.getList().get(position)
					.getOutpatientBeginTime());
			intent.putExtra("3-2", adapter.getList().get(position)
					.getOutpatientEndTime());
			intent.putExtra("4", "联系电话："
					+ adapter.getList().get(position).getTelephone());
			intent.putExtra("5", adapter.getList().get(position)
					.getDetailDescription());
			intent.putExtra("6", adapter.getList().get(position)
					.getOutpatientConfirmTime());
			intent.putExtra("7", adapter.getList().get(position)
					.getFormStatus());
			intent.putExtra("8", adapter.getList().get(position)
					.getOutpatientID());
			startActivityForResult(intent, 0);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			String phoneTime = data.getStringExtra("id");
			for (IndentList4Info indentList4Info : adapter.getList()) {
				if (indentList4Info.getOutpatientID().equals(phoneTime)) {
					indentList4Info
							.setFormStatus(data.getStringExtra("states"));
					if (data.getStringExtra("states").equals("2")) {
						indentList4Info.setOutpatientConfirmTime(data
								.getStringExtra("time"));
					}
					adapter.notifyDataSetChanged();
					break;
				}
			}
		}
	}
}
