package com.wenbingwang.wenbingdoc;

import java.text.DecimalFormat;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.FansInfo;
import com.wenbingwang.doc.application.PriceInfo;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Activity6 extends BaseActivity implements OnRefreshListener {

	private Button activity6_back;
	private PriceInfo priceInfo;
	private int[] btn = { R.id.activity6_jiatinga, R.id.activity6_jiatingb,
			R.id.activity6_jiatingc, R.id.activity6_jiatingd,
			R.id.activity6_jiatinge, R.id.activity6_jiatingf,
			R.id.activity6_jiatingg, R.id.activity6_jiatingh,
			R.id.activity6_jiatingi, R.id.activity6_jiatingj,
			R.id.activity6_jiatingk, R.id.activity6_jiatingl };
	private TextView[] btnstop;
	private int i = 0;

	private TextView textView1;
	private TextView textView2;
	private TextView textView3;
	private SwipeRefreshLayout swipeRefreshLayout;
	private View zy;
	DecimalFormat df = new DecimalFormat("0");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity6);
		// activity6_jiatingss=(TextView)
		// findViewById(R.id.activity6_jiatingss);
		activity6_back = (Button) findViewById(R.id.activity6_back);
		btnstop = new TextView[12];
		for (i = 0; i < btn.length; i++) {
			btnstop[i] = (TextView) findViewById(btn[i]);
		}
		textView1 = (TextView) findViewById(R.id.gh1);
		textView2 = (TextView) findViewById(R.id.gh2);
		textView3 = (TextView) findViewById(R.id.gh3);
		zy = findViewById(R.id.zy);
		activity6_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Activity6.this.finish();
			}
		});
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeRefreshLayout.setOnRefreshListener(this);
		onRefresh();
	}

	private class LoginHandler extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			showToast("网络连接失败，请检查网络");
			swipeRefreshLayout.setRefreshing(false);
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				i = 0;
				priceInfo = new PriceInfo(jsonObject.toString());
				btnstop[i++].setText("家庭医生："
						+ dobles(priceInfo.getHomeDoctor1()) + "元");
				btnstop[i++].setText("图文咨询："
						+ dobles(priceInfo.getOutpatient1()) + "元");
				btnstop[i++].setText("电话咨询："
						+ dobles(priceInfo.getTelephone1()) + "元");
				btnstop[i++].setText("门诊预约："
						+ dobles(priceInfo.getImageWord1()) + "元");
				btnstop[i++].setText("家庭医生："
						+ dobles(priceInfo.getHomeDoctor2()) + "元");
				btnstop[i++].setText("图文咨询："
						+ dobles(priceInfo.getOutpatient2()) + "元");
				btnstop[i++].setText("电话咨询："
						+ dobles(priceInfo.getTelephone2()) + "元");
				btnstop[i++].setText("门诊预约："
						+ dobles(priceInfo.getImageWord2()) + "元");
				btnstop[i++].setText("家庭医生："
						+ dobles(priceInfo.getHomeDoctor3()) + "元");
				btnstop[i++].setText("图文咨询："
						+ dobles(priceInfo.getImageWord3()) + "元");
				btnstop[i++].setText("电话咨询："
						+ dobles(priceInfo.getTelephone3()) + "元");
				btnstop[i++].setText("门诊预约："
						+ dobles(priceInfo.getOutpatient3()) + "元");
				textView1
						.setText("昨日收入："
								+ df.format((Float.valueOf(priceInfo
										.getHomeDoctor1())
										+ Float.valueOf(priceInfo
												.getOutpatient1())
										+ Float.valueOf(priceInfo
												.getImageWord1()) + Float
										.valueOf(priceInfo.getTelephone1())))
								+ "元");
				textView2
						.setText("本月收入："
								+ df.format((Float.valueOf(priceInfo
										.getHomeDoctor2())
										+ Float.valueOf(priceInfo
												.getOutpatient2())
										+ Float.valueOf(priceInfo
												.getImageWord2()) + Float
										.valueOf(priceInfo.getTelephone2())))
								+ "元");
				textView3
						.setText("上月收入："
								+ df.format((Float.valueOf(priceInfo
										.getHomeDoctor3())
										+ Float.valueOf(priceInfo
												.getOutpatient3())
										+ Float.valueOf(priceInfo
												.getImageWord3()) + Float
										.valueOf(priceInfo.getTelephone3())))
								+ "元");
				zy.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(this, "http://yisheng.wenbing.cn/Info/srjsinfo",
				requestParams, new LoginHandler());
	}

}
