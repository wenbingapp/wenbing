package com.wenbingwang.wenbingdoc;

import com.loopj.android.http.AsyncHttpClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.wenbingwang.doc.application.MyApplication;
import com.wenbingwang.doc.application.MyInfo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

public class BaseFragment extends Fragment {
	protected DisplayImageOptions options;
	protected ImageLoader imageLoader;
	protected AsyncHttpClient asyncHttpClient;
	protected Toast toast;
	protected MyInfo myInfo;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		myInfo = ((MyApplication) activity.getApplication()).myInfo;
		asyncHttpClient = ((MyApplication) activity.getApplication())
				.getAsyncHttpClient();
		imageLoader = ImageLoader.getInstance();
		ColorDrawable colorDrawable = new ColorDrawable(0x969696);
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(colorDrawable)
				.showImageOnFail(colorDrawable).cacheInMemory(true)
				.cacheOnDisk(true).displayer(new BitmapDisplayer() {
					@Override
					public void display(Bitmap bitmap, ImageAware imageAware,
							LoadedFrom loadedFrom) {
						// TODO Auto-generated method stub
						imageAware.setImageBitmap(bitmap);
					}
				}).build();
	}

	protected void showToast(String s) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT);
		toast.show();
	}

}
