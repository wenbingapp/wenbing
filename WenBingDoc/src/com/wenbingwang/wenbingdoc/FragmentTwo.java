package com.wenbingwang.wenbingdoc;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wenbingwang.doc.application.ACache;
import com.wenbingwang.doc.application.DocInfo;
import com.wenbingwang.doc.application.MyApplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentTwo extends BaseFragment implements OnRefreshListener,
		OnClickListener {
	private View rootView;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ImageView imageView;
	private TextView textView;
	private Button button1;
	private Button button2;
	private DocInfo docInfo;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.frament_two, container, false);
			swipeRefreshLayout = (SwipeRefreshLayout) rootView
					.findViewById(R.id.swipe_container);
			swipeRefreshLayout.setOnRefreshListener(this);
			docInfo = ((MyApplication) getActivity().getApplication()).myDocInfo;
			imageView = (ImageView) rootView.findViewById(R.id.imageView1);
			textView = (TextView) rootView.findViewById(R.id.textView1);
			button1 = (Button) rootView.findViewById(R.id.frament_two_btn1);
			button2 = (Button) rootView.findViewById(R.id.frament_two_btn2);
			button1.setOnClickListener(this);
			button2.setOnClickListener(this);
			imageLoader.displayImage(docInfo.getDoctorHeadImageUrl(),
					imageView, options);
			textView.setText(docInfo.getDoctorNickName() + "ҽ�������ã�");
			button1.setText("��˿����" + docInfo.getFanCount());
			button2.setText("��������" + docInfo.getFormCount());
		}
		return rootView;

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((ViewGroup) rootView.getParent()).removeView(rootView);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		RequestParams requestParams = new RequestParams();
		requestParams.add("UserID", myInfo.getUserID());
		asyncHttpClient.post(getActivity(),
				"http://yisheng.wenbing.cn/Info/zyylinfo", requestParams,
				new MyHandler1());
	}

	private class MyHandler1 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				docInfo = new DocInfo(jsonObject.toString());
				imageLoader.displayImage(docInfo.getDoctorHeadImageUrl(),
						imageView, options);
				textView.setText(docInfo.getDoctorNickName() + "ҽ�������ã�");
				RequestParams requestParams = new RequestParams();
				requestParams.add("UserID", myInfo.getUserID());
				asyncHttpClient.post(getActivity(),
						"http://yisheng.wenbing.cn/Info/xxzlinfo",
						requestParams, new MyHandler2());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private class MyHandler2 extends JsonHttpResponseHandler {
		@Override
		public void onStart() {
			// Initiated the request
			swipeRefreshLayout.setRefreshing(true);
		}

		@Override
		public void onFailure(int statusCode, Header[] headers,
				Throwable throwable, JSONObject errorResponse) {
			swipeRefreshLayout.setRefreshing(false);
			showToast("��������ʧ�ܣ���������");
		}

		@Override
		public void onSuccess(int statusCode, Header[] headers,
				JSONObject jsonObject) {
			try {
				docInfo.setFanCount(jsonObject.getString("FanCount"));
				docInfo.setFormCount(jsonObject.getString("FormCount"));
				button1.setText("��˿����" + docInfo.getFanCount());
				button2.setText("��������" + docInfo.getFormCount());
				((MyApplication) getActivity().getApplication()).myDocInfo = docInfo;
				ACache mCache = ACache.get(getActivity());
				mCache.put("Docinfo", ((MyApplication) getActivity()
						.getApplication()).myDocInfo.toString());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.frament_two_btn1:// ��˿�� �����˺�15221025027 123456
			intent.setClass(getActivity(), FansActivity.class);
			startActivity(intent);
			break;

		case R.id.frament_two_btn2:// ������
			intent.setClass(getActivity(), IndentActivity.class);
			startActivity(intent);
			break;

		}
	}

}