package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONObject;

public class PassWrodInfo implements Serializable {

	private String SubmitResult;
	private String SubmitDescription;

	public String getSubmitResult() {
		return SubmitResult;
	}

	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}

	public String getSubmitDescription() {
		return SubmitDescription;
	}

	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}

	public PassWrodInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			SubmitResult = jsonObject.getString("SubmitResult");
			SubmitDescription = jsonObject.getString("SubmitDescription");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
