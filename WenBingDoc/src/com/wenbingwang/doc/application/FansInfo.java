package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class FansInfo implements Serializable{
	
	
	public String getFansID() {
		return FansID;
	}
	public void setFansID(String fansID) {
		FansID = fansID;
	}
	public String getHeadImage() {
		return HeadImage;
	}
	public void setHeadImage(String headImage) {
		HeadImage = headImage;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getVIP() {
		return VIP;
	}
	public void setVIP(String vIP) {
		VIP = vIP;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	public String getAge() {
		return Age;
	}
	public void setAge(String age) {
		Age = age;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	private String FansID;
	private String HeadImage;
	private String Nickname;
	private String VIP;
	private String Time;
	private String Age;
	private String Sex;
	private String Address;
	
	public FansInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			FansID = jsonObject.getString("FansID");
			if (FansID.contains("_")) {
				FansID = FansID.split("_")[1];
			}
			HeadImage = jsonObject.getString("HeadImage");
			Nickname = jsonObject.getString("Nickname");
			VIP = jsonObject.getString("VIP");
			Time = jsonObject.getString("Time");
			Age = jsonObject.getString("Age");
			Sex = jsonObject.getString("Sex");
			Address = jsonObject.getString("Address");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	

}
