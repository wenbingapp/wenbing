package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONObject;

public class IndentList3Info implements Serializable{
	
	private String TelephoneID;
	private String TelephoneType;
	private String TelephoneTime;
	private String TelephoneDuration;
	private String UserID;
	private String HeadImage;
	private String Nickname;
	private String SubmitTime;
	private String FormStatus;
	
	public IndentList3Info(String s){
		try {
			JSONObject jsonObject = new JSONObject(s);
			TelephoneID = jsonObject.getString("TelephoneID");
			if (TelephoneID.contains("_")) {
				TelephoneID = TelephoneID.split("_")[1];
			}
			TelephoneType = jsonObject.getString("TelephoneType");
			TelephoneTime = jsonObject.getString("TelephoneTime");
			TelephoneDuration = jsonObject.getString("TelephoneDuration");
			UserID = jsonObject.getString("UserID");
			HeadImage = jsonObject.getString("HeadImage");
			Nickname = jsonObject.getString("Nickname");
			SubmitTime = jsonObject.getString("SubmitTime");
			FormStatus = jsonObject.getString("FormStatus");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public String getTelephoneID() {
		return TelephoneID;
	}
	public void setTelephoneID(String telephoneID) {
		TelephoneID = telephoneID;
	}
	public String getTelephoneType() {
		return TelephoneType;
	}
	public void setTelephoneType(String telephoneType) {
		TelephoneType = telephoneType;
	}
	public String getTelephoneTime() {
		return TelephoneTime;
	}
	public void setTelephoneTime(String telephoneTime) {
		TelephoneTime = telephoneTime;
	}
	public String getTelephoneDuration() {
		return TelephoneDuration;
	}
	public void setTelephoneDuration(String telephoneDuration) {
		TelephoneDuration = telephoneDuration;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getHeadImage() {
		return HeadImage;
	}
	public void setHeadImage(String headImage) {
		HeadImage = headImage;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getSubmitTime() {
		return SubmitTime;
	}
	public void setSubmitTime(String submitTime) {
		SubmitTime = submitTime;
	}
	public String getFormStatus() {
		return FormStatus;
	}
	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}
	

}
