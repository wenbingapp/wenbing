package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONObject;

public class IndentList4Info implements Serializable{
	
	private String OutpatientID;
	private String UserID;
	private String HeadImage;
	private String Nickname;
	private String Age;
	private String Sex;
	private String Telephone;
	private String DetailDescription;
	private String ImageArray;
	private String OutpatientBeginTime;
	private String OutpatientEndTime;
	private String OutpatientConfirmTime;
	private String FormStatus;
	
	public IndentList4Info(String s){
		
		try {
			JSONObject jsonObject = new JSONObject(s);
			OutpatientID = jsonObject.getString("OutpatientID");
			if (OutpatientID.contains("_")) {
				OutpatientID = OutpatientID.split("_")[1];
			}
			UserID = jsonObject.getString("UserID");
			HeadImage = jsonObject.getString("HeadImage");
			Nickname = jsonObject.getString("Nickname");
			Age = jsonObject.getString("Age");
			Sex = jsonObject.getString("Sex");
			Telephone = jsonObject.getString("Telephone");
			DetailDescription = jsonObject.getString("DetailDescription");
			ImageArray = jsonObject.getString("ImageArray");
			OutpatientBeginTime = jsonObject.getString("OutpatientBeginTime");
			OutpatientEndTime = jsonObject.getString("OutpatientEndTime");
			OutpatientConfirmTime = jsonObject.getString("OutpatientConfirmTime");
			FormStatus = jsonObject.getString("FormStatus");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
	public String getOutpatientID() {
		return OutpatientID;
	}
	public void setOutpatientID(String outpatientID) {
		OutpatientID = outpatientID;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getHeadImage() {
		return HeadImage;
	}
	public void setHeadImage(String headImage) {
		HeadImage = headImage;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getAge() {
		return Age;
	}
	public void setAge(String age) {
		Age = age;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getTelephone() {
		return Telephone;
	}
	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	public String getDetailDescription() {
		return DetailDescription;
	}
	public void setDetailDescription(String detailDescription) {
		DetailDescription = detailDescription;
	}
	public String getImageArray() {
		return ImageArray;
	}
	public void setImageArray(String imageArray) {
		ImageArray = imageArray;
	}
	public String getOutpatientBeginTime() {
		return OutpatientBeginTime;
	}
	public void setOutpatientBeginTime(String outpatientBeginTime) {
		OutpatientBeginTime = outpatientBeginTime;
	}
	public String getOutpatientEndTime() {
		return OutpatientEndTime;
	}
	public void setOutpatientEndTime(String outpatientEndTime) {
		OutpatientEndTime = outpatientEndTime;
	}
	public String getOutpatientConfirmTime() {
		return OutpatientConfirmTime;
	}
	public void setOutpatientConfirmTime(String outpatientConfirmTime) {
		OutpatientConfirmTime = outpatientConfirmTime;
	}
	public String getFormStatus() {
		return FormStatus;
	}
	public void setFormStatus(String formStatus) {
		FormStatus = formStatus;
	}
	
	

}
