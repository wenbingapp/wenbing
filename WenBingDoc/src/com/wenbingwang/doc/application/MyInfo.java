package com.wenbingwang.doc.application;

import org.json.JSONException;
import org.json.JSONObject;

public class MyInfo {

	private String UserID;

	public MyInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			UserID = jsonObject.getString("UserID");
			SubAccount = jsonObject.getString("SubAccount");
			SubPassword = jsonObject.getString("SubPassword");
			VoIPAccount = jsonObject.getString("VoIPAccount");
			VoIPPassword = jsonObject.getString("VoIPPassword");
			SubmitResult = jsonObject.getString("SubmitResult");
			SubmitDescription = jsonObject.getString("SubmitDescription");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getSubAccount() {
		return SubAccount;
	}

	public void setSubAccount(String subAccount) {
		SubAccount = subAccount;
	}

	public String getSubPassword() {
		return SubPassword;
	}

	public void setSubPassword(String subPassword) {
		SubPassword = subPassword;
	}

	public String getVoIPAccount() {
		return VoIPAccount;
	}

	public void setVoIPAccount(String voIPAccount) {
		VoIPAccount = voIPAccount;
	}

	public String getVoIPPassword() {
		return VoIPPassword;
	}

	public void setVoIPPassword(String voIPPassword) {
		VoIPPassword = voIPPassword;
	}

	public String getSubmitResult() {
		return SubmitResult;
	}

	public void setSubmitResult(String submitResult) {
		SubmitResult = submitResult;
	}

	public String getSubmitDescription() {
		return SubmitDescription;
	}

	public void setSubmitDescription(String submitDescription) {
		SubmitDescription = submitDescription;
	}

	private String SubAccount;
	private String SubPassword;
	private String VoIPAccount;
	private String VoIPPassword;
	private String SubmitResult;
	private String SubmitDescription;

}
