package com.wenbingwang.doc.application;

import org.json.JSONException;
import org.json.JSONObject;

public class DocInfo {

	public String getDoctorHeadImageUrl() {
		return DoctorHeadImageUrl;
	}

	public void setDoctorHeadImageUrl(String doctorHeadImageUrl) {
		DoctorHeadImageUrl = doctorHeadImageUrl;
	}

	public String getDoctorNickName() {
		return DoctorNickName;
	}

	public void setDoctorNickName(String doctorNickName) {
		DoctorNickName = doctorNickName;
	}

	public String getDoctorLevel() {
		return DoctorLevel;
	}

	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}

	public String getDoctorHospital() {
		return DoctorHospital;
	}

	public void setDoctorHospital(String doctorHospital) {
		DoctorHospital = doctorHospital;
	}

	public String getDoctorAbility() {
		return DoctorAbility;
	}

	public void setDoctorAbility(String doctorAbility) {
		DoctorAbility = doctorAbility;
	}

	public String getDoctorBriefIntroduction() {
		return DoctorBriefIntroduction;
	}

	public void setDoctorBriefIntroduction(String doctorBriefIntroduction) {
		DoctorBriefIntroduction = doctorBriefIntroduction;
	}

	public String getHomeDoctorWeekPrice() {
		return HomeDoctorWeekPrice;
	}

	public void setHomeDoctorWeekPrice(String homeDoctorWeekPrice) {
		HomeDoctorWeekPrice = homeDoctorWeekPrice;
	}

	public String getHomeDoctorMonthPrice() {
		return HomeDoctorMonthPrice;
	}

	public void setHomeDoctorMonthPrice(String homeDoctorMonthPrice) {
		HomeDoctorMonthPrice = homeDoctorMonthPrice;
	}

	public String getImageWordPrice() {
		return ImageWordPrice;
	}

	public void setImageWordPrice(String imageWordPrice) {
		ImageWordPrice = imageWordPrice;
	}

	public String getTelephonePrice() {
		return TelephonePrice;
	}

	public void setTelephonePrice(String telephonePrice) {
		TelephonePrice = telephonePrice;
	}

	public String getOutpatientPrice() {
		return OutpatientPrice;
	}

	public void setOutpatientPrice(String outpatientPrice) {
		OutpatientPrice = outpatientPrice;
	}

	public String getHomeDoctorBuys() {
		return HomeDoctorBuys;
	}

	public void setHomeDoctorBuys(String homeDoctorBuys) {
		HomeDoctorBuys = homeDoctorBuys;
	}

	public String getImageWordBuys() {
		return ImageWordBuys;
	}

	public void setImageWordBuys(String imageWordBuys) {
		ImageWordBuys = imageWordBuys;
	}

	public String getTelephoneBuys() {
		return TelephoneBuys;
	}

	public void setTelephoneBuys(String telephoneBuys) {
		TelephoneBuys = telephoneBuys;
	}

	public String getOutpatientBuys() {
		return OutpatientBuys;
	}

	public void setOutpatientBuys(String outpatientBuys) {
		OutpatientBuys = outpatientBuys;
	}

	public String getHomeDoctorOpen() {
		return HomeDoctorOpen;
	}

	public void setHomeDoctorOpen(String homeDoctorOpen) {
		HomeDoctorOpen = homeDoctorOpen;
	}

	public String getImageWordOpen() {
		return ImageWordOpen;
	}

	public void setImageWordOpen(String imageWordOpen) {
		ImageWordOpen = imageWordOpen;
	}

	public String getTelephoneOpen() {
		return TelephoneOpen;
	}

	public void setTelephoneOpen(String telephoneOpen) {
		TelephoneOpen = telephoneOpen;
	}

	public String getOutpatientOpen() {
		return OutpatientOpen;
	}

	public void setOutpatientOpen(String outpatientOpen) {
		OutpatientOpen = outpatientOpen;
	}

	public String getHomeDoctorValue() {
		return HomeDoctorValue;
	}

	public void setHomeDoctorValue(String homeDoctorValue) {
		HomeDoctorValue = homeDoctorValue;
	}

	public String getOutpatientValue() {
		return OutpatientValue;
	}

	public void setOutpatientValue(String outpatientValue) {
		OutpatientValue = outpatientValue;
	}

	private String DoctorHeadImageUrl;
	private String DoctorNickName;
	private String DoctorLevel;
	private String DoctorHospital;
	private String DoctorAbility;
	private String DoctorBriefIntroduction;
	private String HomeDoctorWeekPrice;
	private String HomeDoctorMonthPrice;
	private String ImageWordPrice;
	private String TelephonePrice;
	private String OutpatientPrice;
	private String HomeDoctorBuys;
	private String ImageWordBuys;
	private String TelephoneBuys;
	private String OutpatientBuys;
	private String HomeDoctorOpen;
	private String ImageWordOpen;
	private String TelephoneOpen;
	private String OutpatientOpen;
	private String HomeDoctorValue;
	private String OutpatientValue;

	private String FanCount;

	public String getFanCount() {
		return FanCount;
	}

	public void setFanCount(String fanCount) {
		FanCount = fanCount;
	}

	public String getFormCount() {
		return FormCount;
	}

	public void setFormCount(String formCount) {
		FormCount = formCount;
	}

	private String FormCount;

	public DocInfo(String s) {
		try {
			JSONObject jsonObject = new JSONObject(s);
			DoctorHeadImageUrl = jsonObject.getString("DoctorHeadImageUrl");
			DoctorNickName = jsonObject.getString("DoctorNickName");
			DoctorLevel = jsonObject.getString("DoctorLevel");
			DoctorHospital = jsonObject.getString("DoctorHospital");
			DoctorAbility = jsonObject.getString("DoctorAbility");
			DoctorBriefIntroduction = jsonObject
					.getString("DoctorBriefIntroduction");
			HomeDoctorWeekPrice = jsonObject.getString("HomeDoctorWeekPrice");
			HomeDoctorMonthPrice = jsonObject.getString("HomeDoctorMonthPrice");
			ImageWordPrice = jsonObject.getString("ImageWordPrice");
			TelephonePrice = jsonObject.getString("TelephonePrice");
			OutpatientPrice = jsonObject.getString("OutpatientPrice");
			HomeDoctorBuys = jsonObject.getString("HomeDoctorBuys");
			ImageWordBuys = jsonObject.getString("ImageWordBuys");
			TelephoneBuys = jsonObject.getString("TelephoneBuys");
			OutpatientBuys = jsonObject.getString("OutpatientBuys");
			HomeDoctorOpen = jsonObject.getString("HomeDoctorOpen");
			ImageWordOpen = jsonObject.getString("ImageWordOpen");
			TelephoneOpen = jsonObject.getString("TelephoneOpen");
			OutpatientOpen = jsonObject.getString("OutpatientOpen");
			HomeDoctorValue = jsonObject.getString("HomeDoctorValue");
			OutpatientValue = jsonObject.getString("OutpatientValue");
			FanCount = jsonObject.getString("FanCount");
			FormCount = jsonObject.getString("FormCount");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("DoctorHeadImageUrl", DoctorHeadImageUrl);
			jsonObject.put("DoctorNickName", DoctorNickName);
			jsonObject.put("DoctorLevel", DoctorLevel);
			jsonObject.put("DoctorHospital", DoctorHospital);
			jsonObject.put("DoctorAbility", DoctorAbility);
			jsonObject.put("DoctorBriefIntroduction", DoctorBriefIntroduction);
			jsonObject.put("HomeDoctorWeekPrice", HomeDoctorWeekPrice);
			jsonObject.put("HomeDoctorMonthPrice", HomeDoctorMonthPrice);
			jsonObject.put("ImageWordPrice", ImageWordPrice);
			jsonObject.put("TelephonePrice", TelephonePrice);
			jsonObject.put("OutpatientPrice", OutpatientPrice);
			jsonObject.put("HomeDoctorBuys", HomeDoctorBuys);
			jsonObject.put("ImageWordBuys", ImageWordBuys);
			jsonObject.put("TelephoneBuys", TelephoneBuys);
			jsonObject.put("OutpatientBuys", OutpatientBuys);
			jsonObject.put("HomeDoctorOpen", HomeDoctorOpen);
			jsonObject.put("ImageWordOpen", ImageWordOpen);
			jsonObject.put("TelephoneOpen", TelephoneOpen);
			jsonObject.put("OutpatientOpen", OutpatientOpen);
			jsonObject.put("HomeDoctorValue", HomeDoctorValue);
			jsonObject.put("OutpatientValue", OutpatientValue);
			jsonObject.put("FanCount", FanCount);
			jsonObject.put("FormCount", FormCount);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject.toString();

	}
}
