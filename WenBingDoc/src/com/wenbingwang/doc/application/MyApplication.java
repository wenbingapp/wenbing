package com.wenbingwang.doc.application;

import java.util.HashMap;
import java.util.Map;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.speedtong.sdk.ECDevice;
import com.speedtong.sdk.ECDevice.InitListener;
import com.speedtong.sdk.ECInitialize;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {

	private AsyncHttpClient asyncHttpClient;
	public MyInfo myInfo;
	public DocInfo myDocInfo;
	public String question;
	public ECInitialize params;
	public String checkupdate = "0";
	public String forceupdate = "0";
	public String url;

	public AsyncHttpClient getAsyncHttpClient() {
		return asyncHttpClient;
	}

	private PersistentCookieStore myCookieStore;

	@Override
	public void onCreate() {
		super.onCreate();
		asyncHttpClient = new AsyncHttpClient();
		myCookieStore = new PersistentCookieStore(this);
		asyncHttpClient.setCookieStore(myCookieStore);
		asyncHttpClient.setTimeout(5000);
		initImageLoader(getApplicationContext());
		init();
	}

	private void init() {
		ECDevice.initial(this, new InitListener() {

			@Override
			public void onInitialized() {
				// TODO Auto-generated method stub
			}

			@Override
			public void onError(Exception arg0) {
				// TODO Auto-generated method stub
			}
		});
	}

	private void initImageLoader(Context context) {
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(
				context);
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.writeDebugLogs(); // Remove for release app

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config.build());
	}
}
