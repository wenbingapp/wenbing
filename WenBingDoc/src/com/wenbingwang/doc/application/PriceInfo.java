package com.wenbingwang.doc.application;

import java.io.Serializable;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.util.JsonToken;

public class PriceInfo implements Serializable{
	
	private String HomeDoctor1;
	private String ImageWord1;
	private String Telephone1;
	private String Outpatient1;
	private String HomeDoctor2;
	private String ImageWord2;
	private String Telephone2;
	private String Outpatient2;
	private String HomeDoctor3;
	private String ImageWord3;
	private String Telephone3;
	private String Outpatient3;
	private String Yesterday;
	private String ThisMonth;
	private String LastMonth;
	public PriceInfo(String s){
		try {
			JSONObject jsonObject = new JSONObject(s);
			Yesterday = jsonObject.getString("Yesterday");
			ThisMonth = jsonObject.getString("ThisMonth");
			LastMonth = jsonObject.getString("LastMonth");
			JSONObject json1=new JSONObject(Yesterday);
			Telephone1 = json1.getString("Telephone");
			Outpatient1 = json1.getString("Outpatient");
			HomeDoctor1 = json1.getString("HomeDoctor");
			ImageWord1= json1.getString("ImageWord");
			JSONObject json2=new JSONObject(ThisMonth);
			Telephone2 = json2.getString("Telephone");
			Outpatient2 = json2.getString("Outpatient");
			HomeDoctor2 = json2.getString("HomeDoctor");
			ImageWord2= json2.getString("ImageWord");
			JSONObject json3=new JSONObject(LastMonth);
			Telephone3 = json3.getString("Telephone");
			Outpatient3 = json3.getString("Outpatient");
			HomeDoctor3 = json3.getString("HomeDoctor");
			ImageWord3= json3.getString("ImageWord");
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getHomeDoctor1() {
		return HomeDoctor1;
	}
	public void setHomeDoctor1(String homeDoctor1) {
		HomeDoctor1 = homeDoctor1;
	}
	public String getImageWord1() {
		return ImageWord1;
	}
	public void setImageWord1(String imageWord1) {
		ImageWord1 = imageWord1;
	}
	public String getTelephone1() {
		return Telephone1;
	}
	public void setTelephone1(String telephone1) {
		Telephone1 = telephone1;
	}
	public String getOutpatient1() {
		return Outpatient1;
	}
	public void setOutpatient1(String outpatient1) {
		Outpatient1 = outpatient1;
	}
	public String getHomeDoctor2() {
		return HomeDoctor2;
	}
	public void setHomeDoctor2(String homeDoctor2) {
		HomeDoctor2 = homeDoctor2;
	}
	public String getImageWord2() {
		return ImageWord2;
	}
	public void setImageWord2(String imageWord2) {
		ImageWord2 = imageWord2;
	}
	public String getTelephone2() {
		return Telephone2;
	}
	public void setTelephone2(String telephone2) {
		Telephone2 = telephone2;
	}
	public String getOutpatient2() {
		return Outpatient2;
	}
	public void setOutpatient2(String outpatient2) {
		Outpatient2 = outpatient2;
	}
	public String getHomeDoctor3() {
		return HomeDoctor3;
	}
	public void setHomeDoctor3(String homeDoctor3) {
		HomeDoctor3 = homeDoctor3;
	}
	public String getImageWord3() {
		return ImageWord3;
	}
	public void setImageWord3(String imageWord3) {
		ImageWord3 = imageWord3;
	}
	public String getTelephone3() {
		return Telephone3;
	}
	public void setTelephone3(String telephone3) {
		Telephone3 = telephone3;
	}
	public String getOutpatient3() {
		return Outpatient3;
	}
	public void setOutpatient3(String outpatient3) {
		Outpatient3 = outpatient3;
	}
	public String getYesterday() {
		return Yesterday;
	}
	public void setYesterday(String yesterday) {
		Yesterday = yesterday;
	}
	public String getThisMonth() {
		return ThisMonth;
	}
	public void setThisMonth(String thisMonth) {
		ThisMonth = thisMonth;
	}
	public String getLastMonth() {
		return LastMonth;
	}
	public void setLastMonth(String lastMonth) {
		LastMonth = lastMonth;
	}
	

	

}
