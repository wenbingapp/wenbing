package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class CommentsInfo implements Serializable{
	
	private String  CommentID;
	private String CommentValue;
	private String  CommentDescription;
	private String UserID;
	private String  Nickname;
	private String Time;
	
	public CommentsInfo(String s){
		try {
			JSONObject jsonObject = new JSONObject(s);
			CommentID = jsonObject.getString("CommentID");
			CommentValue = jsonObject.getString("CommentValue");
			CommentDescription = jsonObject.getString("CommentDescription");
			UserID = jsonObject.getString("UserID");
			Nickname = jsonObject.getString("Nickname");
			Time = jsonObject.getString("Time");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public String getCommentID() {
		return CommentID;
	}
	public void setCommentID(String commentID) {
		CommentID = commentID;
	}
	public String getCommentValue() {
		return CommentValue;
	}
	public void setCommentValue(String commentValue) {
		CommentValue = commentValue;
	}
	public String getCommentDescription() {
		return CommentDescription;
	}
	public void setCommentDescription(String commentDescription) {
		CommentDescription = commentDescription;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	
	

}
