package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONObject;

public class HomeInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String DoctorHeadImageUrl;
	private String DoctorNickName;
	private String DoctorLevel;
	private String DoctorHospital;
	private String DoctorAbility;
	private String DoctorBriefIntroduction;
	private String HomeDoctorWeekPrice;
	private String HomeDoctorMonthPrice;
	private String ImageWordPrice;
	private String TelephonePrice;
	private String OutpatientPrice;
	private String HomeDoctorBuys;
	private String ImageWordBuys;
	private String TelephoneBuys;
	private String OutpatientBuys;
	private String HomeDoctorOpen;
	private String ImageWordOpen;
	private String TelephoneOpen;
	private String OutpatientOpen;
	private String HomeDoctorValue;
	private String ImageWordValue;
	private String TelephoneValue;
	private String OutpatientValue;
	
	
	
	public HomeInfo(String homeDoctorWeekPrice, String homeDoctorMonthPrice,
			String imageWordPrice, String telephonePrice,
			String outpatientPrice, String homeDoctorBuys,
			String imageWordBuys, String telephoneBuys, String outpatientBuys,
			String homeDoctorValue, String imageWordValue,
			String telephoneValue, String outpatientValue) {
		super();
		HomeDoctorWeekPrice = homeDoctorWeekPrice;
		HomeDoctorMonthPrice = homeDoctorMonthPrice;
		ImageWordPrice = imageWordPrice;
		TelephonePrice = telephonePrice;
		OutpatientPrice = outpatientPrice;
		HomeDoctorBuys = homeDoctorBuys;
		ImageWordBuys = imageWordBuys;
		TelephoneBuys = telephoneBuys;
		OutpatientBuys = outpatientBuys;
		HomeDoctorValue = homeDoctorValue;
		ImageWordValue = imageWordValue;
		TelephoneValue = telephoneValue;
		OutpatientValue = outpatientValue;
	}




	public HomeInfo() {
		super();
	}




	public HomeInfo(String s){
		try {
			JSONObject jsonObject = new JSONObject(s);
			DoctorHeadImageUrl = jsonObject.getString("DoctorHeadImageUrl");
			DoctorNickName = jsonObject.getString("DoctorNickName");
			DoctorLevel = jsonObject.getString("DoctorLevel");
			DoctorHospital = jsonObject.getString("DoctorHospital");
			DoctorAbility = jsonObject.getString("DoctorAbility");
			DoctorBriefIntroduction = jsonObject.getString("DoctorBriefIntroduction");
			HomeDoctorWeekPrice = jsonObject.getString("HomeDoctorWeekPrice");
			HomeDoctorMonthPrice = jsonObject.getString("HomeDoctorMonthPrice");
			ImageWordPrice = jsonObject.getString("ImageWordPrice");
			TelephonePrice = jsonObject.getString("TelephonePrice");
			OutpatientPrice = jsonObject.getString("OutpatientPrice");
			HomeDoctorBuys = jsonObject.getString("HomeDoctorBuys");
			ImageWordBuys = jsonObject.getString("ImageWordBuys");
			TelephoneBuys = jsonObject.getString("TelephoneBuys");
			OutpatientBuys = jsonObject.getString("OutpatientBuys");
			HomeDoctorOpen = jsonObject.getString("HomeDoctorOpen");
			ImageWordOpen = jsonObject.getString("ImageWordOpen");
			TelephoneOpen = jsonObject.getString("TelephoneOpen");
			OutpatientOpen = jsonObject.getString("OutpatientOpen");
			HomeDoctorValue = jsonObject.getString("HomeDoctorValue");
			ImageWordValue = jsonObject.getString("ImageWordValue");
			TelephoneValue = jsonObject.getString("TelephoneValue");
			OutpatientValue = jsonObject.getString("OutpatientValue");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
	
	public String getDoctorHeadImageUrl() {
		return DoctorHeadImageUrl;
	}
	public void setDoctorHeadImageUrl(String doctorHeadImageUrl) {
		DoctorHeadImageUrl = doctorHeadImageUrl;
	}
	public String getDoctorNickName() {
		return DoctorNickName;
	}
	public void setDoctorNickName(String doctorNickName) {
		DoctorNickName = doctorNickName;
	}
	public String getDoctorLevel() {
		return DoctorLevel;
	}
	public void setDoctorLevel(String doctorLevel) {
		DoctorLevel = doctorLevel;
	}
	public String getDoctorHospital() {
		return DoctorHospital;
	}
	public void setDoctorHospital(String doctorHospital) {
		DoctorHospital = doctorHospital;
	}
	public String getDoctorAbility() {
		return DoctorAbility;
	}
	public void setDoctorAbility(String doctorAbility) {
		DoctorAbility = doctorAbility;
	}
	public String getDoctorBriefIntroduction() {
		return DoctorBriefIntroduction;
	}
	public void setDoctorBriefIntroduction(String doctorBriefIntroduction) {
		DoctorBriefIntroduction = doctorBriefIntroduction;
	}
	public String getHomeDoctorWeekPrice() {
		return HomeDoctorWeekPrice;
	}
	public void setHomeDoctorWeekPrice(String homeDoctorWeekPrice) {
		HomeDoctorWeekPrice = homeDoctorWeekPrice;
	}
	public String getHomeDoctorMonthPrice() {
		return HomeDoctorMonthPrice;
	}
	public void setHomeDoctorMonthPrice(String homeDoctorMonthPrice) {
		HomeDoctorMonthPrice = homeDoctorMonthPrice;
	}
	public String getImageWordPrice() {
		return ImageWordPrice;
	}
	public void setImageWordPrice(String imageWordPrice) {
		ImageWordPrice = imageWordPrice;
	}
	public String getTelephonePrice() {
		return TelephonePrice;
	}
	public void setTelephonePrice(String telephonePrice) {
		TelephonePrice = telephonePrice;
	}
	public String getOutpatientPrice() {
		return OutpatientPrice;
	}
	public void setOutpatientPrice(String outpatientPrice) {
		OutpatientPrice = outpatientPrice;
	}
	public String getHomeDoctorBuys() {
		return HomeDoctorBuys;
	}
	public void setHomeDoctorBuys(String homeDoctorBuys) {
		HomeDoctorBuys = homeDoctorBuys;
	}
	public String getImageWordBuys() {
		return ImageWordBuys;
	}
	public void setImageWordBuys(String imageWordBuys) {
		ImageWordBuys = imageWordBuys;
	}
	public String getTelephoneBuys() {
		return TelephoneBuys;
	}
	public void setTelephoneBuys(String telephoneBuys) {
		TelephoneBuys = telephoneBuys;
	}
	public String getOutpatientBuys() {
		return OutpatientBuys;
	}
	public void setOutpatientBuys(String outpatientBuys) {
		OutpatientBuys = outpatientBuys;
	}
	public String getHomeDoctorOpen() {
		return HomeDoctorOpen;
	}
	public void setHomeDoctorOpen(String homeDoctorOpen) {
		HomeDoctorOpen = homeDoctorOpen;
	}
	public String getImageWordOpen() {
		return ImageWordOpen;
	}
	public void setImageWordOpen(String imageWordOpen) {
		ImageWordOpen = imageWordOpen;
	}
	public String getTelephoneOpen() {
		return TelephoneOpen;
	}
	public void setTelephoneOpen(String telephoneOpen) {
		TelephoneOpen = telephoneOpen;
	}
	public String getOutpatientOpen() {
		return OutpatientOpen;
	}
	public void setOutpatientOpen(String outpatientOpen) {
		OutpatientOpen = outpatientOpen;
	}
	public String getHomeDoctorValue() {
		return HomeDoctorValue;
	}
	public void setHomeDoctorValue(String homeDoctorValue) {
		HomeDoctorValue = homeDoctorValue;
	}
	public String getImageWordValue() {
		return ImageWordValue;
	}
	public void setImageWordValue(String imageWordValue) {
		ImageWordValue = imageWordValue;
	}
	public String getTelephoneValue() {
		return TelephoneValue;
	}
	public void setTelephoneValue(String telephoneValue) {
		TelephoneValue = telephoneValue;
	}
	public String getOutpatientValue() {
		return OutpatientValue;
	}
	public void setOutpatientValue(String outpatientValue) {
		OutpatientValue = outpatientValue;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
