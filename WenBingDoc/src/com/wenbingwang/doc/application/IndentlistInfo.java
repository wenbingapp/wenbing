package com.wenbingwang.doc.application;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class IndentlistInfo implements Serializable{
	
	private String UserID;
	private String HeadImage;
	private String Nickname;
	private String ServerType;
	private String Age;
	private String Sex;
	private String BeginTime;
	private String EndTime;
	
	public IndentlistInfo(String s){
		try {
			JSONObject jsonObject = new JSONObject(s);
			UserID = jsonObject.getString("UserID");
			if (UserID.contains("_")) {
				UserID = UserID.split("_")[1];
			}
			HeadImage = jsonObject.getString("HeadImage");
			Nickname = jsonObject.getString("Nickname");
			ServerType = jsonObject.getString("ServerType");
			Age = jsonObject.getString("Age");
			Sex = jsonObject.getString("Sex");
			BeginTime = jsonObject.getString("BeginTime");
			EndTime = jsonObject.getString("EndTime");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getHeadImage() {
		return HeadImage;
	}
	public void setHeadImage(String headImage) {
		HeadImage = headImage;
	}
	public String getNickname() {
		return Nickname;
	}
	public void setNickname(String nickname) {
		Nickname = nickname;
	}
	public String getServerType() {
		return ServerType;
	}
	public void setServerType(String serverType) {
		ServerType = serverType;
	}
	public String getAge() {
		return Age;
	}
	public void setAge(String age) {
		Age = age;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getBeginTime() {
		return BeginTime;
	}
	public void setBeginTime(String beginTime) {
		BeginTime = beginTime;
	}
	public String getEndTime() {
		return EndTime;
	}
	public void setEndTime(String endTime) {
		EndTime = endTime;
	}
	
	

}
